import { Injectable } from '@angular/core';
import { HttpRequest, HttpHandler, HttpEvent, HttpInterceptor, HttpResponse, HttpErrorResponse } from '@angular/common/http';
import { Observable } from 'rxjs';
import { AuthServiceService } from '../auth/services/auth-service.service';
import 'rxjs/add/operator/do';
// import { Router } from '@angular/router';
// import { ToastrService } from 'ngx-toastr';
// import { Subscription } from 'rxjs/Subscription';
/**
 * Retry request after re-authentication
 */
@Injectable()
export class TokenInterceptor implements HttpInterceptor {
  counter: number = 0;
	constructor(
    private authService: AuthServiceService
    // private router: Router
    ) { }
	intercept(request: HttpRequest<any>, next: HttpHandler): Observable<HttpEvent<any>> {
    return next.handle(request)
    .do((event: HttpEvent<any>) => {
      if (event instanceof HttpResponse) {
        // do stuff with response if you want
      }
    }, (err: any) => {
      if (this.counter < 3) {
        if (err instanceof HttpErrorResponse) {
          if (err.status === 401 && localStorage.getItem('currentUser')) {
            // console.log(JSON.parse(localStorage.getItem('currentUser')).token)
            const refresh = JSON.parse(localStorage.getItem('currentUser')).token;
            this.authService.refresh(refresh)
            .subscribe(resp => {
              //  const newRequest = request.clone({
              //   headers: request.headers.set('Authorization', 'Bearer '+ JSON.parse(localStorage.getItem('currentUser')).token),
              // });
              return next.handle(request);
            }, error => {
              window.setTimeout(() => {this.authService.logout();}, 1000);
              this.counter++;
            });
          }
        }
      }else {
        window.setTimeout(() => {this.authService.logout();}, 1000);
      }
    });
  }
}

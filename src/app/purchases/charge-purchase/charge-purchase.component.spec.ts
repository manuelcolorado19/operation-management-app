import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargePurchaseComponent } from './charge-purchase.component';

describe('ChargePurchaseComponent', () => {
  let component: ChargePurchaseComponent;
  let fixture: ComponentFixture<ChargePurchaseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargePurchaseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargePurchaseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

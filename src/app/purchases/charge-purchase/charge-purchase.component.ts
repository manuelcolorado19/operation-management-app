import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { ToastrService } from 'ngx-toastr';
import { PurchaseServiceService } from '../services/purchase-service.service'; 
@Component({
  selector: 'app-charge-purchase',
  templateUrl: './charge-purchase.component.html',
  styleUrls: ['./charge-purchase.component.scss']
})
export class ChargePurchaseComponent implements OnInit {
	
 @ViewChild('file_input') 
 file_input: ElementRef;

 rows = [];
  data_upload:any = [];
  constructor(private toastr: ToastrService, private purchaseService:PurchaseServiceService) { }

  ngOnInit() {
  }
getRowHeight(row) {
    return 50;
  }

  getCellClass({ row, column, value }): any {
    return {
      'text-wrap': true
    };
  }
  onFileChange(evt: any) {
    /* wire up file reader */
    // console.log(this.file_input.nativeElement.innerHTML);
    // console.log(evt.target)
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
    	this.toastr.error("No se pueden cargar múltiples archivos", "Error");
    	throw new Error('Cannot use multiple files');	
    } 
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      // console.log(ws);
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1
      // delete ws.H1

      /* save data */
      this.rows = this.validateData(wb,XLSX.utils.sheet_to_json(ws, {header: 1}));
      // console.log(this.data_upload)
    this.rows = [...this.rows];
    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateData(wb,data:any) {
    data.splice(0, 1);
    const dateMode = wb.Workbook.WBProps.date1904;
    let data_json = []
    for (let row of data ) {
      // console.log(row[0]);
      // if(!(moment(String(row[0]), 'DD-MM-YYYY', true).isValid())){
      //   this.toastr.error("Revise el formato de fecha el correcto es DD-MM-AAAA", "Error")
      //   this.file_input.nativeElement.value = "";
      //   return [];
      // }
      
      // if(typeof(row[1]) !== 'number'){
      //   this.toastr.error("La columna SKU debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   return [];
      // }
      
      // if(typeof(row[6]) !== 'number'){
      //   this.toastr.error("La columna cantidad debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   return [];
      // }
      // if(typeof(row[7]) !== 'number'){
      //   this.toastr.error("La columna costo unitario debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   return [];
      // }
      // if(typeof(row[8]) !== 'number'){
      //   this.toastr.error("La columna iva crédito debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   return [];
      // }
      // if(typeof(row[9]) !== 'number'){
      //   this.toastr.error("La columna pvd debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   return [];
      // }
      let warehouse;
      if (row[15] !=undefined){
        warehouse = row[15]
      }else{
        warehouse = ""
      }
      console.log(warehouse)
      data_json.push({
        date_purchase:XLSX.SSF.format('DD-MM-YYYY', row[0], { date1904: dateMode }),
        sku:row[1],
        code_seven_digits:row[2],
        model:row[3],
        quantity:row[4],
        unit_price:row[5],
        average_price:row[6],
        credit_iva:row[7],
        pmb:row[8],
        seller_sku:row[9],
        rut_provider:row[10],
        name_provider:row[11],
        document_type:row[12],
        number_document:row[13],
        purchase_type:row[14],
        warehouse:warehouse
      });
    }
    return data_json;
  }

  downloadExampleExcel(event): void {
    // console.log("fdsdfsfd")
    this.purchaseService.exportExampleExcel();
  }

  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.rows = [];
  }

  sendData(): void {
  	// console.log("DATA send")
    if (this.rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      this.purchaseService.createPurchase(this.rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                // this.data_upload = [];
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      // this.saleService.sendRetailData(this.data_upload);

    }
    
  }

  formatShipping(date){
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

}

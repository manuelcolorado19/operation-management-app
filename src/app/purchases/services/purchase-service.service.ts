import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';
import { Observable } from 'rxjs/Observable';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class PurchaseServiceService {

public headers: HttpHeaders = new HttpHeaders();
  token: string;

  constructor(public http: HttpClient,) {
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
  	}
   }

   createPurchase(data: any): Observable<boolean> {
  	return this.http.post(url+'massive/purchase/data',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  listPurchases(params:any): any {
    
    return this.http.get(url+'purchase/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  deletePurchase(id:number) {
    return this.http.delete(url+'purchase/'+id + "/", {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  exportExcel(id:number) {
    return this.http.get(url+'export/purchases/' + id + "/", {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", id)) 
  }

  downLoadFile(data:any, type: string, id:number):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Informe de compras " +id+ ".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
  exportExampleExcel(){
    return this.http.get(url+'export/purchase/xlsx/example', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadExample(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet")) 
  }

  downLoadExample(data:any, type: string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Archivo de ejemplo.xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
}

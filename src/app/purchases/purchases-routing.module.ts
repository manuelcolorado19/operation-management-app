import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PurchasesComponent } from './purchases.component'; 
import { ChargePurchaseComponent } from './charge-purchase/charge-purchase.component';
import { ListPurchaseComponent } from './list-purchase/list-purchase.component';

const routes: Routes = [{
	path: '',
	component: PurchasesComponent,
	children: [
    {
      path: 'charge-purchase',
      component: ChargePurchaseComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Carga de Compras' }
    },
    {
      path: 'list-purchase',
      canActivate: [],
      component: ListPurchaseComponent,
      data: { title: 'Asiamerica | Listado de Compras' }
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PurchasesRoutingModule { }

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PurchasesComponent } from './purchases.component';
import { PurchasesRoutingModule } from './purchases-routing.module';
import { LayoutModule } from '../@layout/layout.module';
import { ChargePurchaseComponent } from './charge-purchase/charge-purchase.component';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ListPurchaseComponent } from './list-purchase/list-purchase.component';

import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';

let COMPONENTS = [
	PurchasesComponent,
	ChargePurchaseComponent,
	ListPurchaseComponent
]
@NgModule({
  imports: [
    CommonModule,
    PurchasesRoutingModule,
    LayoutModule,
    FormsModule,
    NgxDatatableModule,
    MatButtonModule,
    MatFormFieldModule,
    MatAutocompleteModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    
  ],
  declarations: [...COMPONENTS,]
})
export class PurchasesModule { }

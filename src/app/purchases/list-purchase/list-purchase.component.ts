import { Component, OnInit, ViewChild } from '@angular/core';
import { PurchaseServiceService } from '../services/purchase-service.service';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../sales/models/sales.models';
import { RouterModule, Router } from '@angular/router';

@Component({
  selector: 'app-list-purchase',
  templateUrl: './list-purchase.component.html',
  styleUrls: ['./list-purchase.component.scss']
})
export class ListPurchaseComponent implements OnInit {
@ViewChild('myTable') table: any;
	rows=[];
	params: any = {};
  page = new Page();

  constructor(private purchaseService:PurchaseServiceService, private toastr:ToastrService, private router: Router) { 
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 50;
  	this.listData();
  }

  ngOnInit() {
  }

  listData(){
    this.purchaseService.listPurchases(this.params).subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.rows = result.results;
          this.page.totalElements = result.count;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  toggleExpandRow(row, rowGrandIndex) {
    // console.log(row);
    // console.log(this.expanded);
    // this.grandIndex = rowGrandIndex;
    this.table.rowDetail.toggleExpandRow(row);
    
  }
  deletePurchase(row) {
    console.log(row);
    this.purchaseService.deletePurchase(row.id)
          .subscribe(
              result => {
                this.toastr.success("Venta eliminada exitosamente", "Bien");
                // window.location.reload();
                this.listData();

              },
              error => {
                this.toastr.clear();
                this.toastr.error("Ud no tiene permisos para eliminar registros de ventas", "Disculpe")
                // for (let each in error.error){
                //   this.toastr.error(error.error[each][0], "Error")
                // }
              }
          )
    }

    downloadPurchase(row) {
      if (row == 999999){
        this.purchaseService.exportExcel(row);
      }else{
        this.purchaseService.exportExcel(row.id);
      }
    }
  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }
  goCharge(event){
    this.router.navigate(['/purchases/charge-purchase']);
  }

}

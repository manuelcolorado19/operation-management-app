import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PricesComponent } from './prices.component';
import { ListPricesComponent } from './list-prices/list-prices.component'
import { ChargePricesComponent } from './charge-prices/charge-prices.component';
import { UploadFalabellaComponent } from './upload-falabella/upload-falabella.component';

const routes: Routes = [{
  path: '',
  component: PricesComponent,
  children: [
    {
      path: 'list-prices',
      component: ListPricesComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Listado de Precios' }
    },
    {
      path: 'charge-prices',
      component: ChargePricesComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Cargar Lista de Precios' }
    },
    {
      path: 'upload-falabella',
      component: UploadFalabellaComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Cargar Lista de Precios Falabella' }
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class PricesRoutingModule { }

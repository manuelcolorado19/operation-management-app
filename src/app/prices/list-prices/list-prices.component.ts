import { Component, OnInit } from '@angular/core';
import { Page } from '../../sales/models/sales.models';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { PricesServiceService } from '../services/prices-service.service';

@Component({
  selector: 'app-list-prices',
  templateUrl: './list-prices.component.html',
  styleUrls: ['./list-prices.component.scss']
})
export class ListPricesComponent implements OnInit {
  rows = [];
  type_sku: string = 'normal';
  params: any = {};
  page = new Page();
  total_pages: any = [];
  current_page:number;
  searchTerm$ = new Subject<string>();
  editing = {};

  constructor(private priceService:PricesServiceService, private toastr:ToastrService) { 
    this.listData();
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 10;
  }

  ngOnInit() {
  }
  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
  }
  listData(){
    this.priceService.listPrices(this.params).subscribe(
        result => {
          this.page.totalElements = result.count;
          // this.data = result.results;
          this.rows = result.results;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }

  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }
   saveData(row) {
    this.priceService.updatePrices(row)
          .subscribe(
              result => {
                this.toastr.success("Venta actualizada exitosamente", "Bien");
                this.listData();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
  }

  export_excel(event):any {
      this.priceService.exportExcel();
  }



}

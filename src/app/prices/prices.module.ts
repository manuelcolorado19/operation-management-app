import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { PricesComponent } from './prices.component';
import { PricesRoutingModule } from './prices-routing.module';
import { LayoutModule } from '../@layout/layout.module';
import { ListPricesComponent } from './list-prices/list-prices.component'
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ChargePricesComponent } from './charge-prices/charge-prices.component';
import { FormsModule } from '@angular/forms';
import { UploadFalabellaComponent } from './upload-falabella/upload-falabella.component';

@NgModule({
  imports: [
    CommonModule,
    PricesRoutingModule,
    LayoutModule,
    MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule,
  NgxDatatableModule,
  FormsModule
  ],
  declarations: [PricesComponent, ListPricesComponent, ChargePricesComponent, UploadFalabellaComponent,]
})
export class PricesModule { }

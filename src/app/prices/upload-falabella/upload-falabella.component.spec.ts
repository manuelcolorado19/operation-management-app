import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadFalabellaComponent } from './upload-falabella.component';

describe('UploadFalabellaComponent', () => {
  let component: UploadFalabellaComponent;
  let fixture: ComponentFixture<UploadFalabellaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadFalabellaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadFalabellaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

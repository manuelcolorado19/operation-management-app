import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { PricesServiceService } from '../services/prices-service.service';
@Component({
  selector: 'app-upload-falabella',
  templateUrl: './upload-falabella.component.html',
  styleUrls: ['./upload-falabella.component.scss']
})
export class UploadFalabellaComponent implements OnInit {
	rows = [];
  data_upload:any = [];
  @ViewChild('file_input') 
	file_input: ElementRef;
  constructor(private toastr: ToastrService, private priceService:PricesServiceService) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
    	this.toastr.error("No se pueden cargar múltiples archivos", "Error");
    	throw new Error('Cannot use multiple files');	
    } 
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      // const dateMode = wb.Workbook.WBProps.date1904;
      // let val = ws.B3.v;
      // XLSX.SSF.format('DD-MM-YYYY', val, { date1904: dateMode });

      const wsname: string = wb.SheetNames[0];
      // const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      this.rows = this.validateData(wb, XLSX.utils.sheet_to_json(ws, {header: 1}));
    	this.rows = [...this.rows];
    };
    reader.readAsBinaryString(target.files[0]);
  }
  validateData(wb,data:any) {
    data.splice(0, 1);
    const dateMode = wb.Workbook.WBProps.date1904;
    let data_json = []
    for (let row of data ) {
      // console.log(row);
      data_json.push({
      	sku:row[0].toString().substring(0,5),
		normal_price:row[1],
		offert_price:row[2],
		start_date:XLSX.SSF.format('DD-MM-YYYY', row[3], { date1904: dateMode }),
		end_date:XLSX.SSF.format('DD-MM-YYYY', row[4], { date1904: dateMode }),
      });
    }
    return data_json;
  }

  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.rows = [];
  }

  sendData(): void {
    // console.log(this.rows);
  	 if (this.rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
    	this.priceService.sendPricesFalabella(this.rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";

                this.data_upload = [];
                this.rows = [];

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    }
  }

}

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { PricesServiceService } from '../services/prices-service.service';
@Component({
  selector: 'app-charge-prices',
  templateUrl: './charge-prices.component.html',
  styleUrls: ['./charge-prices.component.scss']
})
export class ChargePricesComponent implements OnInit {
  rows = [];
  data_upload:any = [];

	@ViewChild('file_input') 
	file_input: ElementRef;

  constructor(private toastr: ToastrService, private priceService:PricesServiceService) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
    	this.toastr.error("No se pueden cargar múltiples archivos", "Error");
    	throw new Error('Cannot use multiple files');	
    } 
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      this.rows = this.validateData(XLSX.utils.sheet_to_json(ws, {header: 1}));
    	this.rows = [...this.rows];
    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateData(data:any) {
    data.splice(0, 1);
    let data_json = []
    for (let row of data ) {
    	if(typeof(row[0]) !== 'number'){
	        this.toastr.error("La columna SKU debe contener números únicamente", "Error")
	        this.file_input.nativeElement.value = "";
	        return [];
	      }
	      if(typeof(row[1]) !== 'string'){
	        this.toastr.error("La columna descripción debe contener caracteres alfanuméricos únicamente", "Error")
	        this.file_input.nativeElement.value = "";
	        return [];
	      }
	      if(typeof(row[2]) !== 'number'){
	        this.toastr.error("La columna Precio Yapo debe contener números únicamente", "Error")
	        this.file_input.nativeElement.value = "";
	        return [];
	      }
	      if(typeof(row[3]) !== 'number'){
	        this.toastr.error("La columna Precio Mercadolibre debe contener números únicamente", "Error")
	        this.file_input.nativeElement.value = "";
	        return [];
	      }
	      if(typeof(row[4]) !== 'number'){
	        this.toastr.error("La columna Precio Normal debe contener números únicamente", "Error")
	        this.file_input.nativeElement.value = "";
	        return [];
	      }
	      if(typeof(row[5]) !== 'number'){
	        this.toastr.error("La columna Precio Mayorista debe contener números únicamente", "Error")
	        this.file_input.nativeElement.value = "";
	        return [];
	      }
      data_json.push({
	       sku:row[0],
		   description:row[1],
		   presential_price:row[2],
		   mercadolibre_price:row[3],
		   normal_price:row[4],
		   wholesaler_price:row[5],
      });
    }
    return data_json;
  }
  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.rows = [];
  }
  sendData(): void {
    if (this.rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      this.priceService.sendPrices(this.rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    }
    
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargePricesComponent } from './charge-prices.component';

describe('ChargePricesComponent', () => {
  let component: ChargePricesComponent;
  let fixture: ComponentFixture<ChargePricesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargePricesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargePricesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

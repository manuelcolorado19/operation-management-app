import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';
import { Observable } from 'rxjs/Observable';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';


@Injectable({
  providedIn: 'root'
})
export class PricesServiceService {
  token: string;
  public headers: HttpHeaders = new HttpHeaders();

  constructor(public http: HttpClient,) {
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
  	}
   }

  listPrices(params:any): any {
    
    return this.http.get(url+'master_price_falabella/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }

  sendPrices(data: any): Observable<boolean> {
    return this.http.post(url+'price_product/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };
  updatePrices(data: any): Observable<boolean> {
    console.log(data);
    // data.price_yapo = parseInt(data.price_yapo);
    return this.http.put(url+'price_product/'+data.id + "/", data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  exportExcel(): any{
       // console.log(params);
    return this.http.get(url+'export/prices/xlsx', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
  }

  downLoadFile(data: any, type: string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Informe de precios Asiamerica.xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
  sendPricesFalabella(data){
      return this.http.post(url+'price_product_falabella/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }
}

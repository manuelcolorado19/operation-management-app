import { TestBed } from '@angular/core/testing';

import { PricesServiceService } from './prices-service.service';

describe('PricesServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PricesServiceService = TestBed.get(PricesServiceService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { SkuServiceService } from '../services/sku-service.service';
import { ToastrService } from 'ngx-toastr';
import { SalesServiceService } from '../../sales/services/sales-service.service';
import { Router, ActivatedRoute } from '@angular/router';
import {Location} from '@angular/common';
@Component({
  selector: 'app-create-sku',
  templateUrl: './create-sku.component.html',
  styleUrls: ['./create-sku.component.scss']
})
export class CreateSkuComponent implements OnInit {

  categories: any = [];
  register: any = {};
  sku:number;
  data:any;

  constructor(private skuService: SkuServiceService, private toastr:ToastrService, private saleService:SalesServiceService, private route: ActivatedRoute, private router:Router, private location: Location) { 
    this.data = this.route.snapshot.data.sku_data;
    // console.log(data);
    if (this.data !== undefined){
      this.register.sku = this.data.sku;
      this.register.description = this.data.description;
      this.register.type_product = this.data.type_sku;
      this.register.length = this.data.length;
      this.register.height = this.data.height;
      this.register.width = this.data.width;
      this.register.weight = this.data.weight;
      this.register.category_product_sale = this.data.category_product_sale;
      this.register.products = this.data.products;

    }else{

    this.register.products = [];
    }
  	this.allCategories();
  }
  ngOnInit() {
  }

  allCategories() {
  	this.skuService.getCategories().subscribe(
  		result=> {
        this.categories = result.results;
        // console.log(this.categories);
        
			return result;
  		},
  		error=> {
  			this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
  		}
  	)
  }
  registerSKU() {
  	// console.log(this.register);
    if (this.data !== undefined){
      this.skuService.updateSKU(this.register, this.data.id)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("SKU actualizado exitosamente", "Bien");
              this.router.navigate(['/sku/list-sku'])
            }
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
      }else{
        // console.log(this.register);
  	this.skuService.createSKU(this.register)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("SKU guardado exitosamente", "Bien");
              this.register = {};
              this.register.products = [];
              // this.register.products = [];
            }
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
      }
  }
  addProduct() {
    if (this.sku > 9999999 || this.sku < 10000) {
        this.toastr.clear();
        this.toastr.error("El número de sku debe ser de 5 dígitos", "Error");
    }
    else{
      this.saleService.findProductDescription(String(this.sku)).subscribe(
            result => {
              if (result.length == 0){
                this.toastr.error("No existe producto con SKU " + this.sku, "No existe");
              }else{
                console.log()
                this.register.products.push({
                  sku: result[0].sku,  
                  description:result[0].description,
                  quantity:0,
                  percentage_price:0,
                });
              }
              },
              error =>{
                this.toastr.clear();
                this.toastr.error("Hubo un error al añadir el producto", "Error");
              })
    }
  }
  removeProduct(i) {
      this.register.products.splice(i, 1)
  }

  filterType(event){
    if(this.register.category_product_sale !== undefined && this.register.type_product !== undefined && this.data == undefined){
      switch (this.register.type_product) {
        case "Normal":
          this.register.sku = this.categories[this.register.category_product_sale - 1].last_sku_product + 1
          break;

        case "Pack":
          this.register.sku = parseInt(this.categories[this.register.category_product_sale - 1].last_sku_pack) + 1
          break;
        
        default:
          break;
      }
    }
  }

}

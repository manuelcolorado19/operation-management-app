import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Observable, throwError, interval } from 'rxjs';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { of, BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';
@Injectable({
  providedIn: 'root'
})
export class SkuServiceService {
	public headers: HttpHeaders = new HttpHeaders();
  token: string;
  internal_params: any = {};
  constructor(
  	 public http: HttpClient,
  	) { 
    this.internal_params.channel = '';
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
    }
  }

    createSKU(register: any): Observable<boolean> {
  	return this.http.post(url+'product_sale/',register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };
  listSKU(type_sku:string, params:any): any {
    if (type_sku === 'normal'){

  return this.http.get(url + 'product_sale_sku/', {headers:this.headers, params:params})
             .pipe(
               map((response: any) => {
                 return response
               })

          )
    }else {
      return this.http.get(url + 'pack/', {headers:this.headers, params:params})
             .pipe(
               map((response: any) => {
                 return response
               })
          )
    }
  }

  getSKUData(id:number, type_sku:string): any {
    if (type_sku === 'Normal'){

    return this.http.get(url + 'product_sale_sku/' + id +'/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
    }else {
      return this.http.get(url + 'pack/' + id +'/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
    }
  }

  getCategories(): any {
    return this.http.get(url + 'category_product/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }

  updateSKU(register: any, id:number): Observable<boolean> {
    // console.log(register);
    // return "lkjfhgf";
    return this.http.put(url+'product_sale/'+id + "/", register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  searchSKU(terms: Observable<string>, params2: any, type_sku:any) {
    // console.log(params);
  return terms.debounceTime(600)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchEntrieSKU(term, params2, type_sku));
}
searchEntrieSKU(term, params, type_sku) {
    // console.log(type_sku)
    params.search = term;
     if (type_sku === 'normal'){
      return this.http.get(url + 'product_sale_sku/',{params:params,headers:this.headers})
      .pipe(
          map((res) => {
            // console.log(res);
            return res;
          })
        )
  }else {
    return this.http.get(url + 'pack/',{params:params,headers:this.headers})
      .pipe(
          map((res) => {
            // console.log(res);
            return res;
          })
        )
  }
  }
  exportExcel(file_name:string, type_sku:string): any{
        // console.log(params);
    return this.http.get(url+'export/sku/xlsx', {params:{type_sku:type_sku},headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file_name))
  }
  downLoadFile(data: any, type: string, file_name:string):any{
        // console.log("lkjhgfddfghjkl")
        const blob = new Blob([data], { type: type});
        const fileName: string = file_name  + ".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

    createProduct_Code(data: any): Observable<boolean> {
    return this.http.post(url+'product_code/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  updateProduct_Code(data: any, id:number): Observable<boolean> {
    return this.http.put(url+'product_code/'+id + "/", data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  listChannelSKU(params:any){
    this.internal_params.channel = params.channel;
       return this.http.get(url + 'sku_channel/', {headers:this.headers, params:params})
             .pipe(
               map((response: any) => {
                 return response
               })

          )

  }

  searchByChannel(terms: Observable<string>, params){
    console.log(params);
    // this.internal_params.channel = params.channel;
    // this.internal_params.page = params.page;
    return terms.debounceTime(600)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchEntries(term, params));
  }

  searchEntries(term, params){
    // console.log(params);
    return this.http.get(url + 'sku_channel/',{params:{search:term, channel:this.internal_params.channel},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
  updateChannelSKU(){
    return this.http.post(url+'sku_channel/',{}, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  chargeSKUList(register: any): Observable<boolean> {
    return this.http.post(url+'sku_charge/',register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };
  chargeSKUPackList(register: any): Observable<boolean> {
    return this.http.post(url+'sku_charge_pack/',register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  exportExampleExcel(url_append:string, type_sku:string){
    return this.http.get(url+url_append, {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadExample(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", type_sku)) 
  }

  downLoadExample(data:any, type: string, type_sku:string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Archivo de ejemplo "+ type_sku +".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
}

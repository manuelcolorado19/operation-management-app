import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, HttpModule  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpClientModule } from '@angular/common/http';
import { SkuServiceService } from './sku-service.service'

@Injectable()
export class UpdateSKUResolveService implements Resolve<any>{
  public url:string = "http://localhost:8000/";
  public headers: HttpHeaders = new HttpHeaders();
  token: string;

  constructor(private skuService: SkuServiceService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.skuService.getSKUData(route.params['id'], route.params['type_sku']);
  }
}

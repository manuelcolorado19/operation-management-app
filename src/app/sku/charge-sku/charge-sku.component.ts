import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import * as XLSX from 'xlsx';
import { SkuServiceService } from '../services/sku-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-charge-sku',
  templateUrl: './charge-sku.component.html',
  styleUrls: ['./charge-sku.component.scss']
})
export class ChargeSkuComponent implements OnInit {
	data_rows = [];
	@ViewChild('file_input') 
	file_input: ElementRef;
  sku_type: string;
  url_append: string;
	data_upload:any = [];
  constructor(private toastr: ToastrService, private skuService:SkuServiceService, private route: ActivatedRoute) { 
    this.sku_type = this.route.snapshot.data.sku_type;
    this.url_append = this.route.snapshot.data.url_example;
  }

  ngOnInit() {
  }
  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: true});
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1

      this.data_upload = this.validateData(XLSX.utils.sheet_to_json(ws, {header: 1}));
    };
    reader.readAsBinaryString(target.files[0]);
  }
  validateData(data:any) {
    data.splice(0, 1);
    if(this.sku_type === 'Producto'){
    for (let row of data ) {
      this.data_rows.push({
        sku:row[0],
        description:row[1],
        variation:row[2],
        code_seven_digits:row[3],
        height:row[4],
        width:row[5],
        length:row[6],
        weight:row[7],
      })
    }
      
    }else{
      for (let row of data ) {
      this.data_rows.push({
        sku:row[0],
        description:row[1],
        sku_product:row[2],
        quantity:row[3],
        percentage_price:row[4],
        height:row[5],
        width:row[6],
        length:row[7],
        weight:row[8],
      })
    }
      
    }

}
cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.data_upload = [];
    this.data_rows = [];
  }
  sendData(): void {

  	 if (this.data_rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      if(this.sku_type === 'Producto'){

    	this.skuService.chargeSKUList(this.data_rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";

                this.data_upload = [];
                this.data_rows = [];

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      }else{
        console.log(this.data_rows);
        this.skuService.chargeSKUPackList(this.data_rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";
                this.data_upload = [];
                this.data_rows = [];

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      }
    }
  }
  downloadExampleExcel(event, url_append, sku_type){
    this.skuService.exportExampleExcel(url_append, sku_type);
  }
}

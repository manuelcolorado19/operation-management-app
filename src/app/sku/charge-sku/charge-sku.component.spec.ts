import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeSkuComponent } from './charge-sku.component';

describe('ChargeSkuComponent', () => {
  let component: ChargeSkuComponent;
  let fixture: ComponentFixture<ChargeSkuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeSkuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeSkuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

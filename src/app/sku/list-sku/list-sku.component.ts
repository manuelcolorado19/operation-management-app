import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Page } from '../../sales/models/sales.models';
import { SkuServiceService } from '../services/sku-service.service';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-list-sku',
  templateUrl: './list-sku.component.html',
  styleUrls: ['./list-sku.component.scss']
})
export class ListSkuComponent implements OnInit {
  @ViewChild('myTable') table: any;
  rows = [];
  rows2 = [];
  modalRef: BsModalRef;
  expanded: any = {};
  type_sku: string = 'normal';
  params: any = {};
  editing = {};
  product_selected:any;
  page = new Page();
  searchTerm$ = new Subject<string>();

  constructor(private skuService:SkuServiceService, private toastr:ToastrService, private router: Router,private modalService: BsModalService,) {
    // this.type_sku = 'normal';
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 50;
    this.listData();
    // console.log(this.type_sku)
    this.skuService.searchSKU(this.searchTerm$, this.params, this.type_sku)
      .subscribe(result => {
        // console.log("ñdlskuy")
        this.page.pageNumber = 0
        this.params.page = 1
          this.page.totalElements = result['count'];

        this.rows = result['results'];
        return result;
      });
    
   }

  ngOnInit() {
  }

  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    // console.log(this.params)
    this.listData();

  }
  listData() {
    // console.log(this.params)
    this.skuService.listSKU(this.type_sku, this.params).subscribe(
      result=> {
        this.rows = result.results;
        // this.page.pageNumber = 0
        // this.params.page = 1
        this.page.totalElements = result.count;
        // console.log(this.categories);
        
      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }
  filterType(event) {
    // console.log(this.params);
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.listData();
    
  }
  export_excel(event):any {
   
      this.skuService.exportExcel('Listado de Sku', this.type_sku)
  }
  editSKU(row_id:number, type_sku:string) { 
    this.router.navigate(['/sku/update', row_id, type_sku])
  }

  toggleExpandRow(row) {
    console.log('Toggled Expand Row!', row);
    this.table.rowDetail.toggleExpandRow(row);
  }

  onDetailToggle(event) {
    console.log('Detail Toggled', event);
  }

  openModalColors(template: TemplateRef<any>, row:any){
    this.rows2 = row.product_code;
    this.product_selected = row;
    this.modalRef = this.modalService.show(template);

  }
  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows2[rowIndex][cell] = event.target.value;
    this.rows2 = [...this.rows2];
  }

  addColor() {
    let suggested_code:any;
    if (this.product_selected.product_code.length == 0){
    suggested_code = this.product_selected.sku.toString() + "51";
    }else{

    suggested_code = parseInt(this.product_selected.product_code[this.product_selected.product_code.length-1].code_seven_digits) + 1
    }
    this.rows2.push({
      code_seven_digits: suggested_code.toString(),
      color_description: '-'
    })
    this.rows2 = [...this.rows2];
    
  }
  saveColor(row){
    console.log(row);
    if (row.id !== undefined){
      // console.log("Existente")
      let data = {
        code_seven_digits:row.code_seven_digits,
        color_description:row.color_description,
      }
      this.skuService.updateProduct_Code(data, row.id)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("Código guardado exitosamente", "Bien");
            }
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
    }else{
      let data = {
        code_seven_digits:row.code_seven_digits,
        color_description:row.color_description,
        product:this.product_selected.id,
      }
    this.skuService.createProduct_Code(data)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("Código guardado exitosamente", "Bien");
            }
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
    }

      }
    
  }



import { Component, OnInit, ViewChild } from '@angular/core';
import { Page } from '../../sales/models/sales.models';
import { SkuServiceService } from '../services/sku-service.service';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-channels-sku',
  templateUrl: './channels-sku.component.html',
  styleUrls: ['./channels-sku.component.scss']
})
export class ChannelsSkuComponent implements OnInit {
	@ViewChild('myTable') table: any;
	rows = [];
	params: any = {};
	page = new Page();
	searchTerm$ = new Subject<string>();

  constructor(private skuService:SkuServiceService, private toastr:ToastrService) {
    this.params.page = 1;
  	this.params.channel = "";
    this.page.pageNumber = 0;
    this.page.size = 50;
    this.listData();

    this.skuService.searchByChannel(this.searchTerm$, this.params)
      .subscribe(result => {
        this.page.pageNumber = 0
        this.params.page = 1
        this.page.totalElements = result['count'];
        this.rows = result['results'];
        return result;
      });
    

   }

  ngOnInit() {
  }

  listData(){
  	this.skuService.listChannelSKU(this.params).subscribe(
      result=> {
        this.rows = result.results;
        this.page.totalElements = result.count;
        
      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }
  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    // console.log(this.params)
    this.listData();

  }

  filterType(event) {
    // console.log(this.params);

    this.params.page = 1;
    this.page.pageNumber = 0;
    this.listData();
    
  }

  updateChannelsSKU($event){
    this.skuService.updateChannelSKU()
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("SKU actualizados exitosamente", "Bien");
              // this.register.products = [];
            }
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }

}

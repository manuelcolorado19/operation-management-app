import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelsSkuComponent } from './channels-sku.component';

describe('ChannelsSkuComponent', () => {
  let component: ChannelsSkuComponent;
  let fixture: ComponentFixture<ChannelsSkuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelsSkuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelsSkuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

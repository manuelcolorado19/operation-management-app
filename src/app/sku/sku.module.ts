import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LayoutModule } from '../@layout/layout.module'
import { SkuComponent } from './sku.component';
import { FormsModule } from '@angular/forms';

import { SkuRoutingModule } from './sku-routing.module';
import { CreateSkuComponent } from './create-sku/create-sku.component';
import { ListSkuComponent } from './list-sku/list-sku.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UpdateSKUResolveService } from './services/update-sku-resolve.service'
import { ModalModule } from 'ngx-bootstrap/modal';
import { ChannelsSkuComponent } from './channels-sku/channels-sku.component';
import { ChargeSkuComponent } from './charge-sku/charge-sku.component';


@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    SkuRoutingModule,
    FormsModule,
     MatButtonModule,
	  MatFormFieldModule,
	  MatInputModule,
	  MatRippleModule,
	  MatCardModule,
	  MatAutocompleteModule,
	  NgxDatatableModule,
    ModalModule.forRoot(),
    
  ],
  declarations: [CreateSkuComponent, SkuComponent, ListSkuComponent, ChannelsSkuComponent, ChargeSkuComponent],
  providers: [UpdateSKUResolveService,]
})
export class SkuModule { }

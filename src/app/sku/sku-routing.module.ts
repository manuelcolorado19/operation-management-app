import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { CommonModule } from '@angular/common';
import { SkuComponent } from './sku.component';
import { CreateSkuComponent } from './create-sku/create-sku.component';
import { ListSkuComponent } from './list-sku/list-sku.component';
import { ChannelsSkuComponent } from './channels-sku/channels-sku.component';
import { ChargeSkuComponent } from './charge-sku/charge-sku.component';
import { UpdateSKUResolveService } from './services/update-sku-resolve.service'

const routes: Routes = [{
  path: '',
  component: SkuComponent,
  children: [
    {
      path: 'create-sku',
      component: CreateSkuComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Crear SKU producto' }
    },
    {
      path: 'list-sku',
      component: ListSkuComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Lista de SKU' }
    },
    {
      path: 'charge-sku',
      component: ChargeSkuComponent,
      canActivate: [],
      data: { title: 'Asiamerica | SKU carga de productos', "sku_type": "Producto", "url_example": "sku_charge/"}
    },
    {
      path: 'charge-sku-pack',
      component: ChargeSkuComponent,
      canActivate: [],
      data: { title: 'Asiamerica | SKU carga de productos', "sku_type": "Pack", "url_example": "sku_charge_pack/"}
    },
    {
      path: 'update/:id/:type_sku',
      component: CreateSkuComponent,
      canActivate: [],
      resolve: {
        sku_data:UpdateSKUResolveService
      },
      data: { title: 'Asiamerica | Actualizar SKU.' }
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class SkuRoutingModule { }

import { Component, OnInit } from '@angular/core';
import { SalesServiceService } from '../../sales/services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-sidebar',
  templateUrl: './sidebar.component.html',
  styleUrls: ['./sidebar.component.scss']
})
export class SidebarComponent implements OnInit {
  public boolie: boolean;
  message:string;
  constructor(private data: SalesServiceService, private toastr: ToastrService, private router: Router,) { 
  	this.boolie = true;
  }

  ngOnInit() {
  }

  expandSidebar(): void {
  	this.boolie = !this.boolie
  	this.data.changeMessage('change');
  	this.data.currentMessage.subscribe(message => this.message = message)
  }
  goOut(): void {
    this.toastr.info("Has cerrado sesión exitosamente", "Hasta pronto");
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  }
 

}

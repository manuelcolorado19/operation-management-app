import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RetailPaymentListComponent } from './retail-payment-list.component';

describe('RetailPaymentListComponent', () => {
  let component: RetailPaymentListComponent;
  let fixture: ComponentFixture<RetailPaymentListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RetailPaymentListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RetailPaymentListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

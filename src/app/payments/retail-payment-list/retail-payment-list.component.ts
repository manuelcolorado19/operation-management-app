import { Component, OnInit, ViewChild } from '@angular/core';
import { PaymentServiceService } from '../services/payment-service.service';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../sales/models/sales.models';
@Component({
  selector: 'app-retail-payment-list',
  templateUrl: './retail-payment-list.component.html',
  styleUrls: ['./retail-payment-list.component.scss']
})
export class RetailPaymentListComponent implements OnInit {
  rows = [];
  @ViewChild('myTable') table: any;
  params: any = {};
  page = new Page();

  constructor(private paymentService:PaymentServiceService, private toastr:ToastrService) { 
  	this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 50;
  	this.listData();
  }

  ngOnInit() {
  }
  listData(){
    this.paymentService.listPayments(this.params).subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.rows = result.results;
          this.page.totalElements = result.count;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  toggleExpandRow(row, rowGrandIndex) {
    this.table.rowDetail.toggleExpandRow(row);
    
  }
  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }

  downloadRetailFile(row){
    this.paymentService.exportRetailFile(row.id);
  }

}

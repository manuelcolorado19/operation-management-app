import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { PaymentsRoutingModule } from './payments-routing.module';
import { UploadRetailPaymentComponent } from './upload-retail-payment/upload-retail-payment.component';
import { PaymentsComponent } from './payments.component';
import { LayoutModule } from '../@layout/layout.module';
import { UploadBankRegisterComponent } from './upload-bank-register/upload-bank-register.component';
import { RetailPaymentListComponent } from './retail-payment-list/retail-payment-list.component';
import { BankRegisterListComponent } from './bank-register-list/bank-register-list.component';
import { LiquidationCrossingComponent } from './liquidation-crossing/liquidation-crossing.component'
import { DndModule } from 'ngx-drag-drop';
import { MatListModule } from "@angular/material/list";
import { ModalModule } from 'ngx-bootstrap/modal';
@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    PaymentsRoutingModule,
    FormsModule,
    NgxDatatableModule,
    DndModule,
    MatListModule,
    ModalModule.forRoot()
  ],
  declarations: [UploadRetailPaymentComponent, PaymentsComponent, UploadBankRegisterComponent, RetailPaymentListComponent, BankRegisterListComponent, LiquidationCrossingComponent]
})
export class PaymentsModule { }

import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { HttpHeaders, HttpClientModule } from '@angular/common/http';
import { PaymentServiceService } from '../services/payment-service.service'; 
@Injectable()
export class PaymentCheckedResolveService implements Resolve<any>{
// public url:string = "http://localhost:8000/";
  // public headers: HttpHeaders = new HttpHeaders();
  // token: string;

  constructor(private paymentService: PaymentServiceService) {}

  resolve() {
    return this.paymentService.paymentsChecked();
  }
}

import { TestBed } from '@angular/core/testing';

import { PaymentCheckedResolveService } from './payment-checked-resolve.service';

describe('PaymentCheckedResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: PaymentCheckedResolveService = TestBed.get(PaymentCheckedResolveService);
    expect(service).toBeTruthy();
  });
});

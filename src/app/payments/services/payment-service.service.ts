import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';
import { Observable } from 'rxjs/Observable';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { ToastrService } from 'ngx-toastr';
@Injectable({
  providedIn: 'root'
})
export class PaymentServiceService {
	token: string;
  public headers: HttpHeaders = new HttpHeaders();


  constructor(public http: HttpClient, private toastr: ToastrService) { 
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
  	}
  }

  loadRetailPayment(data: any, date_payment:string): Observable<boolean> {
  	return this.http.post(url+'retail-payment/',{rows: data, date_payment:date_payment}, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  loadBankPayment(data: any): Observable<boolean> {
    return this.http.post(url+'bank-report/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };
  listPayments(params:any): any {  
    return this.http.get(url+'retail-payment/', {params:{status_payment:"pendiente"},headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })
        )
  }

  removeMatch(id:any):any{
    return this.http.put(url+'retail-payment/'+id+'/',{})
          .pipe(
            map((response:any)=>{
              return response
            })
          )
  }
  listRegister(params:any): any {
    return this.http.get(url+'bank-report/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  exportRetailFile(id_number): any {
    // return this.http.get(url+'retail-sales-report/', {params:{retail_payment:id_number}, headers:this.headers})
    //        .pipe(
    //          map((response: any) => {
    //            return response
    //          })
    //     )
     return this.http.get(url+'retail-sales-report/', {params:{retail_payment:id_number}, headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadRetailFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", id_number, "Informe de pagos retail "))
  }

 

  downLoadRetailFile(data:any, type: string, id:any, filename:string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = filename + id + ".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

   exportPaymentsReport(){
     console.log(this.headers) 
     return this.http.get(url+'payment-xlsx-report', {params:{},headers: this.headers, responseType: 'arraybuffer'})
     .subscribe(response=> this.downLoadRetailFile(response,"application/vnd.openxmlformats-officedocument.spreadsheetml.sheet","", "Reporte de liquidaciones"))
   }
   exportBankReportFile(id_number){
     return this.http.get(url+'bank-xlsx-report/', {params:{retail_payment:id_number}, headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadRetailFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", id_number, "Informe de cartola Bancaria "))
   }

   crossRetailPaymentBankReport(data: any): Observable<boolean> {
    return this.http.put(url+'bank-report/99/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };
  paymentsChecked(){
    return this.http.get(url+'retail-payment-checked/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  paymentCheckedChangeStatus(data:any){
    return this.http.put(url+'retail-payment-checked/', data, {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  presentialFile(params:any, filename:string){
    // console.log(params);
    // console.log(filename);
    return this.http.get(url+'sales-xlsx-report/', {params:params, headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(
        response => this.downLoadRetailFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "", params.payment_type + " " + filename),
          error =>{
            this.toastr.clear();
            this.toastr.error("No existen ventas presenciales para el día de hoy con este método de pago", "Error");
            
          }

      )

  }


  forecastFile(){
    return this.http.get(url+'payment-forecast', {params:{}, headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadRetailFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "", "" + "Proyección de pagos"))

  }
}

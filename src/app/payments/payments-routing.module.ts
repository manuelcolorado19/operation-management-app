import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { PaymentsComponent } from './payments.component';
import { UploadRetailPaymentComponent } from './upload-retail-payment/upload-retail-payment.component';
import { UploadBankRegisterComponent } from './upload-bank-register/upload-bank-register.component';
import { RetailPaymentListComponent } from './retail-payment-list/retail-payment-list.component';
import { BankRegisterListComponent } from './bank-register-list/bank-register-list.component';
import { LiquidationCrossingComponent } from './liquidation-crossing/liquidation-crossing.component';
import { PaymentCheckedResolveService } from './services/payment-checked-resolve.service';
const routes: Routes = [{
  path: '',
  component: PaymentsComponent,
  children: [
    {
      path: 'upload-retail-payment',
      canActivate: [],
      component: UploadRetailPaymentComponent,
      resolve: {
        payments_checked:PaymentCheckedResolveService
      },
      data: { title: 'Asiamerica | Carga Pagos Retail' }
    },
    {
      path: 'upload-register-bank',
      canActivate: [],
      component: UploadBankRegisterComponent,
      data: { title: 'Asiamerica | Carga Cartola Bancaria' }
    },
    {
      path: 'retail-payment-list',
      canActivate: [],
      component: RetailPaymentListComponent,
      data: { title: 'Asiamerica | Lista Pagos Retail' }
    },
    {
      path: 'register-bank-list',
      canActivate: [],
      component: BankRegisterListComponent,
      data: { title: 'Asiamerica | Lista Cartola Bancaria' }
    },
    {
      path: 'liquidation-crossing',
      canActivate: [],
      component: LiquidationCrossingComponent,
      data: { title: 'Asiamerica | Liquidacion - Cartolas' }
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
  providers: [PaymentCheckedResolveService]
})
export class PaymentsRoutingModule { }

import { Component, OnInit, TemplateRef  } from '@angular/core';
import { DndDropEvent, DropEffect } from "ngx-drag-drop";
import { PaymentServiceService } from '../services/payment-service.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

@Component({
  selector: 'app-liquidation-crossing',
  templateUrl: './liquidation-crossing.component.html',
  styleUrls: ['./liquidation-crossing.component.scss']
})
export class LiquidationCrossingComponent implements OnInit {
  selected: number;
  related_payments: any ;
  retail_payments = [];
  modalRef: BsModalRef;
	bank_reports = [];
	retail_total: number;
	bank_total: number;
	disable_retail_pay:boolean = false;
  disable_bank_report:boolean = false;
  bank_report_pending = [];
  bank_report_fill = [];

  draggableListRight = [];
  layout:any;
  horizontalLayoutActive:boolean = false;
  private currentDraggableEvent:DragEvent;
  private currentDragEffectMsg:string;
  private readonly verticalLayout = {
    container: "row",
    list: "column",
    dndHorizontal: false
  };
  private readonly horizontalLayout = {
    container: "row",
    list: "row",
    dndHorizontal: true
  };

  constructor(private paymentService:PaymentServiceService, private toastr:ToastrService, private modalService: BsModalService) {
  	this.listPayments();
  	this.listBankRegisters();
  	this.setHorizontalLayout( this.horizontalLayoutActive );
   }

  ngOnInit() {
  }
  setHorizontalLayout( horizontalLayoutActive:boolean ) {

    this.layout = (horizontalLayoutActive) ? this.horizontalLayout : this.verticalLayout;
  }

  onDragStart( event:DragEvent ) {

    this.currentDragEffectMsg = "";
    this.currentDraggableEvent = event;

  }

  onDragged( item:any, list:any[], effect:DropEffect ) {

    this.currentDragEffectMsg = `Drag ended with effect "${effect}"!`;

    if( effect === "move" ) {

      const index = list.indexOf( item );
      list.splice( index, 1 );
    }
  }

  onDragEnd( event:DragEvent ) {

    this.currentDraggableEvent = event;
   
  }
  filterBankReport(filter_type:Boolean){
    let array_pivote = []
    this.bank_reports.forEach(element => {
      if(element.related_payments.length > 0 && filter_type){
        array_pivote.push(element);
        console.log("entra")
        console.log(element)
      }
      if(!filter_type && element.related_payments.length == 0){
        array_pivote.push(element);
        console.log("no entra")
        console.log(element)
      }
      //array_pivote.push(element);
    });
    return array_pivote;
  }
  filterRelatedPayment(){
    let arr = []
    this.retail_payments.forEach(element => {
      if(element.bank_register_id != ""){
        arr.push(element)
        console.log("paso payment")
      }
    });
    return arr
  }
  checkSelected(id:number,sth:any){
    this.selected = id
    console.log("aqui")
    console.log(this.selected)
    return "selected"
  }

  onDrop( event:DndDropEvent, list?:any[] ) {
  	// console.log(event.data);
    if( list
      && (event.dropEffect === "copy"
        || event.dropEffect === "move") ) {

      let index = event.index;

      if( typeof index === "undefined" ) {

        index = list.length;
      }
     
      let data_push = {};
      if (event.data.hasOwnProperty('bill_register')){
      	data_push = {
          id: event.data.bill_register, 
          bill_register: event.data.bill_register,
          date_payment: event.data.date_payment,
          channel_name: event.data.channel_name,
          total_amount: event.data.total_payment_retail,
        }
      	this.retail_total = event.data.total_payment_retail;
      	this.disable_retail_pay = true;
      }else{
      	data_push = {
          id: event.data.bank_register, 
          bill_register:event.data.bank_register,
          date_payment: event.data.date_report,
          channel_name: event.data.channel_name,
          total_amount: event.data.total_amount,
        }
      	this.bank_total = event.data.total_amount;
      	this.disable_bank_report = true;
      }

      list.splice( index, 0, data_push);
    }
  }

  exportPaymentsReport(){
    this.paymentService.exportPaymentsReport()
  }

  listPayments(){
  	this.paymentService.listPayments({}).subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.retail_payments = result;
          // this.page.totalElements = result.count;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  listBankRegisters(){
    this.paymentService.listRegister({}).subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.bank_reports = result;
          this.bank_report_pending = this.filterBankReport(false);
          this.bank_report_fill = this.filterBankReport(true);
          this.retail_payments = this.filterRelatedPayment()
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }

  relatedElement(i: any){
    this.bank_reports.forEach(item => {
      if(item.id == i){
        this.related_payments = item.related_payments;
        // console.log(this.related_payments)
      }
    });
  }

  unmatchElement(i:any){
  if (confirm("¿Está seguro que desea desvincular?"))
    this.paymentService.removeMatch(i).subscribe(
      result=>{
        this.listBankRegisters();
        this.related_payments = []
        this.listPayments() 
        return result;
      },
      error =>{
        this.toastr.clear();
        this.toastr.error("Hubo un error al cargar los datos", "Error");
        
      }
    )

  }
  removeElement(row){
  	if (this.draggableListRight[row].hasOwnProperty('bill_register')){
      	this.disable_retail_pay = false;
	  }else{
	  	this.disable_bank_report = false;
	  }
  	this.draggableListRight.splice(row, 1);
  }

  openModal(template: TemplateRef<any>) {
    this.modalRef = this.modalService.show(template);
  }
  confirmCross(){
    console.log(this.draggableListRight);
    if (confirm("¿Está seguro de realizar el cruce de los datos?")){
      // console.log("si");
      this.paymentService.crossRetailPaymentBankReport(this.draggableListRight)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.draggableListRight = [];
                this.listBankRegisters();
                this.listPayments();
                this.disable_retail_pay = false;
                this.disable_bank_report = false;
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    }else{
      return
    }
  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LiquidationCrossingComponent } from './liquidation-crossing.component';

describe('LiquidationCrossingComponent', () => {
  let component: LiquidationCrossingComponent;
  let fixture: ComponentFixture<LiquidationCrossingComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LiquidationCrossingComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LiquidationCrossingComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { PaymentServiceService } from '../services/payment-service.service'; 

@Component({
  selector: 'app-upload-bank-register',
  templateUrl: './upload-bank-register.component.html',
  styleUrls: ['./upload-bank-register.component.scss']
})
export class UploadBankRegisterComponent implements OnInit {
	rows = [];
	@ViewChild('file_input') 
 	file_input: ElementRef;
  	data_upload:any = [];


  constructor(private toastr: ToastrService, private paymentService:PaymentServiceService) { }

  ngOnInit() {
  }
  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
    	this.toastr.error("No se pueden cargar múltiples archivos", "Error");
    	throw new Error('Cannot use multiple files');	
    } 
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      // console.log(ws);
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1

      /* save data */
      this.rows = this.validateData(wb,XLSX.utils.sheet_to_json(ws, {header: 1}));
      // console.log(this.data_upload)
    this.rows = [...this.rows];
    };
    reader.readAsBinaryString(target.files[0]);
  }
   validateData(wb,data:any) {
    data.splice(0, 1);
    const dateMode = wb.Workbook.WBProps.date1904;
    let data_json = []
    for (let row of data ) {
      data_json.push({
        register_bank:row[0],
        date_report:XLSX.SSF.format('DD-MM-YYYY', row[1], { date1904: dateMode }),
        total_amount:row[2],
        movement_type:row[3],
        payment_type:row[4],
        channel:row[5],
        additional_description:row[6],
        order_channel:row[7],
        document_type:row[8],
        bill_register:row[9],
        number_document:row[10],
      });
    }
    return data_json;
  }

  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.rows = [];
  }
  sendData(): void {
    if (this.rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      this.paymentService.loadBankPayment(this.rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                // this.data_upload = [];
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      // this.saleService.sendRetailData(this.data_upload);

    }
    
  }

}

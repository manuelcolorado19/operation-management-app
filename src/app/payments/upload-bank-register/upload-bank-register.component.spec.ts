import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadBankRegisterComponent } from './upload-bank-register.component';

describe('UploadBankRegisterComponent', () => {
  let component: UploadBankRegisterComponent;
  let fixture: ComponentFixture<UploadBankRegisterComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadBankRegisterComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadBankRegisterComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadRetailPaymentComponent } from './upload-retail-payment.component';

describe('UploadRetailPaymentComponent', () => {
  let component: UploadRetailPaymentComponent;
  let fixture: ComponentFixture<UploadRetailPaymentComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadRetailPaymentComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadRetailPaymentComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

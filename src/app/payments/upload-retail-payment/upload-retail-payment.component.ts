import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import { PaymentServiceService } from '../services/payment-service.service'; 
import { Router, ActivatedRoute } from '@angular/router';
@Component({
  selector: 'app-upload-retail-payment',
  templateUrl: './upload-retail-payment.component.html',
  styleUrls: ['./upload-retail-payment.component.scss']
})
export class UploadRetailPaymentComponent implements OnInit {
  
 @ViewChild('file_input') 
 file_input: ElementRef;
  color = "blue"
  rows = [];
  data_upload:any = [];
  payment_type:string = 'efectivo';
  weeks_header = [];
  data_checked = [];
  date_payment: string;

  constructor(private toastr: ToastrService, private paymentService:PaymentServiceService, private route: ActivatedRoute,) { 
    this.weeks_header = this.route.snapshot.data.payments_checked.weeks_header;
    this.data_checked = this.route.snapshot.data.payments_checked.data_checked;
  }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    let file = evt.target.files[0];
    let fileName = file.name;
    this.date_payment = fileName.substr(-13)
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
    	this.toastr.error("No se pueden cargar múltiples archivos", "Error");
    	throw new Error('Cannot use multiple files');	
    } 
    const reader: FileReader = new FileReader();
    // console.log(this.date_payment);
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1

      /* save data */
      this.rows = this.validateData(wb,XLSX.utils.sheet_to_json(ws, {header: 1}));
      // console.log(this.data_upload)
    this.rows = [...this.rows];
    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateData(wb,data:any) {
    data.splice(0, 1);
    const dateMode = wb.Workbook.WBProps.date1904;
    let data_json = []
    for (let row of data ) {
      data_json.push({
        order_channel:row[5],
        movement_type:row[4],
        channel:row[3],
        date_payment:XLSX.SSF.format('DD-MM-YYYY', row[2], { date1904: dateMode }),
        retail_id_number:row[1],
        total_sale:row[6],
        discount_commission:row[7],
        discount_shipping:row[8],
        discount_penalty:row[9],
        discount_resend:row[10],
        several_discount:row[11],
        total_pay:row[12],
        enterprise_rut:row[13],
        shipping_bill_number:row[14],
        commission_bill_number:row[15],
        product_bill_number:row[16],
        bank_register:row[17],
      });
    }
    return data_json;
  }
   cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.rows = [];
  }
  sendData(): void {
    if (this.rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      this.paymentService.loadRetailPayment(this.rows, this.date_payment)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.loadData();
                // this.data_upload = [];
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      // this.saleService.sendRetailData(this.data_upload);

    }
    
  }
  changeStatus(id_checked, status){
    let data = {
      id:id_checked,
      status:status
    }
    this.paymentService.paymentCheckedChangeStatus(data)
          .subscribe(
              result => {
                this.toastr.success("Revisión de pago actualizado exitosamente", "Bien");
                this.loadData();
                // this.data_upload = [];
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    
  }
  loadData(){
    this.paymentService.paymentsChecked().subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.weeks_header = result.weeks_header;
          this.data_checked = result.data_checked;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  generatePresentialFile(){
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('-');
    let filename = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('');
    let presential_params = {
      payment_type:this.payment_type,
      date_payment:dformat
    }
    this.paymentService.presentialFile(presential_params, filename);
  }

  generatePaymentsForecast(){
    this.paymentService.forecastFile();

  }

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { BankRegisterListComponent } from './bank-register-list.component';

describe('BankRegisterListComponent', () => {
  let component: BankRegisterListComponent;
  let fixture: ComponentFixture<BankRegisterListComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ BankRegisterListComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(BankRegisterListComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

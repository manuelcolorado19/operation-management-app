import { Component, OnInit } from '@angular/core';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../sales/models/sales.models';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-change-messages',
  templateUrl: './change-messages.component.html',
  styleUrls: ['./change-messages.component.scss']
})
export class ChangeMessagesComponent implements OnInit {
  data: any = {};
  data_messages: any = {};
  rows = [];
  type_sku: string = '';
  editing = {};
  params: any = {};
  page = new Page();
  searchTerm$ = new Subject<string>();
  hide_table: boolean = true;
//   editorConfig = {
//     "editable": true,
//     "spellcheck": true,
//     "height": "auto",
//     "minHeight": "0",
//     "width": "auto",
//     "minWidth": "0",
//     "translate": "yes",
//     "enableToolbar": true,
//     "showToolbar": true,
//     "placeholder": "Enter text here...",
//     "imageEndPoint": "",
//     "toolbar": [
//         ["bold"],
//         ["justifyLeft", "justifyCenter", "justifyRight",],
//         ["undo"],
//         ["orderedList", "unorderedList"],
//     ]
// }

  constructor(private configurationService: ConfigurationServiceService, private toastr:ToastrService) { 
  	this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 50;
    this.getMessages();

    this.configurationService.searchMessage(this.searchTerm$, this.params)
      .subscribe(result => {
        this.page.pageNumber = 0
        this.params.page = 1
        this.page.totalElements = result['count'];

        // this.data = result['results'];
        this.rows =result['results'];
        return result;
      });
  }

  ngOnInit() {
  }

  filterType($event) {
    if (this.data.type_message === 'custom'){
      this.params.pack = '3';
      this.hide_table = false;
      this.listMessages()
    }else{
      this.rows = [];
      this.hide_table = true;
    if (this.data.type_message === 'to_be_agreed') {
      this.data.message = this.data_messages[1].message;
      this.data.id = this.data_messages[1].id;
    }else{
      this.data.message = this.data_messages[0].message;
      this.data.id = this.data_messages[0].id;
    }
    }
  }
  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listMessages();

  }

  filterTypeSku($event) {
    this.listMessages();
  }
  getMessages() {
    this.configurationService.getAutomaticMessages().subscribe(
      result=> {
        this.data_messages = result.results
        this.data.type_message = this.data_messages[0].message_type;
        this.data.message = this.data_messages[0].message;
        this.data.id = this.data_messages[0].id
      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }

  listMessages() {
    this.configurationService.getListMessages(this.params).subscribe(
      result=> {
        this.rows = result.results
        this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }

  sendData() {
    console.log(this.data);
    this.configurationService.updateMessage(this.data).subscribe(
      result=> {
        this.toastr.success("Se ha actualizado el mensaje automático correctamente", "Bien")
      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
  }

   saveData(row) {
    this.configurationService.updateCustomMessage(row)
          .subscribe(
              result => {
                this.toastr.success("Mensaje cambiado exitosamente", "Bien");
                this.listMessages();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
  }
}

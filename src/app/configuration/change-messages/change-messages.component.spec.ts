import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChangeMessagesComponent } from './change-messages.component';

describe('ChangeMessagesComponent', () => {
  let component: ChangeMessagesComponent;
  let fixture: ComponentFixture<ChangeMessagesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChangeMessagesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChangeMessagesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

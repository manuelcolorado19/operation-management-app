import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';
import { ConfigurationRoutingModule } from './configuration-routing.module';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { LayoutModule } from '../@layout/layout.module';
import { ConfigurationComponent } from './configuration.component';
import { SendDailyOrdersComponent } from './send-daily-orders/send-daily-orders.component';
import { ChangeMessagesComponent } from './change-messages/change-messages.component';
import { NgxEditorModule } from 'ngx-editor';
import { UsersManagementsComponent } from './users-managements/users-managements.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';
import { ModalModule } from 'ngx-bootstrap/modal';
import { HistoricReportComponent } from './historic-report/historic-report.component';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { ProceduresComponent } from './procedures/procedures.component';
import { NgxUploaderModule } from 'ngx-uploader';
import { ChannelReportSalesComponent } from './channel-report-sales/channel-report-sales.component';
import { SupervisorActivitiesComponent } from './supervisor-activities/supervisor-activities.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';
import { SupervisionHistoricComponent } from './supervision-historic/supervision-historic.component';
import { UploadLabelsFalabellaComponent } from './upload-labels-falabella/upload-labels-falabella.component';
import { ShippingGuidesComponent } from './shipping-guides/shipping-guides.component';


defineLocale('es', esLocale);
@NgModule({
  imports: [
    CommonModule,
    ConfigurationRoutingModule,
    LayoutModule,
    NgxDatatableModule,
    FormsModule,
    NgxEditorModule,
    MatButtonModule,
  MatFormFieldModule,
  NgxUploaderModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule,
  NgxChartsModule,
  ModalModule.forRoot(),
  BsDatepickerModule.forRoot(),
  TimepickerModule.forRoot(),
  
  ],
  declarations: [ConfigurationComponent, SendDailyOrdersComponent, ChangeMessagesComponent, UsersManagementsComponent, HistoricReportComponent, SalesReportComponent, ProceduresComponent, ChannelReportSalesComponent, SupervisorActivitiesComponent, SupervisionHistoricComponent, UploadLabelsFalabellaComponent, ShippingGuidesComponent,]
})
export class ConfigurationModule { }

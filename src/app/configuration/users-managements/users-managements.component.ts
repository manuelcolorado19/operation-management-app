import { Component, OnInit, TemplateRef } from '@angular/core';
import { Page } from '../../sales/models/sales.models';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';
import { DatePipe } from '@angular/common';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import {Location} from '@angular/common';
@Component({
  selector: 'app-users-managements',
  templateUrl: './users-managements.component.html',
  styleUrls: ['./users-managements.component.scss']
})
export class UsersManagementsComponent implements OnInit {
	rows = [];
  user_types = [
       {name: "Normal"},
       {name: "Administrador"},
     ];
  permissions_type = [
    {name: "6 horas"},
    {name: "1 día"},
    {name: "1 semana"},
    {name: "Seleccionar fecha Máxima"},
    {name: "Para siempre"},
  ]
  register_data:any = {};
  date_range:string;
  permission_range: string = "";
  public user_modify :any;
  minDate: Date;
	params: any = {};
	modalRef: BsModalRef;
  page = new Page();
  searchTerm$ = new Subject<string>();
  constructor(private localeService: BsLocaleService, private modalService: BsModalService,private configurationService: ConfigurationServiceService, private toastr:ToastrService) {
    this.localeService.use('es');
    this.minDate = new Date();
    this.params.page = 1;
    this.page.size = 10;
    this.listData();
   }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>) {
    // this.sale_selected = sale;
    this.modalRef = this.modalService.show(template);
  }
  listData() {
    this.configurationService.getUserList(this.params)
        .subscribe(
          result => {
            // console.log(result);
            this.rows = result.results;
            this.page.totalElements = result.count;
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }
  registerUser() {
    // console.log(this.register_data);
    this.configurationService.createUser(this.register_data)
        .subscribe(
          result => {
            // console.log(result);
            this.modalRef.hide();
            this.toastr.success("Usuario creado de forma exitosa", "Bien")

            this.listData();
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });

  }
  deleteUser(id:number) {
    if (confirm('¿Está seguro de eliminar este usuario?')){
  this.configurationService.deleteUser(id)
          .subscribe(
              result => {
                this.toastr.success("Usuario eliminado exitosamente", "Bien");
                this.listData();
              },
              error => {
                this.toastr.clear();
                this.toastr.error("Ud no tiene permisos para eliminar usuarios", "Disculpe")
              }
          )
    }
  }
  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }
openModal2(template: TemplateRef<any>, row:any) {
    // this.sale_selected = sale;
    this.modalRef = this.modalService.show(template);
    this.user_modify = row;
  }
  editUserData(row){
    let date_permission:any;
    switch (this.permission_range) {
      case "6 horas":
        date_permission = this.formatDate(new Date().setHours(new Date().getHours()+6));
        break;
      case "1 día":
      date_permission = this.formatDate(new Date().setDate(new Date().getDate()+1));
        break;
      case "1 semana":
      date_permission = this.formatDate(new Date().setDate(new Date().getDate()+7));
        break;
      case "Seleccionar fecha Máxima":
      date_permission = this.formatDate(this.date_range);
        break;
      case "Para siempre":
      date_permission = this.formatDate("9999-01-01");
        break;
      default:
        break;
    }
    console.log(row);
    this.configurationService.updatePermission(date_permission, row)
        .subscribe(
          result => {
            this.modalRef.hide();
            this.toastr.success("Permiso habilitado exitosamente", "Bien")
            this.listData();
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });

  }
  formatDate(date) {
    let date_new = new Date(date)
    let hours:any = date_new.getHours();
    let minutes:any = date_new.getMinutes();
    let month:any = date_new.getMonth()+1;
    hours = hours < 10 ? '0'+hours : hours;
    month = month < 10 ? '0'+month : month;
    minutes = minutes < 10 ? '0'+minutes : minutes;
    let strTime = hours + ':' + minutes;
    return date_new.getFullYear() + "-" + month + "-" + date_new.getDate() + " " + strTime;
}

}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SendDailyOrdersComponent } from './send-daily-orders.component';

describe('SendDailyOrdersComponent', () => {
  let component: SendDailyOrdersComponent;
  let fixture: ComponentFixture<SendDailyOrdersComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SendDailyOrdersComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SendDailyOrdersComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { ConfigurationServiceService } from '../services/configuration-service.service';
@Component({
  selector: 'app-send-daily-orders',
  templateUrl: './send-daily-orders.component.html',
  styleUrls: ['./send-daily-orders.component.scss']
})
export class SendDailyOrdersComponent implements OnInit {
	email_model:any = {};
	emails = "";
  rows = [{
    id:1,
    process:"Ordenes de Mercadolibre Últimos 21 días.",
    description: "Archivo con las ordenes de compra de los últimos 21 días, divididas en ventas por acordar o con etiqueta (envío gratis), filtrada por status de envío 'Lista para enviar' y sub status 'Lista para imprimir etiqueta' ordenadas por fecha descendente, las de envío por acordar filtradas por status pendiente de indicar en la plataforma de mercadolibre la entrega."
  },
  {
    id:2,
    process:"Archivo Descriptivo de Publicaciones Mercadolibre.",
    description: "Archivo con descripción completa de las publicaciones de Mercadolibre (MLC, Título, Id Categoría, Precio, Precio Base, Precio Original, Cantidad Inicial, Cantidad Disponible, Cantidad Vendida, Tipo de Publicación, Permalink, Video, ¿Acepta MercadoPago?, Estado, Tags, Garantía, ¿Envío Gratis?, Precio de Envío)."
  },
  {
    id:3,
    process:"Archivo Descriptivo de Publicaciones Ripley.",
    description: "Archivo con descripción completa de las publicaciones de Ripley."
  },
  {
    id:4,
    process:"Archivo Descriptivo de Publicaciones Linio.",
    description: "Archivo con descripción completa de las publicaciones de Linio."
  },

  {
    id:7,
    process:"Archivo Descriptivo de Publicaciones Falabella.",
    description: "Archivo con descripción completa de las publicaciones de Falabella."
  }


]
  constructor(private configurationService: ConfigurationServiceService, private toastr:ToastrService) {
    this.listData();
   }

  ngOnInit() {
  }
  addEmail(): void {
  	this.configurationService.addToEmailList(this.email_model)
        .subscribe(
          result => {
              this.listData();
              this.email_model.email = '';
              this.toastr.success("Email añadido", "Bien");
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }
  sendMail() {
    if (this.emails.length === 0){
      this.toastr.error("Debe añadir por lo menos un correo a la lista", "Error");
    }else{

    this.configurationService.sendEmailOrders(this.emails)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("Ordenes enviadas de forma exitosa", "Bien");
            }
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
    }
  }
  removeEmail(index):void {
    this.configurationService.removeFromEmailList(index)
    .subscribe(
      result => {
          this.listData();
          this.toastr.success("Email removido de la lista", "Bien");
      },
      error =>{

        this.toastr.clear();
        for (let each in error.error){
          this.toastr.error(error.error[each][0], "Error")
        }
      });
  }

  listData(): void {
    this.configurationService.getEmailList()
        .subscribe(
          result => {
            this.emails = result.filename;
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
    }
    export_excel($event, row_id) {
      switch (row_id) {
        case 1:
          this.configurationService.exportExcel();
          break;
        case 2:
          this.configurationService.exportPublications();
          break;
        case 3:
          this.configurationService.exportPublicationsRipley();
          break;
        case 4:
          this.configurationService.exportPublicationsLinio();
          // code...
          break;
        case 5:
          this.configurationService.exportCompleteReportSales();

          // code...
          break;
        
         case 6:
          this.listData();
          this.configurationService.exportLastReportSales(this.emails);
          // code...
          break;
        case 7:
          this.configurationService.exportFalabellaProducts();
  
            // code...
          break;
         case 8:
           this.configurationService.exportInventoryTotalization();
          break;
        default:
      }
    }
  }



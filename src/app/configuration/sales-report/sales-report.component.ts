import { Component, OnInit } from '@angular/core';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { DatePipe } from '@angular/common';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import {Location} from '@angular/common';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-sales-report',
  templateUrl: './sales-report.component.html',
  styleUrls: ['./sales-report.component.scss']
})
export class SalesReportComponent implements OnInit {
 	date_filter:string = 'today';
 	date_range:any;
 	date_from: string = "";
 	date_to: string = null;
 	locale = 'es';
 	maxDate: Date;
	minDate: Date;
  date_search: string;
	rows = [];
	payment_types = [
		{
			name: "Efectivo",
			prop: "cash"
		},
		{
			name: "TDD",
			prop: "debit_card"

		},
		{
			name: "TDC",
			prop: "credit_card"

		},
		{
			name: "Trans.",
			prop: "transfer"

		},
		{
			name: "Cheque",
			prop: "check"

		},
		{
			name: "Por pagar",
			prop: "to_pay"

		},
		{
			name: "Sin pago",
			prop: "no_pay"

		},
		{
			name: "T. Retail",
			prop: "retail_transfer"

		},
	]
  constructor(private localeService: BsLocaleService, private configurationService: ConfigurationServiceService, private toastr:ToastrService) { 
  	this.localeService.use('es');
  	// this.maxDate = new Date();
  	// this.minDate = new Date(2018, 11, 1);
  	this.date_from = this.formatDate(new Date());
  	this.listData();
  }

  ngOnInit() {
	registerLocaleData( es );
  }
  filterType(event) {
  	switch (this.date_filter) {
  		case "today":
  			this.date_from = this.formatDate(new Date());
  			this.date_to = null;
  			break;

  		case "yesterday":
  			this.date_from = this.formatDate(new Date().setDate(new Date().getDate()-1));
  			this.date_to = null;
  			break;
  		case "last_7":
  			this.date_from = this.formatDate(new Date().setDate(new Date().getDate()-7));
  			this.date_to = this.formatDate(new Date());;
  			break;
  		case "last_30":
  			this.date_from = this.formatDate(new Date().setDate(new Date().getDate()-35));
  			this.date_to = this.formatDate(new Date());;
  			break;
  		
  		default:
  			return
  	}
  	this.listData();
  }
  filterRange(event){
  	console.log("lkjhgf")
  	if (this.date_range !== undefined){
			this.date_from = this.formatDate(this.date_range[0])
			this.date_to = this.formatDate(this.date_range[1])
  			this.listData();

		}

  }
  listData() {
  	this.configurationService.getReportData(this.date_from, this.date_to)
        .subscribe(
          result => {
            console.log(result);
            this.rows = result.sales;
            this.date_search = result.date_search;
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }
  formatDate(date) {
    var d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
}
generateReport(event) {
  this.configurationService.generateReportSales(this.date_from, this.date_to)
            .subscribe(
                result => {
                  console.log(result);
                  this.toastr.success("Archivo generado extosamente", "Bien");
                  const downloadLink = document.createElement("a");
                  const fileName = "Cuadratura de caja" + ".pdf";
                  const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];

                    let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                        'resizable,screenX=10,screenY=10,width=1050,height=1050';

                    let htmlPop = '<embed width=100% height=100%'
                                     + ' type="application/pdf"'
                                     + ' src="data:application/pdf;base64,'
                                     + escape(result['pdf_file'])
                                     + '"></embed>'; 

                    let printWindow = window.open ("", "PDF", winparams);
                    printWindow.document.write(htmlPop);
                    // downloadLink.href = linkSource;
                    // downloadLink.download = fileName;
                    // downloadLink.click();

                },
                error => {
                  this.toastr.clear();
                  for (let each in error.error){
                    this.toastr.error(error.error[each][0], "Error")
                  }
                }
            )
}

}

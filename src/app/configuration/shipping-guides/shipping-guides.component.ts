import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { Page } from '../../sales/models/sales.models';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SalesServiceService } from '../../sales/services/sales-service.service';
import { StockServiceService } from '../../stock/services/stock-service.service';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-shipping-guides',
  templateUrl: './shipping-guides.component.html',
  styleUrls: ['./shipping-guides.component.scss']
})
export class ShippingGuidesComponent implements OnInit {
  @ViewChild('myTable') table: any;
  modalRef: BsModalRef;
  row_selected:any;
  expanded: any = {};
  searchTerm$ = new Subject<string>();
  editing = {};
  sku:any;
  quantity:number;
  rows=[];
  params: any = {};
  page = new Page();

  constructor(private configurationService: ConfigurationServiceService, private toastr:ToastrService, private modalService: BsModalService, private saleService: SalesServiceService, private stockService:StockServiceService) { 
  	this.params.page = 1;
    this.page.size = 10;
  	this.listData();
  	this.stockService.searchRetirement(this.searchTerm$)
      .subscribe(result => {
        // this.params.search = 
        this.rows = result['results'];
        this.page.pageNumber = 0;

        this.page.totalElements = result['count'];
        return result;
      });
  }

  toggleExpandRow(row) {
    this.table.rowDetail.toggleExpandRow(row);
    
  }

  ngOnInit() {
  }
  listData() {
	this.configurationService.getRetirementList(this.params).subscribe(
      result=> {
        this.rows = result.results
          this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
}
setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }

  openModal(template: TemplateRef<any>, row:any){
    // this.sku = "";
    this.row_selected = row;
    this.modalRef = this.modalService.show(template);

  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.row_selected.products_retirement[rowIndex][cell] = event.target.value;
    this.row_selected.products_retirement = [...this.row_selected.products_retirement];
  }
  addProduct(): void{
  	if (this.sku != null && Number(this.sku) > 999999){
  		this.saleService.findProductDescription(this.sku).subscribe(
  			result => {
          if (result.length == 0){
            this.toastr.error("No existe producto con SKU " + this.sku, "No existe");
          }else{
            // console.log(result);
            this.row_selected.products_retirement.push({
            	product_sale__sku:result[0].sku,
  				product_sale__description:result[0].description,
  				product_code__code_seven_digits:result[0].code_seven_digits,
  				product_code__color__description:result[0].color,
  				quantity:this.quantity,
  				unit_price:1,
  				total:1 * this.quantity,
            });
            this.sku = "";
            this.quantity = 1;
            this.row_selected.products_retirement = [...this.row_selected.products_retirement];
            // this.register.total = 0;
          }
          },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al añadir el producto", "Error");
          })
  	}
  	else {

  		this.toastr.error("Indique numero de SKU de 7 dígitos para poder añadir producto", "Error");
  	}
  }
  removeProduct(index): void{
    if (confirm('¿Desea eliminar este producto?')){
      this.row_selected.products_retirement.splice(index, 1);
      this.row_selected.products_retirement = [...this.row_selected.products_retirement];

    }
  }

  updateRetirement(){
  	this.stockService.updateRetirement(this.row_selected).subscribe(
      result=> {
        this.toastr.success("Retiro creado exitosamente", "Bien");
        this.modalRef.hide();
        this.listData();

      return result;
      },
      error=> {
        this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
      }
    )
  }
  approvedRetirement(){
    this.stockService.approvedRetirement(this.row_selected).subscribe(
      result=> {
        this.toastr.success("Retiro creado exitosamente", "Bien");
        this.modalRef.hide();
        this.listData();

      return result;
      },
      error=> {
        this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
      }
    )
  }

}

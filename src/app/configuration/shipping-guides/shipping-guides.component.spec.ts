import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ShippingGuidesComponent } from './shipping-guides.component';

describe('ShippingGuidesComponent', () => {
  let component: ShippingGuidesComponent;
  let fixture: ComponentFixture<ShippingGuidesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ShippingGuidesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ShippingGuidesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

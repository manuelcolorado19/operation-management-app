import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Observable, throwError, interval } from 'rxjs';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { of, BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';

@Injectable({
  providedIn: 'root'
})
export class ConfigurationServiceService {
public headers: HttpHeaders = new HttpHeaders();
  token: string;
  internal_params: any = {};
  constructor(
  	 public http: HttpClient,
  	 ) { 
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
    }
    this.internal_params.page = 1;
    this.internal_params.pack = '3';


  }

  sendEmailOrders(emails:any): any {
  return this.http.post(url + 'send/orders/xlsx', emails, {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }

  getAutomaticMessages(): any {
    return this.http.get(url + 'automatic_message/', {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }

  getListMessages(params:any): any {
    this.internal_params.page = params.page;
    this.internal_params.pack = params.pack;
    return this.http.get(url + 'custom_message_product/', {headers:this.headers, params:params})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }
  updateMessage(data:any) {
    return this.http.put(url + 'automatic_message/' + data.id +"/", data, {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }

  getEmailList(): any {
    return this.http.get(url + 'last-report-sale/', {headers:this.headers, params:{file_name:"true"}})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }

  addToEmailList(email): any {
  return this.http.post(url + 'email_receiver/', email, {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }

  removeFromEmailList(email_id:number): any {
  return this.http.delete(url + 'email_receiver/' + email_id + "/", {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }
  updateCustomMessage(data:any) {
    return this.http.put(url + 'custom_message_product/' + data.id +"/", data, {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }
  searchMessage(terms: Observable<string>, params: any) { 
    return terms.debounceTime(650)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchMessages(term, params));
 }

 searchMessages(term, params) {
    return this.http.get(url + 'custom_message_product/',{params:{search:term, page:this.internal_params.page, pack:this.internal_params.pack},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }

  exportExcel() {
    return this.http.get(url+'export/orders/xlsx', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Ordenes del día"))
  
  }
  exportCompleteReportSales(){
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'export/sales/xlsx', {headers: this.headers, params:{all_columns:"hola"}, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Reporte de Ventas " + dformat))
  }
  exportPublications() {
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'export/publications/xlsx', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Publicaciones Ml " + dformat))
  
  }
  exportPublicationsRipley() {
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'export/publications-ripley/xlsx', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Publicaciones Ripley "+ dformat))
  
  }

  exportPublicationsLinio() {
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'export/publications-linio/xlsx', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Publicaciones Linio "+ dformat))
  
  }
  exportLastReportSales(file_server_name){
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');

    return this.http.get(url+'last-report-sale/', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file_server_name))
  }

  exportFalabellaProducts(){
    let d = new Date,
    dformat = [d.getFullYear(),
      d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
      d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
     [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
      d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'falabella-products-report', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Publicaciones Falabella "+dformat))
  }
  exportInventoryTotalization(){
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'export/totalization/xlsx', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Totalización de Productos "+ dformat))
  }
  
  downLoadFile(data: any, type: string, file_name:string):any{
    
        // console.log(yyuu)
        const blob = new Blob([data], { type: type});
        const fileName: string = file_name +".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

    getReportList(params:any){
      return this.http.get(url + 'sales_report/', {params:params, headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
    }
    getReportFile(id:number){
      return this.http.get(url + 'sales_report/' + id + "/", {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
    }
    getReportData(date_from:string, date_to:string): any {
      // console.log(date_from);
      let params = {date_from:date_from, date_to:date_to}
      if (date_to == null) {
        delete params.date_to;
      }
    return this.http.get(url + 'sales/report/cash', {headers:this.headers, params: params})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }
  generateReportSales(date_from:string, date_to:string){
    let data = {date_from:date_from, date_to:date_to}
      if (date_to == null) {
        delete data.date_to;
      }
     return this.http.post(url+'sales/report/cash', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }

    getVideos(): any {
    
    return this.http.get(url + 'video/', {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }

  createUser(data:any){
     return this.http.post(url+'users/', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }
    getUserList(params:any): any {
    
    return this.http.get(url + 'users/', {params:params, headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }
  deleteUser(id:number){
    return this.http.delete(url+'users/'+id + "/", {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  updatePermission(date_string:string, data){
    let data_request = {superuser_until:date_string}
    return this.http.patch(url + 'users/' + data.id +"/", data_request, {headers:this.headers})
             .pipe(
               map((response: any) => {
                 return response
               })
          )
  }
  getReportDataChannel(filter_type:string){
    return this.http.get(url + 'sales/report/channel', {params:{filter_type:filter_type}, headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
  }
  getReportXLSX(){
    return this.http.post(url + 'sales/report/channel', {} ,{headers:this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadChannelReport(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",))
    
  }
   downLoadChannelReport(data: any, type: string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Informe de Ventas (Canales) Asiamerica.xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
    getActivityList(){
      return this.http.get(url + 'report-activity/', {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
    }

    saveActivities(data:any){
     return this.http.post(url+'report-activity/', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }
    getReportSupervisorList(){
      return this.http.get(url + 'report-supervisor/', {headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
    }
    exportExcelSupervisor(){
      return this.http.get(url + 'generate-report/', {headers:this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadSupervisorReport(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet",))
    }
    downLoadSupervisorReport(data: any, type: string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Informe de Supervisión de actividades.xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

    uploadFiles(files: File[]):any {  
    const formData: FormData = new FormData();
  
    for (let i = 0; i < files.length; i++) {
      const file: File = files[i];
      formData.append('file' + (i + 1), file, file.name);
      // console.log(formData);
    }
    return this.http.post(url + 'merge-labels/', formData, {headers:this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadLabels(response, "application/pdf",))
  
  }
  downLoadLabels(data: any, type: string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = "Etiquetas.pdf";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
    getRetirementList(params:any){
      return this.http.get(url + 'retirement/', {params:params, headers:this.headers})
         .pipe(
           map((response: any) => {
             return response
           })
      )
    }
   searchManifiesto(terms: Observable<string>, params: any) { 
    return terms.debounceTime(650)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchMani(term, params));
 }

 searchMani(term, params) {
    return this.http.get(url + 'sales_report/',{params:{search:term},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
  }


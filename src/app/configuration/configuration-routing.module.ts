import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { ConfigurationComponent } from './configuration.component';
import { SendDailyOrdersComponent } from './send-daily-orders/send-daily-orders.component';
import { ChangeMessagesComponent } from './change-messages/change-messages.component';
import { UsersManagementsComponent } from './users-managements/users-managements.component';
import { HistoricReportComponent } from './historic-report/historic-report.component';
import { SalesReportComponent } from './sales-report/sales-report.component';
import { ProceduresComponent } from './procedures/procedures.component';
import { ChannelReportSalesComponent } from './channel-report-sales/channel-report-sales.component';
import { AdminGuard } from '../sales/guards/admin.guard' 
import { SupervisorActivitiesComponent } from './supervisor-activities/supervisor-activities.component';
import { SupervisionHistoricComponent } from './supervision-historic/supervision-historic.component';
import { UploadLabelsFalabellaComponent } from './upload-labels-falabella/upload-labels-falabella.component';
import { ShippingGuidesComponent } from './shipping-guides/shipping-guides.component';

const routes: Routes = [{
  path: '',
  component: ConfigurationComponent,
  children: [
    {
      path: 'send-daily-orders',
      component: SendDailyOrdersComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Enviar Ordenes de Compra' }
    },
    {
      path: 'change-message',
      component: ChangeMessagesComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Cambiar Mensaje Automático' }
    },
    {
      path: 'users-management',
      component: UsersManagementsComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Gestión de Usuarios' }
    },
    {
      path: 'historic-report',
      component: HistoricReportComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Histórico Manifiestos' }
    },
    {
      path: 'upload-labels',
      component: UploadLabelsFalabellaComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Unificado de etiquetas' }
    },
    {
      path: 'sales-report',
      component: SalesReportComponent,
      canActivate: [AdminGuard],
      data: { title: 'Asiamerica | Reportes de ventas' }
    },
    {
      path: 'supervisor',
      component: SupervisorActivitiesComponent,
      canActivate: [AdminGuard],
      data: { title: 'Asiamerica | Actividades' }
    },
    {
      path: 'supervisor-historic',
      component: SupervisionHistoricComponent,
      canActivate: [AdminGuard],
      data: { title: 'Asiamerica | Supervisión Histórica' }
    },
    {
      path: 'procedures',
      component: ProceduresComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Procedimientos' }
    },
    // {
    //   path: 'channel-report-sales',
    //   component: ChannelReportSalesComponent,
    //   canActivate: [AdminGuard],
    //   data: { title: 'Asiamerica | Reporte de ventas por Canal' }
    // },
    {
      path: 'shipping-guides',
      component: ShippingGuidesComponent,
      canActivate: [AdminGuard],
      data: { title: 'Asiamerica | Guias de Retiro' }
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class ConfigurationRoutingModule { }

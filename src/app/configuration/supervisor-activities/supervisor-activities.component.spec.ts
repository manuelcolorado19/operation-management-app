import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisorActivitiesComponent } from './supervisor-activities.component';

describe('SupervisorActivitiesComponent', () => {
  let component: SupervisorActivitiesComponent;
  let fixture: ComponentFixture<SupervisorActivitiesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisorActivitiesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisorActivitiesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

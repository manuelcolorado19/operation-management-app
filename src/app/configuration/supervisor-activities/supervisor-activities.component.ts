import { Component, OnInit, TemplateRef } from '@angular/core';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Activity } from './../models/configuration.models';
@Component({
  selector: 'app-supervisor-activities',
  templateUrl: './supervisor-activities.component.html',
  styleUrls: ['./supervisor-activities.component.scss']
})
export class SupervisorActivitiesComponent implements OnInit {
	rows = [];
	editing = {};
  modalRef: BsModalRef;
  mytime: Date = new Date();
  changes = [];
  new_activity:Activity = new Activity();
  selected = [];
  report_id: number;

  constructor(private configurationService: ConfigurationServiceService, private toastr:ToastrService, private modalService: BsModalService) {
  	this.listData();
   }

  ngOnInit() {
  }
  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
    // if (this.changes.includes(this.rows[rowIndex])){
    //   this.changes.splice(rowIndex, 1);
    //   this.changes.push(this.rows[rowIndex]);

    // }else{
      this.changes.push(this.rows[rowIndex]);
    // }
    console.log(this.changes);
    this.saveChanges();
  }
  listData() {
	this.configurationService.getActivityList().subscribe(
      result=> {
        this.rows = result
        this.report_id = result[0].report_supervisor
        console.log(result);
          // this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
}
addActivity(){
  this.modalRef.hide()
  this.new_activity.hour = this.formatDate(this.new_activity.hour);
	this.rows.push(this.new_activity);
	this.rows = [...this.rows];
  this.changes.push(this.new_activity);
  this.saveChanges();
  this.new_activity = new Activity();
}
openModal(template: TemplateRef<any>,) {
    this.modalRef = this.modalService.show(template);
  }

formatDate(date) {
    let d = new Date(date),
        hour = '' + (d.getHours() + 1),
        minute = '' + d.getMinutes()

    if (hour.length < 2) hour = '0' + hour;
    if (minute.length < 2) minute = '0' + minute;

    return [hour, minute].join(':');
  }
  saveChanges(){
    let data = {activities: this.changes, report_id: this.report_id}
    // console.log(data);

    this.configurationService.saveActivities(data)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("Actividad actualizada de forma exitosa", "Bien");
              this.listData();
              this.changes = [];

            }
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }
  getRowClass(row) {
    // console.log(row.status_activity);
    return {
      'complete': (row.status_activity == 'yes'),
    };
  }
  
}

import { Component, OnInit, EventEmitter, ViewChild,  ElementRef} from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';
import { url } from '../../../global';
import { ConfigurationServiceService } from '../services/configuration-service.service';

@Component({
  selector: 'app-upload-labels-falabella',
  templateUrl: './upload-labels-falabella.component.html',
  styleUrls: ['./upload-labels-falabella.component.scss']
})
export class UploadLabelsFalabellaComponent implements OnInit {
url = 'http://localhost:4900/upload';
  formData: FormData;
  files: File[];
  @ViewChild('file_input') 
 file_input: ElementRef;
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  options: UploaderOptions;
  constructor(private configurationService: ConfigurationServiceService) { 
  	this.options = { concurrency: 1, maxUploads: 100,
  	allowedContentTypes: [
          "application/pdf",
        ]
 };
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  }

  ngOnInit() {
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      const file: File = output.file.nativeFile;
      // check file types
      if (file.type && file.type === 'application/pdf') {
        this.files.push(output.file.nativeFile);
        // console.log(output.file.nativeFile);
      } else {
        // display notification
      }
    }
  }

  handleFileInput(newfiles: FileList): void {
  
    // Future suported types: .png, .jpg
    for (let i = 0; i < this.files.length; i++) {
      const file: File = this.files[i];
      // make some checks for files
      this.files.push(file);
    }
  }

  startUpload(){
  	// console.log(this.files);
  	this.configurationService.uploadFiles(this.files);
  }

removeAllFiles(): void {
    this.files = [];
    this.file_input.nativeElement.value = "";

  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadLabelsFalabellaComponent } from './upload-labels-falabella.component';

describe('UploadLabelsFalabellaComponent', () => {
  let component: UploadLabelsFalabellaComponent;
  let fixture: ComponentFixture<UploadLabelsFalabellaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadLabelsFalabellaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadLabelsFalabellaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit } from '@angular/core';
import { DatePipe } from '@angular/common';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import {Location} from '@angular/common';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-channel-report-sales',
  templateUrl: './channel-report-sales.component.html',
  styleUrls: ['./channel-report-sales.component.scss']
})
export class ChannelReportSalesComponent implements OnInit {
  data_visualization:string;
  filter_type:string;
 	locale = 'es';
  date_search: string;
  sort_data:string = 'date_format'
	rows = [];
  channels = [
    {
      name: "Mercadolibre",
      prop: "mercadolibre_total"
    },
    {
      name: "Ripley",
      prop: "ripley_total"

    },
    {
      name: "Falabella",
      prop: "falabella_total"

    },
    {
      name: "Paris",
      prop: "paris_total"

    },
    {
      name: "Groupon",
      prop: "groupon_total"

    },
    {
      name: "Mercadoshops",
      prop: "mercadoshops_total"

    },
    {
      name: "Yapo.cl",
      prop: "yapo_total"

    },
    {
      name: "Presencial",
      prop: "presencial_total"

    },
    {
      name: "Linio",
      prop: "linio_total"

    },
    {
      name: "Extra ML",
      prop: "extra_ml_total"

    },
    {
      name: "Facebook",
      prop: "facebook_total"

    },
    {
      name: "Instagram",
      prop: "instagram_total"
    },
  ]

// single: any[];
  // multi: any[];

  view: any[] = [1300, 600];

  // options
  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = "Leyenda";
  showXAxisLabel = true;
  xAxisLabel = 'Tiempo';
  showYAxisLabel = true;
  yAxisLabel = 'Ventas';

  colorScheme = {
    domain: ['#fff159', '#1a1a1a', '#abd500', '#f50', '#00b0eb', '#53a318', '#f7323f', '#fb7300', '#009e47', '#1f4e96', '#8b9dc3','#fb3958']
  };
  autoScale = true;
  multi = [];

  constructor(private localeService: BsLocaleService, private configurationService: ConfigurationServiceService, private toastr:ToastrService) { 
  	this.localeService.use('es');
    this.data_visualization = "table";
    this.filter_type = "today";
    this.listData();
     // Object.assign(this, {this.single: this.multi}) 
  }

  ngOnInit() {
  }

  generateReport(event) {
    this.configurationService.getReportXLSX();
  }

  listData() {
    this.configurationService.getReportDataChannel(this.filter_type)
        .subscribe(
          result => {
            console.log(result);
            this.rows = result.sales;
            this.multi = result.data_chart;
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }

  filterType(event){
    switch (this.filter_type) {
      case "week":
        this.sort_data = 'week_number'
        break;
      case "today":
        this.sort_data = 'date_format'
        break;
      case "year":
        this.sort_data = 'year_format'
        break;
      
      default:
        this.sort_data = 'date_format'
        break;
    }
    this.listData();
  }

}

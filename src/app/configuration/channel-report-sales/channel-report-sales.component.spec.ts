import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelReportSalesComponent } from './channel-report-sales.component';

describe('ChannelReportSalesComponent', () => {
  let component: ChannelReportSalesComponent;
  let fixture: ComponentFixture<ChannelReportSalesComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelReportSalesComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelReportSalesComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { SupervisionHistoricComponent } from './supervision-historic.component';

describe('SupervisionHistoricComponent', () => {
  let component: SupervisionHistoricComponent;
  let fixture: ComponentFixture<SupervisionHistoricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ SupervisionHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(SupervisionHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

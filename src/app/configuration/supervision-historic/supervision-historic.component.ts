import { Component, OnInit } from '@angular/core';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-supervision-historic',
  templateUrl: './supervision-historic.component.html',
  styleUrls: ['./supervision-historic.component.scss']
})
export class SupervisionHistoricComponent implements OnInit {
	rows = [];

  constructor(private configurationService: ConfigurationServiceService, private toastr:ToastrService,) {
  	this.listData();
   }

  ngOnInit() {
  }
  listData() {
	this.configurationService.getReportSupervisorList().subscribe(
      result=> {
        this.rows = result.results
        console.log(result);
          // this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
}
export_excel($event) {
      this.configurationService.exportExcelSupervisor();
    }

}

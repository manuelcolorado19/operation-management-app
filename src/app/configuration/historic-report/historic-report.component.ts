import { Component, OnInit } from '@angular/core';
import { ConfigurationServiceService } from '../services/configuration-service.service';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../sales/models/sales.models';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-historic-report',
  templateUrl: './historic-report.component.html',
  styleUrls: ['./historic-report.component.scss']
})
export class HistoricReportComponent implements OnInit {

  rows = []
  page = new Page();
  params:any = {};
  searchTerm$ = new Subject<string>();

  constructor(private configurationService: ConfigurationServiceService, private toastr:ToastrService) {
    this.params.page = 1;
    // this.page.pageNumber = 0;
    this.page.size = 10;
  	this.listData();
    this.configurationService.searchManifiesto(this.searchTerm$, this.params)
      .subscribe(result => {
        // this.params.search = 
        this.rows = result['results'];
        this.page.pageNumber = 0;

        this.page.totalElements = result['count'];
        return result;
      });
   }
  ngOnInit() {
  }

downloadFile(row_id) {
	console.log(row_id);
	this.configurationService.getReportFile(row_id).subscribe(
      result=> {
        // this.rows = result.results
        const downloadLink = document.createElement("a");
        const fileName = result.report_file_name + ".pdf";
        const linkSource = 'data:application/pdf;base64,' + result.pdf;
      downloadLink.href = linkSource;
      downloadLink.download = fileName;
      downloadLink.click();
      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
}
listData() {
	this.configurationService.getReportList(this.params).subscribe(
      result=> {
        this.rows = result.results
          this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
}
setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    // console.log(this.params)
    this.listData();

  }
}

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { HistoricReportComponent } from './historic-report.component';

describe('HistoricReportComponent', () => {
  let component: HistoricReportComponent;
  let fixture: ComponentFixture<HistoricReportComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ HistoricReportComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(HistoricReportComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

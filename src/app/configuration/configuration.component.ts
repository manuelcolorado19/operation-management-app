import { Component } from '@angular/core';

@Component({
  selector: 'rm-configuration',
  //styleUrls: ['./auth.component.css'],
  template: `
  	<app-sidebar></app-sidebar>
    <router-outlet></router-outlet>
  `,
})
export class ConfigurationComponent {
}

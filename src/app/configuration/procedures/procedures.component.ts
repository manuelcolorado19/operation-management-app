import { Component, OnInit, TemplateRef, EventEmitter } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr'
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';

import { ConfigurationServiceService } from '../services/configuration-service.service';
@Component({
  selector: 'app-procedures',
  templateUrl: './procedures.component.html',
  styleUrls: ['./procedures.component.scss']
})
export class ProceduresComponent implements OnInit {
	modalRef: BsModalRef;
	formData: FormData;
	files: UploadFile[];
	url = "";
	uploadInput: EventEmitter<UploadInput>;
	humanizeBytes: Function;
	channel_selected:any = {};
	cards_data = [];
  constructor(private modalService: BsModalService, private configurationService: ConfigurationServiceService, private toastr:ToastrService) { 
  	this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
    this.getListOfVideo();
  }

  ngOnInit() {
  }
  openModal(template: TemplateRef<any>, data:any) {
  	// console.log(data);
  	this.channel_selected = data;
    this.modalRef = this.modalService.show(template);
  }

  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'allAddedToQueue') {
      // const event: UploadInput = {
      //   type: 'uploadAll',
      //   url: this.url,
      //   method: 'POST',
      //   data: { foo: 'bar' }
      // };

      // this.uploadInput.emit(event);
    } else if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      this.files.push(output.file);
    } else if (output.type === 'uploading' && typeof output.file !== 'undefined') {
      const index = this.files.findIndex(file => typeof output.file !== 'undefined' && file.id === output.file.id);
      this.files[index] = output.file;
    } else if (output.type === 'cancelled' || output.type === 'removed') {
      this.files = this.files.filter((file: UploadFile) => file !== output.file);
    
    } else if (output.type === 'rejected' && typeof output.file !== 'undefined') {
      console.log(output.file.name + ' rejected');
    }

    this.files = this.files.filter(file => file.progress.status !== UploadStatus.Done);
  }

  startUpload(id:number): void {
    const event: UploadInput = {
      type: 'uploadAll',
      url: "http://localhost:8000/video/" + id + "/",
      fieldName: "videofile",
      method: 'PUT',
    };

    this.uploadInput.emit(event);
    this.toastr.success("Video cargado exitosamente", "Bien");

  }

  getListOfVideo(){
    this.configurationService.getVideos().subscribe(
      result=> {
        // this.rows = result.results
        console.log(result);
        this.cards_data = result.results;
      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }
  

}

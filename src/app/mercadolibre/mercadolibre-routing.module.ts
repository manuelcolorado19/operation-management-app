import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';
import { MercadolibreComponent } from './mercadolibre.component';
import { QuestionsAnswersComponent } from './questions-answers/questions-answers.component';
import { MetricsIndicatorsComponent } from './metrics-indicators/metrics-indicators.component';
const routes: Routes = [{
  path: '',
  component: MercadolibreComponent,
  children: [
    {
      path: 'questions-answers',
      component: QuestionsAnswersComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Panel de Preguntas y Respuestas' }
    }, 
    {
      path: 'metrics-indicators',
      component: MetricsIndicatorsComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Metricas & KPIS' }
    }, 
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}]

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class MercadolibreRoutingModule { }

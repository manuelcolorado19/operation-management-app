import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';
import { Observable } from 'rxjs/Observable';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class MercadolibreServiceService {
  token: string;
  public headers: HttpHeaders = new HttpHeaders();
  constructor(public http: HttpClient,) {
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
  	}
   }

   listQuestions(params:any): any {   
    return this.http.get(url+'question/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
                return response
             })
        )
  }

  getAnswerTimeMetrics():any{
      return this.http.get(url+'period/',{headers:this.headers})
              .pipe(
                map((response:any)=>{
                  return response
                })
              );
  }
  answerQuestion(data:any){
    return this.http.post(url+'answer/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  newIntention(data:any){
    return this.http.post(url+'intention-robot/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  questionAnswerFile(){
    let d = new Date,
    dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');
    return this.http.get(url+'question-file/', {headers: this.headers,  responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", "Informe de Preguntas ML " + dformat))
  }

   downLoadFile(data: any, type: string, file_name:string):any{
        const blob = new Blob([data], { type: type});
        const fileName: string = file_name +".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
}

import { TestBed } from '@angular/core/testing';

import { MercadolibreServiceService } from './mercadolibre-service.service';

describe('MercadolibreServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: MercadolibreServiceService = TestBed.get(MercadolibreServiceService);
    expect(service).toBeTruthy();
  });
});

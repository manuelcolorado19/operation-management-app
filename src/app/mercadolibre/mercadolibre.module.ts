import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { MercadolibreComponent } from './mercadolibre.component';
import { LayoutModule } from '../@layout/layout.module'
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';

import { MercadolibreRoutingModule } from './mercadolibre-routing.module';
import { QuestionsAnswersComponent } from './questions-answers/questions-answers.component';
import { MetricsIndicatorsComponent } from './metrics-indicators/metrics-indicators.component';
import {NgxChartsModule} from '@swimlane/ngx-charts';
@NgModule({
  imports: [
    CommonModule,
    MercadolibreRoutingModule,
    LayoutModule,
    NgxDatatableModule,
    FormsModule,
    ModalModule.forRoot(),
    NgxChartsModule
  ],
  declarations: [MercadolibreComponent, QuestionsAnswersComponent, MetricsIndicatorsComponent]
})
export class MercadolibreModule { }

import { Component, OnInit, TemplateRef } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { ToastrService } from 'ngx-toastr';
import { MercadolibreServiceService } from '../services/mercadolibre-service.service';
import * as moment from 'moment';
import { Page } from '../../sales/models/sales.models';
import * as $ from 'jquery';
@Component({
  selector: 'app-questions-answers',
  templateUrl: './questions-answers.component.html',
  styleUrls: ['./questions-answers.component.scss']
})
export class QuestionsAnswersComponent implements OnInit {
	modalRef: BsModalRef;
  modalRef2: BsModalRef;
	row_selected:any;
	params:any = {};
  status:string = 'pending';
  answer_data:any = {};
  selected_words:any = [];
  all_intentions:any = [];
  intentions_question:any = [];
  training:any = {};
  new_intention: string='';
  unanswer:Number = 0;
  page = new Page();
  word_splits:string;
  concat_data:string ='';
	rows = [];
  constructor(private modalService: BsModalService,private mercadolibreService:MercadolibreServiceService, private toastr:ToastrService,) {
  		moment.locale('es')
      // this.params.is_answer = 3;
      this.params.status_question = "pending";
      this.params.page = 1;
      this.page.pageNumber = 0;
  		this.listData();
   }

  ngOnInit() {
    
  }

  setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }
  listData(){
    this.mercadolibreService.listQuestions(this.params).subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.rows = result.results;
          this.page.totalElements = result.count;
          this.unanswer = result.unanswer;
          for (let i of this.rows){
          	i.date_created = moment(i.date_created).startOf('minute').fromNow(true);
            
          }
          // console.log(this.rows);
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }

  openModal(template: TemplateRef<any>, row:any) {
    this.modalRef = this.modalService.show(template);
    // this.answer_data = row;
    this.row_selected = row;
    if (this.row_selected.suggested_answer !== null){
      this.row_selected.answer = this.row_selected.suggested_answer;
    }
    this.word_splits = this.row_selected.text.replace(/[.,\/#!$%\^&\*;:{}=\-_`~()¿?]/g,"").split(" ") 
    this.training.selected_words = this.row_selected.key_words;
    this.all_intentions = this.row_selected.all_intentions;
    this.training.intentions_question = this.row_selected.intentions_question;
    this.training.question_id = this.row_selected.id;
  }
  openModal2(template: TemplateRef<any>) {
    this.modalRef2 = this.modalService.show(template, { class: 'w-25 nested-modal' });
  }
  statusQuestion(status:string){
    this.params.page = 1;
    this.page.pageNumber = 0;

    this.status = status;
    switch (status) {
      case "answered":
        this.params.is_answer = "";
        this.params.suggested_answer = "";
        this.params.answer_question__is_robot = "";
        this.params.status_question = "answered";
        // code...
        break;
      case "pending":
        this.params.is_answer = "";
        this.params.suggested_answer = "";
        this.params.answer_question__is_robot = "";
        this.params.status_question = "pending";
        // code...
        break;
      case "delete_by_mercadolibre":
        this.params.is_answer = "";
        this.params.suggested_answer = "";
        this.params.answer_question__is_robot = "";
        this.params.status_question = "delete_by_mercadolibre";
        // code...
        break;
      case "training":
        this.params.status_question = "";
        this.params.is_answer = 2;
        this.params.suggested_answer = 2;
        this.params.answer_question__is_robot = 3;
        // code...
        break;
      
      default:
        // code...
        break;
    }
    
    this.listData();

  }
  selectTech(tech_value:string, field_value:string){
    this.concat_data += (field_value + ": " + tech_value) + "\n";
    // console.log(this.concat_data);
    this.row_selected.answer = this.concat_data;

  }
  sendAnswer(){
    let data = {
      question:this.row_selected.id,
      mercadolibre_id_question:this.row_selected.mercadolibre_id_question,
      answer:this.row_selected.answer,
    };
    // console.log(data);
    this.mercadolibreService.answerQuestion(data)
        .subscribe(
          result => {
            if(result == true){
              this.modalRef.hide()
              this.listData();
              this.toastr.success("Respuesta enviada satisfactoriamente", "Bien");
              // this.register.products = [];
            }
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }

  appendWord(word){
    this.training.selected_words.push(word);
  }
  removeWord(word){
    this.training.selected_words.splice(this.training.selected_words.indexOf(word), 1);
  }
  addNewIntention(){
    if (this.new_intention != ''){
      this.all_intentions.push(this.new_intention);
      this.training.intentions_question.push(this.new_intention);
      this.new_intention = '';
    }
  }
  appendIntention(word){
    // this.selected_words.push(word);
    this.training.intentions_question.push(word);
  }
  removeIntention(word){
    this.training.intentions_question.splice(this.training.intentions_question.indexOf(word), 1);
  }
  sendDataIntention(){
    console.log(this.training)
    this.mercadolibreService.newIntention(this.training)
        .subscribe(
          result => {
            if(result == true){
              this.modalRef.hide()
              // this.listData();
              this.toastr.success("Respuesta enviada satisfactoriamente", "Bien");
              // this.register.products = [];
            }
          },
          error =>{

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }
}

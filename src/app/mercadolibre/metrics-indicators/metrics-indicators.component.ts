import { Component, OnInit } from '@angular/core';
import { MercadolibreServiceService } from '../services/mercadolibre-service.service';

@Component({
  selector: 'app-metrics-indicators',
  templateUrl: './metrics-indicators.component.html',
  styleUrls: ['./metrics-indicators.component.scss']
})
export class MetricsIndicatorsComponent implements OnInit {
  rows = []
  data_chart:any = [];

  view: any[] = [1300, 600];
  sort_data:string = 'date_search'

  showXAxis = true;
  showYAxis = true;
  gradient = false;
  showLegend = true;
  legendTitle = "Leyenda";
  showXAxisLabel = true;
  xAxisLabel = 'Tiempo (Días)';
  showYAxisLabel = true;
  yAxisLabel = 'Periodo (Minutos)';

  colorScheme = {
    domain: ['#fff159', '#1a1a1a', '#abd500', '#f50', '#00b0eb', '#53a318', '#f7323f', '#fb7300', '#009e47', '#1f4e96', '#8b9dc3','#fb3958']
  };
  autoScale = true;
  constructor(private mercadolibreService:MercadolibreServiceService) { 
    this.listData();
  }

  ngOnInit() {
  } 

  listData(){
    this.mercadolibreService.getAnswerTimeMetrics().subscribe(
      result => {
        this.rows = result.data_table;
        this.data_chart = result.data_chart;
        return result;
      }
    )
  }
  generateReportQuestions($event){
    this.mercadolibreService.questionAnswerFile();
  }
}

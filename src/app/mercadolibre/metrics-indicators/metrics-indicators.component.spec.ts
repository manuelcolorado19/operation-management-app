import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MetricsIndicatorsComponent } from './metrics-indicators.component';

describe('MetricsIndicatorsComponent', () => {
  let component: MetricsIndicatorsComponent;
  let fixture: ComponentFixture<MetricsIndicatorsComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MetricsIndicatorsComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MetricsIndicatorsComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { MercadolibreModule } from './mercadolibre.module';

describe('MercadolibreModule', () => {
  let mercadolibreModule: MercadolibreModule;

  beforeEach(() => {
    mercadolibreModule = new MercadolibreModule();
  });

  it('should create an instance', () => {
    expect(mercadolibreModule).toBeTruthy();
  });
});

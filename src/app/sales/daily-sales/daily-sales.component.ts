import { Component, OnInit, TemplateRef  } from '@angular/core';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { Page } from '../models/sales.models';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-daily-sales',
  templateUrl: './daily-sales.component.html',
  styleUrls: ['./daily-sales.component.scss']
})
export class DailySalesComponent implements OnInit {
	public sales: any = [];
	modalRef: BsModalRef;
  message:string;
	public sale_selected :any;
  today_date:string;
  total_pages: any = [];
  params:any = {};
  data:any;
  col_12:boolean;
  current_page:number;
  pagination:boolean = false;
  sub_title:string;
  sale_data:any;
  rows = [];
  channel_selected: string;
  editing = {};
  page = new Page();
  searchTerm$ = new Subject<string>();
  cancel_row:any;

  constructor(
  	private modalService: BsModalService, 
  	private toastr: ToastrService, 
  	private saleService: SalesServiceService,
    private router: Router,
    private route: ActivatedRoute,

  	) { 
  	this.sales.count = 0;
    this.sub_title = this.route.snapshot.data.sub_title;
    this.col_12 = true;
    this.params.channel = "all";
    this.channel_selected = "all";
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 50;
    this.setData();
    this.saleService.searchSales(this.searchTerm$, this.params)
      .subscribe(result => {
        this.page.pageNumber = 0
        this.params.page = 1
          this.page.totalElements = result['count'];

        this.data = result['results'];
        this.rows = this.data;
        return result;
      });
  }

  ngOnInit() {
    this.saleService.currentMessage.subscribe(message =>{
    this.col_12 = !this.col_12
    this.message = message
  } 
   )
  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
  }

  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    // console.log(this.params)
    this.setData();

  }

  openModal(template: TemplateRef<any>, sale:any) {
    this.sale_selected = sale;
    this.modalRef = this.modalService.show(template);
  }

  setData(): any {
     this.params.date_update_date = null;

     if (this.sub_title !== 'Históricas'){
       this.params.date_update_date = new Date();
     }
    this.saleService.listSales(this.params).subscribe(
        result => {
          this.sales = result;
          this.rows = this.sales.results; 
          this.page.totalElements = this.sales.count;

          this.today_date = this.formatDate(result.today_date);
          return result;
          },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
    
  }
  export_excel(event):any {
    if (this.sub_title !== 'Históricas'){
      this.saleService.exportExcel(false, 'RV')
    }else{
  	  this.saleService.exportExcel(true, 'RH');
    }
  }

  editSale(id:number):any {
    this.modalRef.hide();
    this.router.navigate(['/sale/update', id])
  }


tryDeleteSale(sale_id:number){
  if (confirm('¿Está seguro de eliminar esta Venta?')){
  this.saleService.deleteSale(sale_id)
          .subscribe(
              result => {
                this.toastr.success("Venta eliminada exitosamente", "Bien");
                // window.location.reload();
                this.setData();

              },
              error => {
                this.toastr.clear();
                this.toastr.error("Ud no tiene permisos para eliminar registros de ventas", "Disculpe")
                // for (let each in error.error){
                //   this.toastr.error(error.error[each][0], "Error")
                // }
              }
          )
    }
}

generateBill(row) {
  // console.log(row);
    if (confirm('¿Desea generar boleta electrónica?')){
      // console.log(row)
      let data = {id:row.id}
      if (confirm('¿Desea imprimir la boleta electrónica generada?')){
        data['print_bill'] = true;

      }else{
        data['print_bill'] = false;
      }
      this.saleService.generateBillOpenFactura(data, 'application/json')
          .subscribe(
              result => {
                
                // this.modalRef.hide();
                this.toastr.success("Boleta electrónica generada exitosamente.", "Bien");
                if (data['print_bill']){
                  const linkSource = 'data:application/pdf;base64,' + result['pdf'];
                  const downloadLink = document.createElement("a");
                  const fileName = "Boleta " + result['folio'] + ".pdf";
                  // console.log(linkSource)
                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();
                }
                this.setData();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    }
  }

  filterChannel(event) {
    // console.log(this.params);
    this.params.page = 1;
    this.page.pageNumber = 0;
    if (this.channel_selected === '1 por acordar') {
      this.params.channel = 1;

    }else{
      if (this.channel_selected === '1 con etiqueta'){
        this.params.channel = 1; 
      }else{
        this.params.channel = this.channel_selected;
      }
      
    }
    // console.log(this.params)
    this.setData();
    
  }
formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

  saveDeclaredTotal(row){
    console.log(row);
    this.saleService.updateDeclaredTotal(row)
      .subscribe(
          result => {
            this.toastr.success("Venta actualizada exitosamente", "Bien");
            this.setData();
          },
          error => {
            this.toastr.clear();
            this.toastr.error("Hubo un error al guardar los datos", "Disculpe")
            // for (let each in error.error){
            //   this.toastr.error(error.error[each][0], "Error")
            // }
          }
      )
  }

  reprintInvoice(id:any, name:any){
    this.saleService.getBill(id).subscribe(
      result => {
        const linkSource = 'data:application/pdf;base64,' + result['pdf'];
        const downloadLink = document.createElement("a");
        const fileName = "Documento "+ name +".pdf";

        this.toastr.success("Documento generado exitosamente", "Bien");
        let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

        let htmlPop = '<embed width=100% height=100%'
                         + ' type="application/pdf"'
                         + ' src="data:application/pdf;base64,'
                         + escape(result['pdf'])
                         + '"></embed>'; 

        let printWindow = window.open ("", "PDF", winparams);
        printWindow.document.write(htmlPop);
  

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
      },
        error => {
          this.toastr.clear();
          for (let each in error.error){
            this.toastr.error(error.error[each], "Error")
          }
        }
    )
  }
  reprintLabel(id:number){
    let data = [{id:id}]
    this.saleService.generateShippingDocument(data)
    .subscribe(
              result => {
                this.toastr.success("Etiqueta generada exitosamente.", "Bien");
                  const linkSource = 'data:application/pdf;base64,' + result['pdf'];
                  const downloadLink = document.createElement("a");
                 // console.log(result);
                var fileName =""
                  if (result['id_pedidos'].length == 1){
                    fileName = "Etiqueta " + result['id_pedidos'][0] + ".pdf";

                  }else{
                    fileName = "Etiquetas"
                    console.log(result['id_pedidos'])
                    result['id_pedidos'].sort()
                    console.log(result['id_pedidos'])
                    fileName += result['id_pedidos'][0] + " - " +  result['id_pedidos'][result['id_pedidos'].length-1] + ".pdf"
                  }

                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each], "Error")
                }
              }
          )
  }
  openModalCancelation(template: TemplateRef<any>, row) {
    this.modalRef = this.modalService.show(template, {class: 'modal-sm'});
    this.cancel_row = row;
  }
  confirm(): void {
    this.message = 'reprint';
    this.createCreditNote();
  }
  decline(): void {
    this.message = 'cancelation';
    this.createCreditNote();
  }
  createCreditNote(){
    let data = {
      observation: this.message,
      sale:this.cancel_row.id
    }
    this.saleService.creditNote(data)
      .subscribe(
          result => {
            this.toastr.success("Documento anulado exitosamente", "Bien");
            this.modalRef.hide();
            this.setData();
          },
          error => {
            this.toastr.clear();
            this.toastr.error("Hubo un error al anular el documento", "Disculpe")
            // for (let each in error.error){
            //   this.toastr.error(error.error[each][0], "Error")
            // }
          }
      )
  }

}

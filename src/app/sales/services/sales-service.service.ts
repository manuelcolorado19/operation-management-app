import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Observable, throwError, interval } from 'rxjs';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { of, BehaviorSubject } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url,url_open_factura,api_token_open_factura } from '../../../global';
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import 'rxjs/add/operator/switchMap';
@Injectable({
  providedIn: 'root'
})
export class SalesServiceService {
  private messageSource = new BehaviorSubject('default message');
  currentMessage = this.messageSource.asObservable();
  internal_params:any={};
  
  public headers: HttpHeaders = new HttpHeaders();
  token: string;
  constructor(
  	 public http: HttpClient,
    public jwtHelper: JwtHelperService,
  	) { 
    if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
    }
  }

  changeMessage(message: string) {
    this.messageSource.next(message)
  }
  findProductDescription(code:string): any{
  	return this.http.get(url+'product_sale/', {params: {search:code}, headers: this.headers})
          .pipe(
            map((response: any) => {
                return response.results;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }

  createSale(register: any): Observable<boolean> {
  	return this.http.post(url+'sale/',register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  userData(): any{
    // console.log(this.headers);
    return this.http.get(url+'users/',  {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response.results[0];
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  listSales(params:any): any {
    if(params.channel == "all"){
      params.channel = "";
    }
    this.internal_params.channel = params.channel
    if (params.date_update_date !== null){

    params.date_update_date = params.date_update_date.getFullYear() + "-" + (params.date_update_date.getMonth() + 1) +"-" + params.date_update_date.getDate();
    }else {

      delete params.date_update_date;
    }
    
      return this.http.get(url+'sale/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
    
  }

  getBill(id:any){
    return this.http.get(url_open_factura +'/v2/dte/document/' + id +'/pdf', {headers:{"apikey": api_token_open_factura}})
    .pipe(
      map((response: any) => {
        return response
      })
 )


  }
  getSaleData(id:number): any {
    return this.http.get(url + 'sale/' + id +'/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }

  updateSale(register: any): Observable<boolean> {
    console.log(register);
    return this.http.put(url+'sale/'+register.id_venta + "/", register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };

  deleteSale(register_id: number): Observable<boolean> {
    return this.http.delete(url+'sale/'+register_id + "/", {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }

  sendRetailData(data:any): any {
    return this.http.post(url+'massive/retail/data', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    // console.log(data)
  }
  listRetailSales(params:any): any {
    // console.log(params)
    if (params.channel__name === 'all'){
      this.internal_params.channel = '';
    }else{
      this.internal_params.channel=params.channel__name;
    }
    this.internal_params.status = params.status;
    this.internal_params.shipping_type = params.shipping_type;
    this.internal_params.shipping_type_sub_status = params.shipping_type_sub_status;
    this.internal_params.sale_of_product__product_code = params.sale_of_product__product_code;
    this.internal_params.page = params.page;
    this.internal_params.number_document = params.number_document;
    this.internal_params.sale_of_product = params.sale_of_product;
    // this.internal_params.date_sale = '2018-11-30'
    
    return this.http.get(url+'retail-sales/', {params:this.internal_params,headers:this.headers})
           .pipe(
             map((response: any) => {
               // console.log(response);
               return response
             })

        )
  }
  updateSubStatusShipping(): any {
   return this.http.get(url+'update/shipping-sub-status', {headers: this.headers})
            .pipe(
              map((response: any) => {
                console.log(response);
                  return true;
              }),
              // catchError((error: HttpErrorResponse | any) => {
              //   console.log("nopo");
              // })
            )
  }

  updateSaleRetail(data: any): Observable<boolean> {
    console.log(data);
    return this.http.put(url+'retail-sales/'+data.id + "/", data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  };


  generateAllSaleRetail(data:any): any {
    return this.http.post(url+'retail-sales/', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
              // const blob = new Blob([response], { type: "application/pdf"});
              // const fileName: string = "Manifiesto.pdf";
              // const objectUrl: string = URL.createObjectURL(blob);
              // const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
              // a.href = objectUrl;
              // a.download = fileName;
              // document.body.appendChild(a);
              // a.click();        

              // document.body.removeChild(a);
              // URL.revokeObjectURL(objectUrl);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    // console.log(data)
  }

  exportExcel(all:boolean, file_name:string): any{
    const params = {}
    if (!all){
      let date=new Date();
      let format_date = date.getFullYear() + "-" + (date.getMonth() + 1) +"-" + date.getDate();
      params['date_filter'] = format_date;
    }
    // console.log(params);
    return this.http.get(url+'export/sales/xlsx', {params:params,headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file_name))
  }

  exportExampleExcel(file_name:string): any{
    // console.log(params);
    return this.http.get(url+'export/sales/retail/example', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", file_name))
  }


  downLoadFile(data: any, type: string, file_name:string):any{
        const d = new Date(), 
        dformat = [d.getFullYear(),
               d.getMonth() < 10 ? ("0" + String(d.getMonth() +1)):(d.getMonth() +1),
               d.getDate() < 10 ? ("0" + String(d.getDate())):(d.getDate())].join('')+
              [d.getHours() < 10 ? ("0" + String(d.getHours())):(d.getHours()),
               d.getMinutes() < 10 ? ("0" + String(d.getMinutes())):(d.getMinutes())].join('');;
        let file_date = dformat;
        // console.log(file_date);
        const blob = new Blob([data], { type: type});
        const fileName: string = file_name + " " + file_date + ".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

  search(terms: Observable<string>, params: any) {
    return terms.debounceTime(650)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchEntries(term, params));
  }
searchSales(terms: Observable<string>, params: any) {
  return terms.debounceTime(650)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchEntrieSales(term, params));
}
  searchEntries(term, params) {
    // console.log(params)
    return this.http.get(url + 'retail-sales/',{params:{search:term, status:"pendiente", channel:this.internal_params.channel, sale_of_product__product_code:this.internal_params.sale_of_product__product_code, number_document:this.internal_params.number_document, sale_of_product:this.internal_params.sale_of_product, shipping_type_sub_status:this.internal_params.shipping_type_sub_status},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }

  searchEntrieSales(term, params) {
    // console.log(params)
    return this.http.get(url + 'sale/',{params:{search:term, channel:this.internal_params.channel},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
  generateBillOpenFactura(data, response_type){
    return this.http.post(url+'generate-bill/', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  updateSalesRipley(data:any) {
    return this.http.post(url+'automatic/retail/sales/ripley', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
updateSalesLinio(data:any) {
    return this.http.post(url+'automatic/retail/sales/linio', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  updateSalesMercadoshop(data:any) {
    return this.http.post(url+'automatic/retail/sales/mercadoshop', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  generateShippingDocument(data:any) {
    return this.http.post(url+'generate/shipping-document/', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  searchProduct(terms: Observable<string>) {
    return terms.debounceTime(600)
      .distinctUntilChanged()
      .switchMap((term) => this.searchEntrieProducts(term));
  }
  searchEntrieProducts(term) {
    // console.log(params)
    return this.http.get(url+'product_sale2/',{params:{search:term},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          res['term'] = term;
          return res;
        })
      )
  }
  sendGrouponData(data:any){
     return this.http.post(url+'load/groupon', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  sendFalabellaData(data:any){
     return this.http.post(url+'load/falabella', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }

  generateShippingGuide(data:any, params:any){
    return this.http.post(url+'shipping/guide', data, {headers: this.headers, params:params})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  updateDeclaredTotal(row:any): Observable<boolean> {
    return this.http.patch(url+'sale/'+row.id + "/", {total_declared:row.total_declared}, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }

   sendParisData(data:any){
     return this.http.post(url+'load/paris', data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }

  generateBillParis(data:any, date_emition:string){
    let new_data = {"sales": data, "date_emititon": date_emition}
    return this.http.post(url+'generate/bill', new_data, {headers: this.headers})
          .pipe(
            map((response: any) => {
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }

  exportEmmited(data:any){
    let data_new = {"sales": data}
    return this.http.post(url+'export/emmited', data_new, {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadEmmited(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
  }

   downLoadEmmited(data: any, type: string):any{
        
        const blob = new Blob([data], { type: type});
        const fileName: string = "Lista de Emitidas.xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        
        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

 updateSalesMercadolibre(data:any){
   return this.http.post(url+'automatic/retail/sales/mercadolibre', data, {headers: this.headers})
      .pipe(
        map((response: any) => {
            return response;
        }),
        // catchError((error: HttpErrorResponse | any) => {
        //   console.log("nopo");
        // })
      )
 }
 creditNote(data:any){
    return this.http.post(url+'cancel-bill/', data, {headers: this.headers})
      .pipe(
        map((response: any) => {
            return response;
        }),
        // catchError((error: HttpErrorResponse | any) => {
        //   console.log("nopo");
        // })
      )
 }
 uploadManifiesto(files: File, channel_selected):any {  
   console.log(files)
    const formData: FormData = new FormData();
    formData.append('file_upload', files, files.name);
    // console.log(formData);

    return this.http.post(url+'retail-sales/', formData, {params:{manifiesto_upload:"1", channel:channel_selected},headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
              // const blob = new Blob([response], { type: "application/pdf"});
              // const fileName: string = "Manifiesto.pdf";
              // const objectUrl: string = URL.createObjectURL(blob);
              // const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
              // a.href = objectUrl;
              // a.download = fileName;
              // document.body.appendChild(a);
              // a.click();        

              // document.body.removeChild(a);
              // URL.revokeObjectURL(objectUrl);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    // return this.http.post(url + 'merge-labels/', formData, {headers:this.headers, responseType: 'arraybuffer'})
    // .subscribe(response => this.downLoadLabels(response, "application/pdf",))
  
  }

  flagSale(id:number, rut, email){
    return this.http.patch(url+'sale/'+id + "/", {rut:rut, email:email}, {headers: this.headers, params:{flag: "true"}})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }

}

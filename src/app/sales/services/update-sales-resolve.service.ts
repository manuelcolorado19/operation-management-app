import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, HttpModule  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpClientModule } from '@angular/common/http';
import { SalesServiceService } from './sales-service.service'

@Injectable()
export class UpdateSalesResolveService implements Resolve<any>{
  public url:string = "http://localhost:8000/";
  public headers: HttpHeaders = new HttpHeaders();
  token: string;

  constructor(private hnService: SalesServiceService) {}

  resolve(route: ActivatedRouteSnapshot) {
    return this.hnService.getSaleData(route.params['id']);
  }
}

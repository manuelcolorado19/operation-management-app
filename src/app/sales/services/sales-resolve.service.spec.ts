import { TestBed } from '@angular/core/testing';

import { SalesResolveService } from './sales-resolve.service';

describe('SalesResolveService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: SalesResolveService = TestBed.get(SalesResolveService);
    expect(service).toBeTruthy();
  });
});

import { Component, OnInit, ViewChild, Renderer, TemplateRef  } from '@angular/core';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/Subject';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { Page } from '../models/sales.models';

@Component({
  selector: 'app-retail-sales',
  templateUrl: './retail-sales.component.html',
  styleUrls: ['./retail-sales.component.scss']
})
export class RetailSalesComponent implements OnInit {
  update_several:any={data:[]};
  rut_client: String;
  email_client: String;
  disabled_button:boolean = false;
  date_shipping_guide: Date = new Date();
  bsInlineValue = new Date();
  channel_selected_manifiesto: string = "Ripley";
  address_falabella: string = 'lo espejo';
  minDate: Date = new Date();
	editing = {};
  params:any = {};
  flag_selected_sale:any = {};
  paris:boolean;
  selected = [];
  data:any;
  expanded: any = {};
  locale = 'es';
  channel_selected: string;
	rows = []
  grandIndex:number;
  searchTerm$ = new Subject<string>();
  page = new Page();
  editing_fields:string = "";
  expandedId=[];
  title:string;
  change_total_declared: number;
  row_update: any;
  modalRef: BsModalRef;
  sale_selected: any;
  file: File;

  
  constructor(private saleService:SalesServiceService, private toastr:ToastrService, private renderer: Renderer, private route: ActivatedRoute, private modalService: BsModalService, private router: Router, private localeService: BsLocaleService) { 
    this.localeService.use('es');
    this.params.channel__name = "all";
    this.params.shipping_type = "";
    this.channel_selected = "all";
    this.params.sale_of_product = "1";
    this.params.shipping_type_sub_status = "";
    this.params.status = "pendiente";
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 10;
    let title_data = this.route.snapshot.data.title;
    if (title_data === 'Asiamerica | Generar Boletas'){
      this.title = "Generar Boletas"
      this.params.sale_of_product__product_code = 1;
      this.params.number_document = 3;
      this.params.sale_of_product = "2";
    }else{
      this.title = "Ingresar Códigos"
      this.params.sale_of_product__product_code = 3;
      this.params.number_document = 1;
      this.params.sale_of_product = "2";



    }
    // this.saleService.listRetailSales();
    this.listData();

     this.saleService.search(this.searchTerm$, this.params)
      .subscribe(result => {
        this.page.pageNumber = 0
        this.params.page = 1
          this.page.totalElements = result['count'];

        this.data = result['results'];
        this.rows = this.data;
        return result;
      });
    }

    setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    
    this.listData();
  }
  
  @ViewChild('myTable') table: any;
  onSelect({ selected }) {
      // console.log('Select Event', this.selected);

      this.selected.splice(0, this.selected.length);
      this.selected.push(...selected);
    }

   getId(row) {    
    return row.id;
  }


  ngOnInit() {
  }
  updateValue(event, cell, rowIndex) {
    // console.log('inline editing rowIndex', rowIndex)
    // if (cell == 'total_declared') {
    //   this.change_total_declared = 1;
    // }
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    // this.rows = [...this.rows];
    // setTimeout(()=> {
    //    this.expand();
    //  },25);
    // if (this.update_several.data.some(item => item.id === this.rows[rowIndex].id)){
      
    // }
    // this.update_several.data.push(this.rows[rowIndex]);
    // console.log('UPDATED!', this.rows[rowIndex][cell]);
  }

//   expand(){
//     this.table.rowDetail.collapseAllRows();
//     for(let i=0;i<this.rows.length;i++){
//       if(this.expandedId.indexOf(this.rows[i].id) > -1){
//         this.table.rowDetail.toggleExpandRow(this.rows[i])
//       }
//     }
// }

  listData(){
    // console.log(this.params)
    this.saleService.listRetailSales(this.params).subscribe(
        result => {
          this.page.totalElements = result.count;
          this.data = result.results;
          this.rows = this.data
          // console.log(this.data)
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  openModal(template: TemplateRef<any>, sale:any) {
    this.sale_selected = sale;
    this.modalRef = this.modalService.show(template);
  }


editField(event,rowIndex, row){
  // console.log(rowIndex)
  // console.log(row)
  this.toggleExpandRow(row, rowIndex);
  this.editing[rowIndex + '-number_document'] = true;
  this.editing[rowIndex + '-payment_type'] = true;
  this.editing[rowIndex + '-document_type'] = true;
  this.editing[rowIndex + '-total_declared'] = true;
  // console.log(this.rows[rowIndex].products.length);
  for (let i = 0; i < this.rows[rowIndex].products.length; i++) {
    this.editing[rowIndex + String(i) + '-product_code__code_seven_digits'] = true;
    this.editing[rowIndex + String(i) + '-unit_price'] = true;
  }
  // console.log(this.renderer);
  // this.renderer.setElementStyle(event.currentTarget.parentElement.parentElement.parentElement.parentElement, "background-color", "white !important");
  // console.log(event.currentTarget.parentElement.parentElement.parentElement.parentElement.attributes.class);

}
editSale(id:number):any {
    this.modalRef.hide();
    this.router.navigate(['/sale/update', id])
  }

updateSubValue(event, cell, rowIndex) {
    this.editing[this.grandIndex + String(rowIndex) + '-' + cell] = false;
    this.rows[this.grandIndex].products[rowIndex][cell] = event.target.value;
    // this.rows = [...this.rows];
    this.update_several.data.push(this.rows[this.grandIndex]);
    // setTimeout(()=> {
    //    this.expand();
    //  },25);
    // console.log(this.editing[this.grandIndex + rowIndex + '-' + cell])
    // console.log('UPDATED!', this.rows[this.grandIndex].products[cell]);
  }
  toggleExpandRow(row, rowGrandIndex) {
    // console.log(row);
    this.grandIndex = rowGrandIndex;
    console.log(this.grandIndex);
    this.table.rowDetail.toggleExpandRow(row);
    const index=this.expandedId.indexOf(row.id);
    if(index > -1)
      this.expandedId.splice(index,1);
    else{
      this.expandedId.push(row.id);
    }
  }
  saveData(row) {
    // if (this.change_total_declared !== undefined) {
    //   this.row_update = row;
    //   this.row_update.declared_total = this.change_total_declared
    // }else{
    //   this.row_update = row;
    //   this.row_update.declared_total = null
    // }
    // console.log(row);

    this.saleService.updateSaleRetail(row)
          .subscribe(
              result => {
                this.toastr.success("Venta actualizada exitosamente", "Bien");
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();
                this.listData();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
  }

  deleteData(row){
  if (confirm('¿Está seguro de eliminar esta Venta?')){
  this.saleService.deleteSale(row.id)
          .subscribe(
              result => {
                this.toastr.success("Venta eliminada exitosamente", "Bien");
                // window.location.reload();
                this.listData();

              },
              error => {
                this.toastr.clear();
                this.toastr.error("Ud no tiene permisos para eliminar registros de ventas", "Disculpe")
                // for (let each in error.error){
                //   this.toastr.error(error.error[each][0], "Error")
                // }
              }
          )
    }
}
getRowClass(row) {
  
    return {
      'red-label': row.warning_label || row.shipping_status === 'Cancelado',
      'yellow-label': (!row.warning_label && row.channel_status !== 'Pagado' && row.shipping_status !== '' && row.channel_status !== null && row.channel === 'Mercadolibre'),
      'express-label': (row.comments.search("Despacho Express") !== -1),
      'multibulto-label': (row.comments.search("Multibulto") !== -1)
    };
  }
  onDetailToggle(event) {
    // console.log('Detail Toggled', event);
  }
  filterChannel(event) {
    // console.log(this.params);
    if (this.params.shipping_type_sub_status === 'envío por pagar') {
      this.selected = [];
    }
    this.params.page = 1;
    this.page.pageNumber = 0;
    if (this.channel_selected === '1 por acordar') {
      this.params.channel__name = 1;
      this.params.shipping_type = 'por acordar';
      if (this.params.shipping_type_sub_status === ''){
      this.params.shipping_type_sub_status = "pendiente"
      }

    }else{
      if (this.channel_selected === '1 con etiqueta'){
        this.params.channel__name = 1; 
        this.params.shipping_type_sub_status = ""
        this.params.shipping_type = 'con etiqueta';
      }else{
        this.params.channel__name = this.channel_selected;
        this.params.shipping_type = ""
        this.params.shipping_type_sub_status = ""
      }
      
    }
    // console.log(this.params)
    this.listData();
    
  }
  updateSubStatus() {
    this.saleService.updateSubStatusShipping().subscribe(
        result => {
          // console.log(result);
          this.toastr.success("Ventas por acordar actualizadas exitosamente", "Bien");
      this.listData();
          return result;

        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }

  generateAll($event) {
    // console.log(this.update_several);
    this.disabled_button = true;
    this.saleService.generateAllSaleRetail(this.selected)
          .subscribe(
              result => {
                this.selected = [];
                this.toastr.success("Boletas generadas exitosamente", "Bien");
                const downloadLink = document.createElement("a");
                const fileName = "Manifiesto de ventas" + ".pdf";
                const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];

                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf_file'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();
                this.listData();
                this.disabled_button = false;

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
                this.disabled_button = false;
              }
          )
  }

  generateBill(row) {
    if (confirm('¿Desea generar boleta electrónica?')){
      // console.log(row)
      let data = {id:row.id}
      let response_type = 'application/json';
      if (confirm('¿Desea imprimir la boleta electrónica generada?')){
        data['print_bill'] = true;

      }else{
        data['print_bill'] = false;
      }
      this.saleService.generateBillOpenFactura(data, response_type)
          .subscribe(
              result => {
                this.toastr.success("Boleta electrónica generada exitosamente.", "Bien");
                if (data['print_bill']){
                  const linkSource = 'data:application/pdf;base64,' + result['pdf'];
                  const downloadLink = document.createElement("a");
                  const fileName = "Boleta " + result['folio'] + ".pdf";

                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();

                }
                  this.listData();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    }
  }
  generateShippingDocument(rows) {
    this.saleService.generateShippingDocument(rows)
    .subscribe(
              result => {
                this.toastr.success("Etiqueta generada exitosamente.", "Bien");
                  const linkSource = 'data:application/pdf;base64,' + result['pdf'];
                  const downloadLink = document.createElement("a");
                 // console.log(result);
                var fileName =""
                  if (result['id_pedidos'].length == 1){
                    fileName = "Etiqueta " + result['id_pedidos'][0] + ".pdf";

                  }else{
                    fileName = "Etiquetas"
                    console.log(result['id_pedidos'])
                    result['id_pedidos'].sort()
                    console.log(result['id_pedidos'])
                    fileName += result['id_pedidos'][0] + " - " +  result['id_pedidos'][result['id_pedidos'].length-1] + ".pdf"
                  }

                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();

                  this.listData();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each], "Error")
                }
              }
          )
  }
  massiveLabelGeneration(){
    this.generateShippingDocument(this.selected);
  }
  generateLabel(row){
    let data = [row];
    this.generateShippingDocument(data);
  }
  generateGuide($event){
    this.disabled_button = true;
    let data = {
       date_guide:this.formatDate(this.date_shipping_guide),
       sales:this.selected
    }
    this.modalRef.hide();
    // console.log(data);
    if (this.paris){
      this.generateBillParis(this.formatDate(this.date_shipping_guide));
    }else{
      this.saleService.generateShippingGuide(data, {address:this.address_falabella}).subscribe(
      result => {
        // console.log(result);
        const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];
        const downloadLink = document.createElement("a");
        const fileName = "Guia de despacho " + result['folio'] +".pdf";

        this.toastr.success("Guía de despacho generada exitosamente", "Bien");
        let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

        let htmlPop = '<embed width=100% height=100%'
                         + ' type="application/pdf"'
                         + ' src="data:application/pdf;base64,'
                         + escape(result['pdf_file'])
                         + '"></embed>'; 

        let printWindow = window.open ("", "PDF", winparams);
        printWindow.document.write(htmlPop);
        this.selected = [];

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        this.disabled_button = false;

      },
        error => {
          this.toastr.clear();
          for (let each in error.error){
            this.toastr.error(error.error[each], "Error")
          }
          this.disabled_button = false;
        }
    )
    }
    
  }

  selectDateGuide($event, template: TemplateRef<any>, bill:boolean){
    console.log(bill)
    if (bill){
        this.paris = true;
    }else{
      this.paris = false;
    }
     this.modalRef = this.modalService.show(template);
  }

  formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }


  generateBillParis(date_emition:string){
   
    this.saleService.generateBillParis(this.selected, date_emition).subscribe(
      result => {
        // console.log(result);
        const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];
        const downloadLink = document.createElement("a");
        const fileName = "Facturas Paris.pdf";

        this.toastr.success("Factura generada exitosamente", "Bien");
        let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

        let htmlPop = '<embed width=100% height=100%'
                         + ' type="application/pdf"'
                         + ' src="data:application/pdf;base64,'
                         + escape(result['pdf_file'])
                         + '"></embed>'; 

        let printWindow = window.open ("", "PDF", winparams);
        printWindow.document.write(htmlPop);
        this.selected = [];

        downloadLink.href = linkSource;
        downloadLink.download = fileName;
        downloadLink.click();
        this.listData();

      },
        error => {
          this.toastr.clear();
          for (let each in error.error){
            this.toastr.error(error.error[each], "Error")
          }
        }
    )
  }
  massiveBill($event, template: TemplateRef<any>){
    this.modalRef = this.modalService.show(template);
  }
  massiveBillFromManifiesto(){
    let file = this.file;
    this.disabled_button = true;
    this.saleService.uploadManifiesto(file, this.channel_selected_manifiesto)
          .subscribe(
              result => {
                // console.log(result);
                // this.selected = [];
                this.modalRef.hide()
                this.toastr.success("Boletas generadas exitosamente", "Bien");
                const downloadLink = document.createElement("a");
                const fileName = "Manifiesto de ventas" + ".pdf";
                const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];

                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf_file'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();
                this.listData();
                this.disabled_button = false;

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
                this.disabled_button = false;
              }
          )
  }
  onFileSelected(event) {
   if(event.target.files.length > 0) 
    {
      this.file = event.target.files[0]
      // console.log(event.target.files[0].name);
    }
}
flagBill(sale:any, template: TemplateRef<any>){
    this.flag_selected_sale = sale;
    this.modalRef = this.modalService.show(template);
  }

  addFlagToSale(){
    this.saleService.flagSale(this.flag_selected_sale.id, this.rut_client, this.email_client).subscribe(
        result => {
          // console.log(result);
          this.toastr.success("Ventas por acordar actualizadas exitosamente", "Bien");
          this.modalRef.hide()
          this.listData();
          return result;

        },
          error =>{
             this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
            
          })
  }
}

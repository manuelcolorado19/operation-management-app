
export class Product {
  id?: number;
  description?:string;
  sku?: string;
  quantity?: number;
  color?: string;
  code_seven_digits?: string;
  unit_price?: number;
  id_of_product?:number;
  total?: number;
  id_sale?:any;
}
export class RegisterSale {
  id_venta?: string;
  document_type?: string = 'sdt';
  number_document?: string;
  channel?: any = '10';
  confirm_image?: string = "assets/images/presencial.png";
  sender_responsable?: string = 'retira personalmente';
  payment_type?: string = "efectivo";
  order_channel?: number;
  comments?: string;
  products?: Product[];
  total_sale?: number = 0;
  date_sale?:Date = new Date();
  bill?:boolean;
  print_bill?:boolean;
  bill_fact?:boolean = false;
  rut_number?:string = null;
  email?:string = null;
}
export class Page {
    //The number of elements in the page
    size: number = 0;
    //The total number of elements
    totalElements: number = 0;
    //The total number of pages
    totalPages: number = 0;
    //The current page number
    pageNumber: number = 0;
}

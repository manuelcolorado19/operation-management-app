import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { RegisterSaleComponent } from './register-sale/register-sale.component'
import { DetailSaleComponent} from './detail-sale/detail-sale.component'
import { MainComponent} from './main/main.component'
import { DailySalesComponent} from './daily-sales/daily-sales.component'
import { RetailSalesComponent} from './retail-sales/retail-sales.component'
import { SalesComponent } from './sales.component'
import { ChargeRetailComponent } from './charge-retail/charge-retail.component'
import { AuthGuardGuard } from './guards/auth-guard.guard' 
import { AdminGuard } from './guards/admin.guard' 
import { SalesResolveService } from './services/sales-resolve.service'
import { UpdateSalesResolveService } from './services/update-sales-resolve.service'
import { DailySalesResolveService } from './services/daily-sales-resolve.service'
import { HistoricSalesResolveService } from './services/historic-sales-resolve.service'
import { UpdateRipleyComponent } from './update-ripley/update-ripley.component';
import { UploadGrouponFalabellaComponent } from './upload-groupon-falabella/upload-groupon-falabella.component';
import { UploadParisComponent } from './upload-paris/upload-paris.component';

const appRoutes : Routes = [{
  path: '',
  component: SalesComponent,
  children: [
    {
      path: 'register',
      component: RegisterSaleComponent,
      canActivate: [AuthGuardGuard],
      resolve: {
        user_data:SalesResolveService
      },
      data: { title: 'Asiamerica | Registro de Venta' }
    },
    {
      path: 'retail-sales',
      canActivate: [AuthGuardGuard],
      component: RetailSalesComponent,
      data: { title: 'Asiamerica | Generar Boletas' }
    },
    {
      path: 'charge-retail',
      canActivate: [AuthGuardGuard],
      component: ChargeRetailComponent,
      data: { title: 'Asiamerica | Carga Retail' }
    },
    {
      path: 'update-ripley',
      canActivate: [AuthGuardGuard],
      component: UpdateRipleyComponent,
      data: { title: 'Asiamerica | Actualización Ripley' }
    },
    {
      path: 'update-linio',
      canActivate: [AuthGuardGuard],
      component: UpdateRipleyComponent,
      data: { title: 'Asiamerica | Actualización Linio' }
    },
    {
      path: 'update-mercadoshop',
      canActivate: [AuthGuardGuard],
      component: UpdateRipleyComponent,
      data: { title: 'Asiamerica | Actualización Mercadoshop' }
    },
    {
      path: 'update-mercadolibre',
      canActivate: [AuthGuardGuard],
      component: UpdateRipleyComponent,
      data: { title: 'Asiamerica | Actualización Mercadolibre' }
    },
    {
      path: 'main',
      component: MainComponent,
      canActivate: [AuthGuardGuard],
      data: { title: 'Asiamerica | Principal' }
    },
    {
      path: 'list-historic',
      component: DailySalesComponent,
      canActivate: [AdminGuard],
      data: { title: 'Asiamerica | Ventas Históricas', sub_title:"Históricas" }
    },
    {
      path: 'update/:id',
      component: RegisterSaleComponent,
      canActivate: [AuthGuardGuard],
      resolve: {
        sale_data:UpdateSalesResolveService
      },
      data: { title: 'Asiamerica | Actualizar Registro de venta.' }
    },
    {
      path: 'upload-groupon-falabella',
      component: UploadGrouponFalabellaComponent,
      canActivate: [AuthGuardGuard],
      data: { title: 'Asiamerica | Carga Ventas Groupon.' }
    },
    {
      path: 'upload-falabella',
      component: UploadGrouponFalabellaComponent,
      canActivate: [AuthGuardGuard],
      data: { title: 'Asiamerica | Carga Ventas Falabella.', channel:"Falabella" }
    },
    {
      path: 'upload-paris',
      component: UploadParisComponent,
      canActivate: [AuthGuardGuard],
      data: { title: 'Asiamerica | Carga Ventas Paris.'}
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [RouterModule],
  providers: [SalesResolveService, UpdateSalesResolveService, DailySalesResolveService, HistoricSalesResolveService]
})
export class SalesRoutingModule { }

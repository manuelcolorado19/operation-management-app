import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
@Component({
  selector: 'app-charge-retail',
  templateUrl: './charge-retail.component.html',
  styleUrls: ['./charge-retail.component.scss']
})
export class ChargeRetailComponent implements OnInit {

 @ViewChild('file_input') 
 file_input: ElementRef;

 data_rows:any = []
  data_upload:any = [];
     

  constructor(private saleService:SalesServiceService, private toastr: ToastrService,) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    /* wire up file reader */
    // console.log(this.file_input.nativeElement.innerHTML);
    // console.log(evt.target)
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary'});

      /* grab first sheet */
      const wsname: string = wb.SheetNames[0];
      const ws: XLSX.WorkSheet = wb.Sheets[wsname];
      // console.log(ws);
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1

      /* save data */
      this.data_upload = this.validateData(XLSX.utils.sheet_to_json(ws, {header: 1}));
      // console.log(this.data_upload)
    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateData(data:any) {
    data.splice(0, 1);
    let channels = ['Mercadolibre', 'Ripley', 'Paris', 'Linio', 'Groupon', 'Falabella', 'Yapo', 'Cluboferta', 'Presencial', 'Extra Mercadolibre', 'Mercadoshop', 'Otro canal']
    let index = 0;
    // let data_rows = []
    let products_sku5 = 0;
    let shipping = 0;
    for (let row of data ) {
      // console.log(row[0]);
      
      if(!(moment(String(row[0]), 'DD-MM-YYYY', true).isValid())){
        this.toastr.error("Revise el formato de fecha el correcto es DD-MM-AAAA", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];
        return [];
      }
      
      if(typeof(row[1]) !== 'number'){
        this.toastr.error("La columna SKU debe contener números únicamente", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];

        return [];
      }
      if(typeof(row[2]) !== 'number'){
        this.toastr.error("La columna Cantidad debe contener números únicamente", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];

        return [];
      }
      if(row[2].toString().length > 2){
        this.toastr.warning("La cantidad de productos es mayor a 2 dígitos", "Precaución")
        // this.file_input.nativeElement.value = "";
        // this.data_rows = [];

        // return [];
      }
      if(row[1].toString().length === 5){
        products_sku5 += 1;
      }
      if(row[1].toString().length === 1){
        shipping += 1;
      }
      if(typeof(row[3]) !== 'number'){
        this.toastr.error("La columna Precio Unitario debe contener números únicamente", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];

        return [];
      }
      if(typeof(row[4]) !== 'number'){
        this.toastr.error("La columna Total debe contener números únicamente", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];

        return [];
      }
      if(typeof(row[5]) !== 'number'){
        this.toastr.error("La columna Id Venta debe contener números únicamente", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];

        return [];
      }
      if (!(channels.indexOf(row[6]) > -1)){
        this.toastr.error("El canal indicado es incorrecto las opciones son: Mercadolibre, Ripley, Paris, Linio, Groupon, Falabella, Yapo, Cluboferta, Presencial, ExtraMercadolibre, Mercadoshop, Otro canal", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];
        return [];
      }
      if (row.length == 6){
        data[index][7] = "";
      }
      
      if(row[8] != 'SI' && row[8] != 'NO'){
        this.toastr.error("La columna de etiqueta debe contener los valores SI o NO únicamente", "Error")
        this.file_input.nativeElement.value = "";
        this.data_rows = [];
        return [];
      }
      
      // index += 1;
      this.data_rows.push({
        date_register:row[0],
        sku:row[1],
        quantity:row[2],
        unit_price:row[3],
        total:row[4],
        order_channel:row[5],
        channel:row[6],
        comment:row[7],
        label:row[8]
      })
      
    }
    if(products_sku5 < shipping){
      this.toastr.warning("La cantidad de envíos es mayor a la cantidad de productos", "Precaución")

      // console.log(this.data_rows);
    }
  	return data;
  }

  // validateRow1(): boolean {
  //   return true;
  // }

  downloadExampleExcel(event): void {
    this.saleService.exportExampleExcel("Archivo de Ejemplo");
  }

  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.data_upload = [];
    this.data_rows = [];
  }
  sendData(): void {
    if (this.data_upload.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      this.saleService.sendRetailData(this.data_rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";

                this.data_upload = [];
                this.data_rows = [];
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      // this.saleService.sendRetailData(this.data_upload);

    }
    
  }

}

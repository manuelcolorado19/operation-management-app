import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChargeRetailComponent } from './charge-retail.component';

describe('ChargeRetailComponent', () => {
  let component: ChargeRetailComponent;
  let fixture: ComponentFixture<ChargeRetailComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChargeRetailComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChargeRetailComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

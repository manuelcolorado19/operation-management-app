import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadGrouponFalabellaComponent } from './upload-groupon-falabella.component';

describe('UploadGrouponFalabellaComponent', () => {
  let component: UploadGrouponFalabellaComponent;
  let fixture: ComponentFixture<UploadGrouponFalabellaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadGrouponFalabellaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadGrouponFalabellaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-upload-groupon-falabella',
  templateUrl: './upload-groupon-falabella.component.html',
  styleUrls: ['./upload-groupon-falabella.component.scss']
})
export class UploadGrouponFalabellaComponent implements OnInit {
@ViewChild('file_input') 
 file_input: ElementRef;
 channel:string;
 data_rows:any = []
 button_active:boolean = false;
  data_upload:any = [];
  constructor(private saleService:SalesServiceService, private toastr: ToastrService, private route: ActivatedRoute) { 
    this.channel = this.route.snapshot.data.channel;
  }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1

      this.data_upload = this.validateData(wb, XLSX.utils.sheet_to_json(ws, {header: 1}));
    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateData(wb,data:any) {
    data.splice(0, 1);
    const dateMode = wb.Workbook.WBProps.date1904;
    if(this.channel !== undefined){
        for (let row of data ) {
          let lines_count = row[4].replace(/^\s*[\r\n]/gm, '').split(/\r\n|\r|\n/)
          if ((lines_count.length)/3 > 1){
            for (let j = 0; j < lines_count.length; j+=3) {
                let date_sale_format = XLSX.SSF.format('DD-MM-YYYY HH:MM', row[0], { date1904: dateMode }); 
                 this.data_rows.push({
                  date_sale:date_sale_format,
                  order_channel:row[1],
                  description:lines_count[j],
                  sku:lines_count[j+1].split(":")[1],
                  quantity:lines_count[j+2].split(":")[1],
                  shipping_date:XLSX.SSF.format('DD-MM-YYYY', row[5], { date1904: dateMode }),
                  status:row[6],
                })
            }
          }else{
            let sku = lines_count[1]
            let quantity = lines_count[2]
            let description = lines_count[0]
            let date_sale_format = XLSX.SSF.format('DD-MM-YYYY HH:MM', row[0], { date1904: dateMode }); 
            this.data_rows.push({
              date_sale:date_sale_format,
              order_channel:row[1],
              description:description,
              sku:sku.split(":")[1],
              quantity:quantity.split(":")[1],
              shipping_date:XLSX.SSF.format('DD-MM-YYYY', row[5], { date1904: dateMode }),
              status:row[6],
            })
          }
      }
    }else{
      for (let row of data ) {
        this.data_rows.push({
          coupon:row[5],
          sku_groupon:row[15],
          description:row[16],
          total_sale:row[17],
        })
      }
    }
  }
  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.data_upload = [];
    this.data_rows = [];
  }
  sendData(): void {
    if (this.data_rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      this.button_active = true;
      if(this.channel !== undefined){
        this.saleService.sendFalabellaData(this.data_rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";

                this.data_upload = [];
                this.data_rows = [];
                this.button_active = false;

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )

      }else{

      this.saleService.sendGrouponData(this.data_rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";

                this.data_upload = [];
                this.data_rows = [];
                this.button_active = false;

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      }
    }
  }

  formatDate(date:Date){
    // console.log(date);
    let minutes = (date.getMinutes() - 1)
    if (minutes < 0){
      minutes = 0;
    }
    let format = [(date.getMonth()+1).toString(), (date.getDate()).toString(), (date.getHours()).toString(), minutes.toString()]
    for (let i=0; i<format.length; i++){
      if(Number(format[i]) < 10){
        format[i] = '0' + format[i]
      }
    }
    return [format[1], format[0], date.getFullYear()].join('-')+' '+ [format[2], format[3], '00'].join(':');
  }
  formatShipping(date){
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }
}

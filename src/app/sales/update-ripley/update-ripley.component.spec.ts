import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UpdateRipleyComponent } from './update-ripley.component';

describe('UpdateRipleyComponent', () => {
  let component: UpdateRipleyComponent;
  let fixture: ComponentFixture<UpdateRipleyComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UpdateRipleyComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UpdateRipleyComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

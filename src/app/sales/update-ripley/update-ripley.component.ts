import { Component, OnInit } from '@angular/core';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { DatePipe } from '@angular/common';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import {Location} from '@angular/common';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-update-ripley',
  templateUrl: './update-ripley.component.html',
  styleUrls: ['./update-ripley.component.scss']
})
export class UpdateRipleyComponent implements OnInit {
	locale = 'es';
	register: any = {};
	maxDate: Date;
	minDate: Date;
  title:string;

  constructor(
  	private localeService: BsLocaleService,
    private location: Location,
    private saleService:SalesServiceService,
    private toastr:ToastrService,
    private route: ActivatedRoute,
  ) { 
  	this.localeService.use('es');
  	// this.register.date_to = new Date();
  	this.maxDate = new Date();
  	this.minDate = new Date(2018, 11, 1);
    let title = this.route.snapshot.data.title;
    console.log(title)
    switch (title) {
      case 'Asiamerica | Actualización Linio':
        // code...
        this.title = 'Actualización Ventas Linio'
        break;
      case 'Asiamerica | Actualización Ripley':
        // code...
        this.title = 'Actualización Ventas Ripley'
        break;
      case 'Asiamerica | Actualización Mercadoshop':
        // code...
        this.title = 'Actualización Ventas Mercadoshop'
        break;
      case 'Asiamerica | Actualización Mercadolibre':
        // code...
        this.title = 'Actualización Ventas Mercadolibre'
        break;
      
      default:
        // code...
        break;
    }
  }

  ngOnInit() {
  }

  updateRipleySales(event) { 
    // console.log(this.register);
    let data = {
      date_from:this.formatDate(this.register.date_to[0]),
      date_to:this.formatDate(this.register.date_to[1])
    }

    switch (this.title) {
      case 'Actualización Ventas Linio':
      this.saleService.updateSalesLinio(data)
        .subscribe(
          result => {
              this.toastr.success("Ventas de Linio Actualizadas exitosamente", "Bien");
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
        break;
      case 'Actualización Ventas Ripley':
      this.saleService.updateSalesRipley(data)
        .subscribe(
          result => {
              this.toastr.success("Ventas de Ripley Actualizadas exitosamente", "Bien");
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
        break;
      case 'Actualización Ventas Mercadoshop':
      this.saleService.updateSalesMercadoshop(data)
        .subscribe(
          result => {
              this.toastr.success("Ventas de Ripley Actualizadas exitosamente", "Bien");
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
        // code...
        break;
     case 'Actualización Ventas Mercadolibre':
      this.saleService.updateSalesMercadolibre(data)
        .subscribe(
          result => {
              this.toastr.success("Ventas de Mercadolibre Actualizadas exitosamente", "Bien");
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
        // code...
        break;
      
      default:
        // code...
        break;
    }


    

  }

  formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

}

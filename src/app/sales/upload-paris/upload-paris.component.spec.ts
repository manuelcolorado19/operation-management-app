import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadParisComponent } from './upload-paris.component';

describe('UploadParisComponent', () => {
  let component: UploadParisComponent;
  let fixture: ComponentFixture<UploadParisComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadParisComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadParisComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

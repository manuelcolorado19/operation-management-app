import { Component, OnInit, ElementRef, ViewChild } from '@angular/core';
import * as XLSX from 'xlsx';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import * as moment from 'moment';
import { Router, ActivatedRoute } from '@angular/router';

@Component({
  selector: 'app-upload-paris',
  templateUrl: './upload-paris.component.html',
  styleUrls: ['./upload-paris.component.scss']
})
export class UploadParisComponent implements OnInit {

	@ViewChild('file_input') 
	file_input: ElementRef;
	channel:string;
	data_rows:any = []
	data_upload:any = [];

  constructor(private saleService:SalesServiceService, private toastr: ToastrService,) { }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1) throw new Error('Cannot use multiple files');
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1

      this.data_upload = this.validateData(wb,XLSX.utils.sheet_to_json(ws, {header: 1}));
    };
    reader.readAsBinaryString(target.files[0]);
  }

  validateData(wb,data:any) {
    const dateMode = wb.Workbook.WBProps.date1904;
    data.splice(0, 1);
    // let channels = ['Mercadolibre', 'Ripley', 'Paris', 'Linio', 'Groupon', 'Falabella', 'Yapo', 'Cluboferta', 'Presencial', 'Extra Mercadolibre', 'Mercadoshop', 'Otro canal']
    // let index = 0;
    // let products_sku5 = 0;
    // let shipping = 0;
    for (let row of data ) {
      
      // if(!(moment(String(row[0]), 'DD-MM-YYYY', true).isValid())){
      //   this.toastr.error("Revise el formato de fecha el correcto es DD-MM-AAAA", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];
      //   return [];
      // }
      
      // if(typeof(row[1]) !== 'number'){
      //   this.toastr.error("La columna SKU debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];

      //   return [];
      // }
      // if(typeof(row[2]) !== 'number'){
      //   this.toastr.error("La columna Cantidad debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];

      //   return [];
      // }
      // if(row[2].toString().length > 2){
      //   this.toastr.warning("La cantidad de productos es mayor a 2 dígitos", "Precaución")
      //   // return [];
      // }
      // if(row[1].toString().length === 5){
      //   products_sku5 += 1;
      // }
      // if(row[1].toString().length === 1){
      //   shipping += 1;
      // }
      // if(typeof(row[3]) !== 'number'){
      //   this.toastr.error("La columna Precio Unitario debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];

      //   return [];
      // }
      // if(typeof(row[4]) !== 'number'){
      //   this.toastr.error("La columna Total debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];

      //   return [];
      // }
      // if(typeof(row[5]) !== 'number'){
      //   this.toastr.error("La columna Id Venta debe contener números únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];

      //   return [];
      // }
      // if (!(channels.indexOf(row[6]) > -1)){
      //   this.toastr.error("El canal indicado es incorrecto las opciones son: Mercadolibre, Ripley, Paris, Linio, Groupon, Falabella, Yapo, Cluboferta, Presencial, ExtraMercadolibre, Mercadoshop, Otro canal", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];
      //   return [];
      // }
      // if (row.length == 6){
      //   data[index][7] = "";
      // }
      
      // if(row[8] != 'SI' && row[8] != 'NO'){
      //   this.toastr.error("La columna de etiqueta debe contener los valores SI o NO únicamente", "Error")
      //   this.file_input.nativeElement.value = "";
      //   this.data_rows = [];
      //   return [];
      // }
      
      // index += 1;
      this.data_rows.push({
        date_sale:XLSX.SSF.format('DD-MM-YYYY', row[9], { date1904: dateMode }),
        order_channel:row[0],
        request_number:row[1],
        tracking_number:row[2],
        shipping_date:XLSX.SSF.format('DD-MM-YYYY', row[10], { date1904: dateMode }),
        sku:row[23],
        description:row[25],
        quantity:row[26],
        unit_price:row[30],
      })
      
    }
    // if(products_sku5 < shipping){
    //   this.toastr.warning("La cantidad de envíos es mayor a la cantidad de productos", "Precaución")

    //   // console.log(this.data_rows);
    // }
  	return data;
  }

  cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.data_upload = [];
    this.data_rows = [];
  }
  sendData(): void {

  	 if (this.data_rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
    	this.saleService.sendParisData(this.data_rows)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                this.file_input.nativeElement.value = "";

                this.data_upload = [];
                this.data_rows = [];

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
    }
  }

  exportEmmited(event){
    this.saleService.exportEmmited(this.data_rows);
  }
  formatShipping(date){
    // console.log(moment(date, "MM-DD-YYYY"));
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

}

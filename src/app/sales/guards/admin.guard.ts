import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, Router } from '@angular/router';
import { Observable } from 'rxjs';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import "rxjs/add/operator/map"; 
@Injectable({
  providedIn: 'root'
})
export class AdminGuard implements CanActivate {
	constructor(
   private router: Router,
   private auth: SalesServiceService,
   private toastr: ToastrService,
   ) { 
	}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean> | Promise<boolean> | boolean {
  	if (localStorage.getItem('currentUser')) {
  		return this.auth.userData().map(result => {
        	// return result;
	        if (result.is_superuser){
	        	return true;
	        }else{
	        	this.toastr.clear();
	        	this.toastr.error("Ud no tiene permiso para visualizar esta información, contacte con un Administrador", "Error");
	        	return false;
	        }
        },
          error =>{
            // console.log("error");
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            return [];
            
          })
    }

    this.router.navigate(['/auth/login']);
    return false;
  }
  
}

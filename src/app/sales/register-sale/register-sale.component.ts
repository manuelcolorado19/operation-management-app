import { Component, OnInit, ChangeDetectionStrategy, ChangeDetectorRef, TemplateRef, ViewChild, ElementRef ,  } from '@angular/core';
import { Subscription } from 'rxjs/Subscription';
import { registerLocaleData } from '@angular/common';
import es from '@angular/common/locales/es';
import { SalesServiceService } from '../services/sales-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { RegisterSale, Product } from '../models/sales.models';
import { SalesResolveService } from '../services/sales-resolve.service'
import { DatePipe } from '@angular/common';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
// import { listLocales } from 'ngx-bootstrap/chronos';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import {Location} from '@angular/common';
import 'rxjs/add/operator/map'
import { Subject } from 'rxjs/Subject';



@Component({
  selector: 'app-register-sale',
  templateUrl: './register-sale-new.component.html',
  styleUrls: ['./register-sale.component.scss'],
})



export class RegisterSaleComponent implements OnInit {

  documento = [
    {"name": "Boleta Manual", "value": "boleta manual"},
    {"name": "Factura", "value": "factura"},
    {"name": "Boleta Electronica","value": "boleta electronica"},
    {"name": "Boleta Transbank", "value" :"boleta transbank"},
    {"name": "Sin documento", "value": "sdt"}
   ]

   canales = [
     {"name":"Presencial", "channel": 10, "value":"presencial", "image": "assets/images/presencial.png"},
     {"name": "Extra Mercadolibre", "channel": 11, "value":"extra mercadolibre", "image": "assets/images/extra_ml.png"},
     {"name": "Facebook", "channel": 14, "value":"facebook", "image": "assets/images/facebook.png"},
     {"name": "Yapo", "channel": 7, "value":"yapo", "image": "assets/images/yapo.png"},
     {"name": "Instagram", "channel": 16, "value":"instagram", "image": "assets/images/instagram.png"},
     {"name":"Mercadolibre", "channel": 1, "value":"mercadolibre", "image": "assets/images/mercadolibre.png"},
     {"name":"Falabella", "channel": 8, "value":"falabella",  "image": "assets/images/falabella.png"},
     {"name":"Groupon", "channel": 6, "value":"groupon", "image": "assets/images/groupon.png"},
     {"name": "Paris", "channel": 4, "value":"paris", "image": "assets/images/paris.png"},
     {"name": "Linio", "channel": 5, "value":"linio", "image": "assets/images/Linio_logo.svg.png"},
     {"name": "Ripley", "channel": 3, "value":"ripley", "image": "assets/images/ripley.png"}, 
     {"name": "Mercadoshop", "channel": 13, "value":"mercadoshop", "image": "assets/images/mercadoshop.png"},
     {"name": "Perdida", "channel": 15, "value":"perdida", "image": "assets/images/lost.png"},
     {"name": "Otro canal", "channel": 12, "value":"otro canal", "image": "assets/images/otro_canal.png"}
     

   ]

   forma_pago = [
    {"name":"Efectivo", "value": "efectivo"},
    {"name":"Tarjeta de debito", "value": "tarjeta de debito"},
    {"name":"Tarjeta de credito", "value": "tarjeta de credito"},
    {"name":"Transferencia", "value": "transferencia"},
    {"name":"cheque", "value": "cheque"},
    {"name":"por pagar", "value": "por pagar"},
    {"name":"sin pago", "value" : "sin pago"},
    {"name":"Transferencia Retail", "value": "transferencia retail"},
    {"name":"Mixto", "value": "Mixto"},
    {"name":"Boleta Manual", "value": "boleta manual"}
   ]

   responsable_envio = [
     {"name": "Comprador", "value": "comprador"},
     {"name": "Vendedor", "value": "vendedor"},
     {"name":"Retira personalmente", "value": "retira personalmente"}
   ]

  searchTerm$ = new Subject<string>();
  @ViewChild('myTable2') table: any;
  registerClick:boolean = true;
	register: RegisterSale;
  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  locale = 'es';
  message:string;
  user: string;
  date_format: Date;
  maxDate: Date;
  product: Product = new Product();
  number_document: boolean = true;
  channel:any;
  confirm_image:any = "assets/images/presencial.png";
  rows = [];
  rows2 = [];
  // date_register:string = this.formatDate(new Date());
  data:any;
	description: any;
  col_12:boolean;
  new_data :any;
	 private subscription: Subscription;
  constructor(
    private saleService: SalesServiceService,
    private router: Router,
    private toastr: ToastrService,
    private route: ActivatedRoute,
    private changeDetector: ChangeDetectorRef,
    private localeService: BsLocaleService,
    private location: Location,
    private modalService: BsModalService

  	) { 
    // defineLocale(this.locale);
    this.localeService.use('es');
    this.maxDate = new Date();
    // this.maxDate.setDate(this.maxDate.getDate() + 7);
      let id = this.route.snapshot.paramMap.get('id');
      if (id !== null){
        this.register = new RegisterSale();
        this.new_data = this.route.snapshot.data.sale_data;
        this.register.id_venta = this.new_data.id;
        this.register.document_type = this.new_data.document_type;
        this.register.number_document = this.new_data.number_document;
        this.register.channel = String(this.new_data.channel);
        this.confirm_image = this.canales.filter(value =>{ return value.channel == this.register.channel})[0].image;
        this.register.sender_responsable = this.new_data.sender_responsable;
        this.register.payment_type = this.new_data.payment_type;
        this.register.order_channel = this.new_data.order_channel;
        this.register.comments = this.new_data.comments;
        this.register.products = this.new_data.sale_of_product;
        this.register.total_sale = Math.round(this.new_data.total_sale);
        // console.log(this.new_data)
        this.user = this.new_data.user_full_name;
        this.register.date_sale = new Date(new Date(this.new_data.date_with_slashed).toDateString())

      }else{
        this.register = new RegisterSale();
        this.register.products = []
        this.data = this.route.snapshot.data.user_data;
        this.register.date_sale = new Date(new Date(this.data.today_date).toDateString());
        this.user = this.data.first_name + " " + this.data.last_name
      }
      this.col_12 = true;
      this.saleService.searchProduct(this.searchTerm$)
      .subscribe(result => {
        // console.log(result);
        if (result['term'] == ''){
          this.data = [];
        }else{

        this.data = result['results'];
        }
        this.rows = this.data;
        return result;
      });
   
  }

  ngOnInit() {
  this.saleService.currentMessage.subscribe(message =>{
    registerLocaleData( es );
    this.col_12 = !this.col_12
    this.message = message
  } 
   )
  this.changeDetector.markForCheck();
  }

  // collapseSidebar() {
  //   this.data.changeMessage("Hello from Sibling")
  // }

  addProduct(): void{
  	if ((this.product.sku != null && Number(this.product.sku) > 999999) || Number(this.product.sku) == 1 || Number(this.product.sku) == 2){
  		this.description = this.saleService.findProductDescription(this.product.sku).subscribe(
  			result => {
          if (result.length == 0){
            this.toastr.error("No existe producto con SKU " + this.product.sku, "No existe");
          }else{
            this.register.products.push({
              id_of_product: result[0].id_of_product,
              id_sale: result[0].id_sale,
              sku: result[0].sku,  
              description:result[0].description,
              color:result[0].color,
              quantity:this.product.quantity,
              unit_price:this.product.unit_price,
              code_seven_digits:result[0].code_seven_digits,
              total:this.product.total,
            });
            this.register.total_sale += Math.round(this.product.total);
            this.product = new Product();
            // this.register.total = 0;
          }
          // if (this.register.products.some(item => item.sku == this.register.sku)){
          //   this.toastr.error("Producto ya ingresado previamente", "Error");
          // }else {
          // }
          // return result
          },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al añadir el producto", "Error");
          })
  	}
  	else {

  		this.toastr.error("Indique numero de SKU de 7 dígitos para poder añadir producto", "Error");
  	}
  }
  searchName(template: TemplateRef<any>) {
    this.rows = []
    this.modalRef = this.modalService.show(template);
  }
  searchVariation(template: TemplateRef<any>, row:any) {
    this.rows2 = row.product_code;
    this.modalRef2 = this.modalService.show(template, { class: 'w-25 nested-modal' });
  }
  selectProduct(row){
    // console.log(row);
    this.product.sku = row.sku;
    this.modalRef.hide()
  }
  selectVariation(row){
    // console.log(row);
    this.product.sku = row.code_seven_digits;
    this.modalRef2.hide()
    this.modalRef.hide()
  }

  setTotal(event): void {
    this.product.total = this.product.unit_price * this.product.quantity;
  }

  setUnitPrice(event): void {
    this.product.unit_price = this.product.total/this.product.quantity;
  }
  verifyShipping(event): void {
    if (this.product.sku === '1'){
      this.product.quantity = 1;
    }
  }
  disableNumberDocument(event): void {
    console.log(this.register.document_type)
    if (this.register.document_type == 'sdt'){
      this.number_document = true;
      this.register.number_document = null;
    }else{
      this.number_document = false;
    }
  }

  setBasedOnQuantity(event): void {
    this.product.total = this.product.unit_price * this.product.quantity;
    this.product.unit_price = this.product.total/this.product.quantity;
  }

  userData(): void{
      this.description = this.saleService.userData().subscribe(
        result => {
          this.user = result.first_name + " " + result.last_name
          return result
          },
          error =>{
            console.log("error");
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }


  filterChannelType(event, position): void {
    console.log(this.register.channel);
    switch (position) {
      case "1":
        this.register.sender_responsable = "";
        this.register.payment_type = "transferencia retail";
        this.register.document_type = "boleta manual";
        this.number_document = false;
        this.confirm_image = "assets/images/mercadolibre.png";
        this.register.channel = 1   
        break;
      case "3":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "transferencia retail";
        this.register.document_type = "boleta manual";
        this.number_document = false;
        this.confirm_image = "assets/images/ripley.png"
        this.register.channel = 3
        break;
      case "4":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "transferencia retail";
        this.register.document_type = "sdt";
        this.number_document = true;
        this.confirm_image = "assets/images/paris.png"
        this.register.channel  = 4 

        break;
      case "5":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "transferencia retail";
        this.register.document_type = "boleta manual";
        this.number_document = false;
        this.confirm_image = "assets/images/Linio_logo.svg.png"
        this.register.channel  = 5

        break;
      case "6":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "transferencia retail";
        this.register.document_type = "sdt";
        this.number_document = true;
        this.confirm_image = "assets/images/groupon.png"
        this.register.channel = 6

        break;
      case "7":
        this.register.sender_responsable = "";
        this.register.payment_type = "";
        this.register.document_type = "sdt";
        this.confirm_image = "assets/images/yapo.png"
        this.register.channel = 7

        break;
      case "8":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "transferencia retail";
        this.register.document_type = "sdt";
        this.number_document = true;
        this.confirm_image = "assets/images/falabella.png"
        this.register.channel = 8

        break;
      case 9:
        this.register.sender_responsable = "";
        this.register.payment_type = "";
        this.register.document_type = "boleta manual";
        this.number_document = false;

        break;
      case "10":
        this.register.sender_responsable = "retira personalmente";
        this.register.payment_type = "";
        this.register.document_type = "sdt";
        this.number_document = false;
        this.confirm_image = "assets/images/presencial.png";
        this.register.channel  = 10

        break;
      case "11":
        this.register.sender_responsable = "";
        this.register.payment_type = "";
        this.register.document_type = "sdt";
        this.number_document = false;
        this.confirm_image = "assets/images/extra_ml.png";
        this.register.channel = 11

        break;
      case "12":
        this.register.sender_responsable = "";
        this.register.payment_type = "";
        this.register.document_type = "sdt";
        this.number_document = false;
        this.confirm_image = "assets/images/otro canal.png";
        this.register.channel = 12
        break;
      case "13":
        this.register.sender_responsable = "";
        this.register.payment_type = "";
        this.register.document_type = "boleta manual";
        this.number_document = false;
        this.confirm_image = "assets/images/mercadoshop.png";
        this.register.channel = 13

        break;
      case "14":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "";
        this.register.document_type = "sdt";
        this.number_document = false;
        this.confirm_image = "assets/images/facebook.png";
        this.register.channel = 14

        break;
      case "15":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "";
        this.register.document_type = "";
        this.confirm_image = "assets/images/lost.png";
        this.register.channel = 15

        break;
      case "16":
        this.register.sender_responsable = "comprador";
        this.register.payment_type = "";
        this.register.document_type = "sdt";
        this.number_document = false;
        this.confirm_image ="assets/images/instagram.png";
        this.register.channel = 16

        break;
      default:
        // code...
        break;
    }
  }

  removeProduct(index): void{
    if (confirm('¿Desea eliminar este producto?')){
      this.register.total_sale -= this.register.products[index].total
      this.register.products.splice(index, 1)
    }
  }

  backLogin(): void {
    this.toastr.info("Has cerrado sesión exitosamente", "Hasta pronto");
    localStorage.clear();
    this.router.navigate(['/auth/login']);
  };

  cleanForm(): void{
    this.register = new RegisterSale();
    this.register.products = []
  }
  changeDate(event): void {
    this.date_format = new Date(this.register.date_sale)
  }
  

   registerSale() {
        // console.log(this.register)
        
        if (this.date_format !== undefined){
          this.register.date_sale = this.date_format;
        }

        if (this.new_data !== undefined) {
          // console.log(this.register);
          this.register.bill = false;
          this.subscription = this.saleService.updateSale(this.register)
          .subscribe(
              result => {
                this.toastr.success("Venta actualizada exitosamente", "Bien");
                // this.router.navigate(['/sale/retail-sales'])
                this.location.back();
                // window.location.reload();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
          // console.log("Actualizar venta");
        }else{
          if (this.register.document_type === 'boleta electrónica' || this.register.document_type === 'sdt'){
            if (confirm("¿Desea emitir boleta electrónica de esta venta?")){
              // if(confirm("¿Desea imprimir boleta electrónica?")){
              //   this.register.print_bill = true;
              // }else{
              //   this.register.print_bill = false;
              // }
              this.register.print_bill = true;
              this.register.bill = true;
            }else{
              this.register.bill = false;
              this.register.print_bill = false;
            }

          }else {
            this.register.bill = false;
          }
          if (this.register.document_type === 'factura' && this.register.number_document == null){
            this.register.bill_fact = true;
          }
          // console.log(this.register);
        this.registerClick = false;
        this.subscription = this.saleService.createSale(this.register)
        .subscribe(
          result => {
            // if(result != null){
              // this.router.navigate(['/sale/list-daily'])
              this.toastr.success("Venta guardada exitosamente", "Bien");
              this.confirm_image = "assets/images/presencial.png"
              if (this.register.print_bill){
                  const linkSource = 'data:application/pdf;base64,' + result['pdf'];
                  const downloadLink = document.createElement("a");
                  const fileName = "Boleta " + result['number_document'] + ".pdf";

                  let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                      'resizable,screenX=10,screenY=10,width=1050,height=1050';

                  let htmlPop = '<embed width=100% height=100%'
                                   + ' type="application/pdf"'
                                   + ' src="data:application/pdf;base64,'
                                   + escape(result['pdf'])
                                   + '"></embed>'; 

                  let printWindow = window.open ("", "PDF", winparams);
                  printWindow.document.write(htmlPop);
                  downloadLink.href = linkSource;
                  downloadLink.download = fileName;
                  downloadLink.click();
                }
               if(this.register.bill_fact){
                 // console.log(result);
                const linkSource = 'data:application/pdf;base64,' + result['pdf'];
                const downloadLink = document.createElement("a");
                const fileName = "Factura " + result['number_document'] + ".pdf";

                let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                    'resizable,screenX=10,screenY=10,width=1050,height=1050';

                let htmlPop = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + escape(result['pdf'])
                                 + '"></embed>'; 

                let printWindow = window.open ("", "PDF", winparams);
                printWindow.document.write(htmlPop);
                downloadLink.href = linkSource;
                downloadLink.download = fileName;
                downloadLink.click();
               }
              this.register = new RegisterSale();
              this.register.products = [];
              this.register.date_sale = new Date();
              this.product = new Product();
              this.number_document = false;
              this.registerClick = true;

            // }
          },
          error =>{
            // console.log(error.error);
              this.register.date_sale = new Date();

            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
            this.registerClick = true;
          });
        }
      }

  formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

}

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { SalesRoutingModule } from './sales-routing.module';
import { RegisterSaleComponent } from './register-sale/register-sale.component';
import { DetailSaleComponent } from './detail-sale/detail-sale.component';
import { SalesComponent } from './sales.component';
import { FormsModule } from '@angular/forms';
import { DailySalesComponent } from './daily-sales/daily-sales.component';
import { PaginationModule } from 'ngx-bootstrap/pagination';
import { ModalModule } from 'ngx-bootstrap/modal';
import { MainComponent } from './main/main.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { LayoutModule } from '../@layout/layout.module';
import { RetailSalesComponent } from './retail-sales/retail-sales.component';
import { ChargeRetailComponent } from './charge-retail/charge-retail.component'
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { UpdateRipleyComponent } from './update-ripley/update-ripley.component';
import { UploadGrouponFalabellaComponent } from './upload-groupon-falabella/upload-groupon-falabella.component';
import { UploadParisComponent } from './upload-paris/upload-paris.component';


defineLocale('es', esLocale);
const modules = [
        SalesRoutingModule,
        CommonModule,
        FormsModule,
        LayoutModule,
        PaginationModule.forRoot(),
        ModalModule.forRoot(),
        BsDatepickerModule.forRoot(),
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatCardModule,
        MatAutocompleteModule,
        NgxDatatableModule 
];


const COMPONENTS = [
  RegisterSaleComponent,
  DetailSaleComponent,
  SalesComponent,
  DailySalesComponent,
  RetailSalesComponent,
  MainComponent,
  ChargeRetailComponent,
  UpdateRipleyComponent,
  UploadGrouponFalabellaComponent,
  UploadParisComponent
];

@NgModule({
  imports: [...modules],
  declarations: [...COMPONENTS,]
})
export class SalesModule { }

import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { AuthServiceService } from '../services/auth-service.service';
import { ToastrService } from 'ngx-toastr';

@Component({
  selector: 'app-recover-password',
  templateUrl: './recover-password.component.html',
  styleUrls: ['./recover-password.component.scss']
})
export class RecoverPasswordComponent implements OnInit {
	change:any = {};
  constructor(private route: ActivatedRoute, private authenticationService: AuthServiceService, private toastr: ToastrService, private router: Router) {
  	this.change.token = this.route.snapshot.params.token;
  	this.route.queryParams.subscribe(params => {
        this.change.token = params['token'];
    });
   }

  ngOnInit() {
  }
sendNewPassword(): void {
	return this.authenticationService.changeRequest(this.change)
    .subscribe(
      result => {
        
        	// console.log(result)

          this.toastr.success("Contraseña cambiada exitosamente", "Bien");
          this.router.navigate(['/auth/login']);
      },
      error =>{
      	// console.log(error.error);
        this.toastr.clear();
        // this.toastr.error("Credenciales Invalidas", "Error");
        for (let entry in error.error){
           this.toastr.error(error.error[entry][0], "Error");
        }
      });
}
}

import { Component, OnInit } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-forgot',
  templateUrl: './forgot.component.html',
  styleUrls: ['./forgot.component.scss']
})
export class ForgotComponent implements OnInit {

  public forgot: any = {};
  // private fields: any = {}
  constructor(private authenticationService: AuthServiceService, private toastr: ToastrService, private router: Router) { }

  ngOnInit() {
  }

  forgotSend(): any {
  	// console.log(this.forgot);
  	return this.authenticationService.forgotRequest(this.forgot)
        .subscribe(
          result => {
            
            	console.log(result)

              this.toastr.success(result.email[0], "Bien");
              this.router.navigate(['/auth/login']);
          },
          error =>{
          	console.log(error.error);
            this.toastr.clear();
            // this.toastr.error("Credenciales Invalidas", "Error");
            for (let entry in error.error){
               this.toastr.error(error.error[entry][0], "Error");
            }
          });
  }
  backLogin(): void {
  	this.router.navigate(['/auth/login']);
  }

}

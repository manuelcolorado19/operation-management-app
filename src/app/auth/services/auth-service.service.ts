import { Injectable } from '@angular/core';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { Observable, throwError, interval } from 'rxjs';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { of } from 'rxjs';
import { JwtHelperService } from '@auth0/angular-jwt';
import { XhrErrorHandlerService } from "../../services/xhr-error-handler.service";
import { url } from '../../../global';
interface TokenResponse {
  token: string;
}

@Injectable({
  providedIn: 'root'
})
export class AuthServiceService {
  public token: string;
  constructor(
    public http: HttpClient,
    public jwtHelper: JwtHelperService,
    private xhrErrorHandlerService: XhrErrorHandlerService,
  ) { }

  login(loginParams: any): Observable<boolean>{
        return this.http.post(url+'login/',loginParams)
          .pipe(
            map((response: TokenResponse) => {
                let token = response.token;
                if (token) {
                    this.token = token;
                    localStorage.setItem('currentUser', JSON.stringify({ token: token }));
                    return true;
                } else {
                    return false;
                }
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }


    forgotRequest(forgotParam: any): any{
        return this.http.post(url+'forgot/password/',forgotParam)
          .pipe(
            map((response: any) => {
                
                    return response;
               
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }

    changeRequest(changeParam: any): any{
        return this.http.post(url+'change/password/',changeParam)
          .pipe(
            map((response:any) => {
                    return response;
               
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }

    refresh(token): Observable<boolean> {
    return this.http.post(url+'refresh/', { token:token })
    .pipe(
      map((response: any) => {
        const token = response.access;
        localStorage.removeItem('currentUser');
        localStorage.setItem('currentUser', JSON.stringify( {token: token}));
        // var currentUser = JSON.parse(localStorage.getItem('currentUser'));
        // this.token = currentUser && currentUser.token;
        return true;
      }),
      catchError(this.xhrErrorHandlerService.handleError)
    )
  }

  logout(): void {
        this.token = null;
        localStorage.removeItem('currentUser');
        localStorage.removeItem('state');
        location.href = '/';
    }




}
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { LoginComponent } from './login/login.component';
import { AuthRoutingModule } from './auth-routing.module';
import {BrowserAnimationsModule} from '@angular/platform-browser/animations';
import { FormsModule }   from '@angular/forms';
import { AuthComponent } from './auth.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule
} from '@angular/material';
import { RegisterComponent } from './register/register.component';
import { ConfirmComponent } from './confirm/confirm.component';
import { ForgotComponent } from './forgot/forgot.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';

const modules = [
        MatButtonModule,
        MatFormFieldModule,
        MatInputModule,
        MatRippleModule,
        MatCardModule,
        AuthRoutingModule,
        CommonModule,
        FormsModule
];

const COMPONENTS = [
  LoginComponent,
  AuthComponent
];

@NgModule({
  imports: [...modules],
  declarations: [...COMPONENTS, RegisterComponent, ConfirmComponent, ForgotComponent, RecoverPasswordComponent],
})
export class AuthModule { }

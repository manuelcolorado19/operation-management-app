import { RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';
import { LoginComponent } from './login/login.component';
import { ForgotComponent } from './forgot/forgot.component';
import { AuthComponent } from './auth.component';
import { RecoverPasswordComponent } from './recover-password/recover-password.component';


const appRoutes : Routes = [{
  path: '',
  component: AuthComponent,
  children: [
    {
      path: 'login',
      component: LoginComponent,
      data: { title: 'Asiamerica | Login' }
    },
    {
      path: 'forgot',
      component: ForgotComponent,
      data: { title: 'Asiamerica | Olvido de contraseña' }
    },
    {
      path: 'change-password',
      component: RecoverPasswordComponent,
      data: { title: 'Asiamerica | Cambio de contraseña' }
    },
    { path: '', redirectTo: 'login', pathMatch: 'full' }
  ]
}];

@NgModule({
  imports: [
    RouterModule.forChild(appRoutes)
  ],
  exports: [RouterModule],
  providers: []
})
export class AuthRoutingModule { }

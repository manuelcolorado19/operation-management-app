import { Component } from '@angular/core';

@Component({
  selector: 'rm-auth',
  styleUrls: ['./auth.component.scss'],
  template: `
    <router-outlet></router-outlet>
  `,
})
export class AuthComponent {
}

import { Component, OnInit, OnDestroy } from '@angular/core';
import { AuthServiceService } from '../services/auth-service.service';
import { Router } from '@angular/router';
import { ToastrService } from 'ngx-toastr';
import { Subscription } from 'rxjs/Subscription';
import { url_front } from '../../../global';
@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
   loginParams: any = {};
  private subscription: Subscription;
  public busy: boolean;
  public url:string = url_front;
  private fields = {
    username: "E-mail",
    password: "Contraseña"
  };

  constructor(
    private router: Router,
    private authenticationService: AuthServiceService,
    private toastr: ToastrService,
  ) { }

  ngOnInit() {
  }

  login() {
        this.subscription = this.authenticationService.login(this.loginParams)
        .subscribe(
          result => {
            if(result == true){
              this.toastr.success("Has ingresado correctamente", "Bienvenido");
              // this.router.navigate(['/sale/register']);

              window.location.href = this.url + 'sale/';
              // console.log("hola inside")
            }
            this.busy = false;
          },
          error =>{
            this.busy = false;
            this.toastr.clear();
            this.toastr.error("Credenciales Invalidas", "Error");
            for (let entry in error.errMsg){
               this.toastr.error(error.errMsg[entry][0], this.fields[entry]);
            }
          });
      }

}

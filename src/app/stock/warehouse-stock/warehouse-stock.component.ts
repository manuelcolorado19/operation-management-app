import { Component, OnInit, ViewChild, TemplateRef } from '@angular/core';
import { StockServiceService } from '../services/stock-service.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { SalesServiceService } from '../../sales/services/sales-service.service';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { Router } from '@angular/router';
import { Page } from '../../sales/models/sales.models';
import { Subject } from 'rxjs/Subject';

@Component({
  selector: 'app-warehouse-stock',
  templateUrl: './warehouse-stock.component.html',
  styleUrls: ['./warehouse-stock.component.scss']
})
export class WarehouseStockComponent implements OnInit {

	@ViewChild('myTable') table: any;
  @ViewChild('editTmpl') editTmpl: TemplateRef<any>;
  @ViewChild('hdrTpl') hdrTpl: TemplateRef<any>;
	date_shipping_guide: Date = new Date();
	expanded: any = {};
	modalRef: BsModalRef;
  searchTerm$ = new Subject<string>();
	sku:string;
  page = new Page();
	quantity:number = 1;
  boxes:number;
	products_guide = [];
	locale = 'es';
  params:any = {};
  warehouse_id:string;
  data:any = [];
  warehouses: any = [];

  constructor(private stockService:StockServiceService, private toastr:ToastrService, private modalService: BsModalService, private saleService: SalesServiceService,private localeService: BsLocaleService, private router:Router) { 
  	this.localeService.use('es');
    this.warehouse_id = "1";
    this.params.page = 1;
    this.params.search = "";
  	this.listData();
    this.stockService.warehouseStockProductsearch(this.searchTerm$)
      .subscribe(result => {
        this.data = result['results'];
        this.warehouses = result['warehouses'];
        this.page.totalElements = result['count'];
        for (let i of this.data){
          let sale_pending = 0;
          let break_stock = 0;
          let total = 0;
          let office_count = 0;
          let to_bring = 0;
          let warehousedistribution = [];
          for (let y of this.warehouses){
            warehousedistribution.push({
              name:y.name,
              stock:0
            })
          }
          for (let j of i.product_code){
            let total_warehouses = 0;
            sale_pending += j.sales_pending
            office_count += j.office_stock;
            break_stock += j.break_stock;
            total = j.sales_pending + j.break_stock;
            for (let k of j.warehousedistribution){
              for (let y of warehousedistribution){
                if(k.name == y.name){
                  if (y.hasOwnProperty('stock')){
                    y.stock += k.stock;
                    total_warehouses += k.stock;
                  }else{
                    y.stock = 0
                  }
                }
              }
            }
            j.total_warehouses = total_warehouses;
            if (total_warehouses == 0){
              j.to_bring = 0;
            }else{
              let to_bring2 = (j.sales_pending + 1) - (j.office_stock);
              console.log(j);
              if (to_bring2 > 0){
                j.to_bring = to_bring2;
              }else{
                j.to_bring = 0;
              }
            }
            to_bring += j.to_bring;
            j.total = total;
          }
          i.office_count = office_count;
          i.sale_pending = sale_pending;
          i.break_stock = break_stock;
          i.total = total;
          i.to_bring = to_bring;
          i.warehousedistribution = warehousedistribution;
          // console.log(i);
        }
          this.data = [...this.data];
          return result;

      });
  }
  ngOnInit() {
  }
  listData(){
  	this.stockService.warehouseStockProduct(this.params).subscribe(
      result=> {
        this.data = result.results
        this.warehouses = result.warehouses;
        this.page.totalElements = result.count;
        for (let i of this.data){
          let sale_pending = 0;
          let break_stock = 0;
          let total = 0;
          let office_count = 0;
          let to_bring = 0;
          let warehousedistribution = [];
          for (let y of this.warehouses){
            warehousedistribution.push({
              name:y.name,
              stock:0
            })
          }
          for (let j of i.product_code){
            let total_warehouses = 0;
            sale_pending += j.sales_pending
            office_count += j.office_stock;
            break_stock += j.break_stock;
            total = j.sales_pending + j.break_stock;
            for (let k of j.warehousedistribution){
              for (let y of warehousedistribution){
                if(k.name == y.name){
                  if (y.hasOwnProperty('stock')){
                    y.stock += k.stock;
                    total_warehouses += k.stock;
                  }else{
                    y.stock = 0
                  }
                }
              }
            }
            j.total_warehouses = total_warehouses;
            if (total_warehouses == 0){
              j.to_bring = 0;
            }else{
              let to_bring2 = (j.sales_pending + 1) - j.office_stock;
              if (to_bring2 > 0){
                j.to_bring = to_bring2;
              }else{
                j.to_bring = 0;
              }
            }
            to_bring += j.to_bring;
            j.total = total;
          }
          i.office_count = office_count;
          i.sale_pending = sale_pending;
          i.break_stock = break_stock;
          i.total = total;
          i.to_bring = to_bring;
          i.warehousedistribution = warehousedistribution;
          // console.log(i);
        }
          this.data = [...this.data];
          return result;

      // return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }
   toggleExpandRow(row, rowGrandIndex) {
    // console.log(this.table);
    this.table.rowDetail.toggleExpandRow(row);
  }
  createRetirementGuide(template: TemplateRef<any>){
    this.sku = "";
    this.modalRef = this.modalService.show(template);

  }

  exportStockWareHouse(){
  	console.log("hola");
  }

  addProduct(): void{
    console.log(this.boxes)
  	if (this.sku != null && Number(this.sku) > 999999){
  		this.saleService.findProductDescription(this.sku).subscribe(
  			result => {
          if (result.length == 0){
            this.toastr.error("No existe producto con SKU " + this.sku, "No existe");
          }else{
            // console.log(result);
            if (this.boxes != undefined){
              if (result[0].box_quantity != null){
                this.quantity = this.boxes * result[0].box_quantity
              }else{
                alert("Disculpe no se encuentra registro del número de productos por caja, ingrese cantidad");
                this.boxes = undefined
                return

              }
            }
            this.products_guide.push({
            	sku:result[0].sku,
      				description:result[0].description,
      				code_seven_digits:result[0].code_seven_digits,
      				color:result[0].color,
      				quantity:this.quantity,
      				unit_price:1,
      				total:1 * this.quantity,
            });
            this.sku = "";
            this.quantity = 1;
            this.boxes = undefined
            // this.register.total = 0;
          }
          },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al añadir el producto", "Error");
          })
  	}
  	else {

  		this.toastr.error("Indique numero de SKU de 7 dígitos para poder añadir producto", "Error");
  	}
  }
  removeProduct(index): void{
    if (confirm('¿Desea eliminar este producto?')){
      this.products_guide.splice(index, 1)
    }
  }
  generateGuide():void{
  	let data = {
       date_retirement:this.formatDate(this.date_shipping_guide),
       warehouse:this.warehouse_id,
       products:this.products_guide
    }
    this.stockService.newRetirement(data).subscribe(
      result=> {
        console.log(result);
        this.toastr.success("Retiro creado exitosamente", "Bien");
        const downloadLink = document.createElement("a");
        const fileName = "Guía de Retiro " + result.document_number+ ".pdf";
        const linkSource = 'data:application/pdf;base64,' + result['pdf'];

          let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
              'resizable,screenX=10,screenY=10,width=1050,height=1050';

          let htmlPop = '<embed width=100% height=100%'
                           + ' type="application/pdf"'
                           + ' src="data:application/pdf;base64,'
                           + escape(result['pdf'])
                           + '"></embed>'; 

          let printWindow = window.open ("", "PDF", winparams);
          printWindow.document.write(htmlPop);
          downloadLink.href = linkSource;
          downloadLink.download = fileName;
          downloadLink.click();
          this.modalRef.hide();
          this.listData();
          this.products_guide = [];

      return result;
      },
      error=> {
        this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
      }
    )
  }
    formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year,month,day].join('-');
  }
  uploadStockWareHouse(){
    this.router.navigate(['stock/upload-warehouse'], { queryParams: {warehouse: this.warehouse_id, warehouse_house: this.data.name}});
  }
  setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    // console.log(this.params)
    this.listData();
  }

}

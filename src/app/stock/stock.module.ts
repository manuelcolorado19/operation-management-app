import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { StockRoutingModule } from './stock-routing.module';
import { StockComponent } from './stock.component';
import { ListProductsComponent } from './list-products/list-products.component';
import { LayoutModule } from '../@layout/layout.module'
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';
import { ChannelAsignationComponent } from './channel-asignation/channel-asignation.component';
import { FormsModule } from '@angular/forms';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';

import { ModalModule } from 'ngx-bootstrap/modal';
import { StockHistoricComponent } from './stock-historic/stock-historic.component';
import { WarehouseStockComponent } from './warehouse-stock/warehouse-stock.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { UploadWarehouseComponent } from './upload-warehouse/upload-warehouse.component';
import { TimepickerModule } from 'ngx-bootstrap/timepicker';

defineLocale('es', esLocale);
const COMPONENTS = [
  StockComponent,
  ListProductsComponent,
  ChannelAsignationComponent,
];


@NgModule({
  imports: [
    CommonModule,
    LayoutModule,
    StockRoutingModule,
    MatButtonModule,
  	MatFormFieldModule,
  	MatAutocompleteModule,
  	MatInputModule,
  	MatRippleModule,
  	MatCardModule,
    ModalModule.forRoot(),
    BsDatepickerModule.forRoot(),
    TimepickerModule.forRoot(),
    FormsModule,
    NgxDatatableModule
  ],
  declarations: [...COMPONENTS, StockHistoricComponent, WarehouseStockComponent, UploadWarehouseComponent],

})
export class StockModule { }

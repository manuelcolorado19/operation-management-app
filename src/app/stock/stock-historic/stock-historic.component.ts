import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Page } from '../../sales/models/sales.models';
import { StockServiceService } from '../services/stock-service.service';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import * as XLSX from 'xlsx';
import { BsLocaleService } from 'ngx-bootstrap/datepicker';

@Component({
  selector: 'app-stock-historic',
  templateUrl: './stock-historic.component.html',
  styleUrls: ['./stock-historic.component.scss']
})
export class StockHistoricComponent implements OnInit {
  rows = []
  rows2 = []
  @ViewChild('file_input') 
 file_input: ElementRef;
  modalRef: BsModalRef;
  mytime: Date = new Date();
  hour:Date = new Date();
  date_format: Date;
  date_stock_break: Date = new Date();
  page = new Page();
  params:any = {};
  constructor(private toastr:ToastrService, private stockService: StockServiceService, private modalService: BsModalService, private localeService: BsLocaleService) { 
    this.params.page = 1;
    this.localeService.use('es');
    this.listData();
  }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
      this.toastr.error("No se pueden cargar múltiples archivos", "Error");
      throw new Error('Cannot use multiple files');  
    } 
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      // console.log(ws);
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1
      // delete ws.H1

      /* save data */
      this.rows2 = this.validateData(wb,XLSX.utils.sheet_to_json(ws, {header: 1}));
      // console.log(this.data_upload)
    this.rows2 = [...this.rows2];
    };
    reader.readAsBinaryString(target.files[0]);
  }
  validateData(wb,data:any) {
    data.splice(0, 1);
    let data_json = []
    for (let row of data ) {
      data_json.push({
        sku:row[0],
        description:row[1],
        variation:row[2],
        code_seven:row[3],
        total_stock:row[4],
        stock_bad:row[5],
        stock_detail:row[6],
      });
    }
    return data_json;
  }
cleanTable(event): any {
    this.file_input.nativeElement.value = "";
    this.rows2 = [];
  }
  downloadFile(row_id, row_name) {
	this.stockService.exportExcelInventory(row_id, row_name);
}
listData() {
	this.stockService.getInventory(this.params).subscribe(
      result=> {
        this.rows = result.results
        this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
}
setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    this.listData();

  }

  openModal(template){
    this.modalRef = this.modalService.show(template);

  }
  sendData(event){
    let date_time = this.formatDate(this.date_stock_break) + " " + this.formatHour(this.hour);
    let data = {
      date_stock_break:date_time,
      data_rows:this.rows2
    }
    this.stockService.loadInventory(data).subscribe(
      result=> {
        console.log(result);
        this.modalRef.hide();
        this.toastr.success("Inventario cargado exitosamente", "Bien");
        this.listData();
        // this.rows = result.results
        // this.page.totalElements = result.count;

      return result;
      },
      error=> {
        this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
      }
    )
  }

  formatDate(date) {
    let d = new Date(date),
      month = '' + (d.getMonth() + 1),
      day = '' + d.getDate(),
      year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [day, month, year].join('-');
  }

  formatHour(date) {
    let d = new Date(date),
        hour = '' + (d.getHours()),
        minute = '' + d.getMinutes()

    if (hour.length < 2) hour = '0' + hour;
    if (minute.length < 2) minute = '0' + minute;

    return [hour, minute].join(':');
  }

}

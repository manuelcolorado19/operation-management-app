import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { StockHistoricComponent } from './stock-historic.component';

describe('StockHistoricComponent', () => {
  let component: StockHistoricComponent;
  let fixture: ComponentFixture<StockHistoricComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ StockHistoricComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(StockHistoricComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { StockComponent } from './stock.component';
import { ListProductsComponent } from './list-products/list-products.component';
import { ChannelAsignationComponent } from './channel-asignation/channel-asignation.component';
import { StockResolveService } from './services/stock-resolve.service'
import { StockHistoricComponent } from './stock-historic/stock-historic.component';
import { WarehouseStockComponent } from './warehouse-stock/warehouse-stock.component';
import { UploadWarehouseComponent } from './upload-warehouse/upload-warehouse.component';

const appRoutes : Routes = [{
  path: '',
  component: StockComponent,
  children: [
    {
      path: 'list-products',
      component: ListProductsComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Listado de Productos' }
    },
    {
      path: 'stock-warehouse',
      component: WarehouseStockComponent,
      canActivate: [],
      data: { title: 'Asiamerica | Listado de Productos en Bodega' }
    },    
    // {
    //   path: 'channel-asignation',
    //   canActivate: [],
    //   component: ChannelAsignationComponent,
    //   data: { title: 'Asiamerica | Distribución por canales' }
    // },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes)
  ],
  exports:[RouterModule],
  declarations: [],
  providers: [StockResolveService]
})
export class StockRoutingModule { }

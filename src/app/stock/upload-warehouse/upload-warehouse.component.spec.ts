import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { UploadWarehouseComponent } from './upload-warehouse.component';

describe('UploadWarehouseComponent', () => {
  let component: UploadWarehouseComponent;
  let fixture: ComponentFixture<UploadWarehouseComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ UploadWarehouseComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(UploadWarehouseComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

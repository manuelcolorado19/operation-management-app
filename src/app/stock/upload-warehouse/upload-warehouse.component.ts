import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import * as XLSX from 'xlsx';
import * as moment from 'moment';
import {Location} from '@angular/common';
import { Router, ActivatedRoute } from '@angular/router';
import { StockServiceService } from '../services/stock-service.service';

@Component({
  selector: 'app-upload-warehouse',
  templateUrl: './upload-warehouse.component.html',
  styleUrls: ['./upload-warehouse.component.scss']
})
export class UploadWarehouseComponent implements OnInit {

	data_rows = [];
	@ViewChild('file_input') 
	file_input: ElementRef;
	data_upload:any = [];
	warehouse_name:string;
	warehouse_id:number;

  constructor(private toastr: ToastrService, private location: Location, private route: ActivatedRoute, private stockService: StockServiceService) { 
  	this.warehouse_name = this.route.snapshot.queryParams.warehouse_house;
  	this.warehouse_id = this.route.snapshot.queryParams.warehouse;
  }

  ngOnInit() {
  }

  onFileChange(evt: any) {
    const target: DataTransfer = <DataTransfer>(evt.target);
    if (target.files.length !== 1){
    	this.toastr.error("No se pueden cargar múltiples archivos", "Error");
    	throw new Error('Cannot use multiple files');	
    } 
    const reader: FileReader = new FileReader();
    reader.onload = (e: any) => {
      /* read workbook */
      const bstr: string = e.target.result;
      const wb: XLSX.WorkBook = XLSX.read(bstr, {type: 'binary', cellDates: false});
      const ws = wb.Sheets[wb.SheetNames[0]];
      const wsname: string = wb.SheetNames[0];
      delete ws.A1
      delete ws.B1
      delete ws.C1
      delete ws.D1
      delete ws.E1
      delete ws.F1
      delete ws.G1
      delete ws.H1
      /* save data */
      this.data_rows = this.validateData(wb,XLSX.utils.sheet_to_json(ws, {header: 1}));
      // console.log(this.data_upload)
    this.data_rows = [...this.data_rows];
    };
    reader.readAsBinaryString(target.files[0]);
  }
    validateData(wb,data:any) {
    data.splice(0, 1);
    const dateMode = wb.Workbook.WBProps.date1904;
    let data_json = []
    for (let row of data ) {
      data_json.push({
        sku:row[0],
        description:row[1],
        variation:row[2],
        initial_stock:row[3],
        date_count:XLSX.SSF.format('DD-MM-YYYY', row[4], { date1904: dateMode }),
      });
    }
    return data_json;
  }
  sendData(): void {
    if (this.data_rows.length === 0){
      this.toastr.clear();
      this.toastr.error("No existe data para cargar al sistema", "Error");
    }
    else{
      let data = {
        warehouse:this.warehouse_id,
        data:this.data_rows
      }
      this.stockService.loadInitialstockWarehouse(data)
          .subscribe(
              result => {
                this.toastr.success("Datos cargados de forma exitosa", "Bien");
                // this.data_rows = [];
                // this.router.navigate(['/sale/list-daily'])
                // this.location.back();

              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
      // this.saleService.sendRetailData(this.data_upload);

    }
    
  }

}

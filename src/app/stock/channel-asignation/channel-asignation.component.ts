import { Component, OnInit, TemplateRef } from '@angular/core';
import { StockServiceService } from '../services/stock-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-channel-asignation',
  templateUrl: './channel-asignation.component.html',
  styleUrls: ['./channel-asignation.component.scss']
})
export class ChannelAsignationComponent implements OnInit {
  data:any;
  modalRef: BsModalRef;
  total_asigned: number;
  total_not_asigned: number;
  public product_selected :any;

  constructor(private stockService: StockServiceService, private toastr:ToastrService, private route: ActivatedRoute, private modalService: BsModalService) {
  	this.retrieveData("1");
   }

  ngOnInit() {
  }

  openModal(template: TemplateRef<any>, item:any) {
    // console.log(sale);
    this.total_asigned = item.mercadolibre_asigned + item.ripley_asigned + item.paris_asigned + item.linio_asigned +item.groupon_asigned + item.falabella_asigned;

    this.total_not_asigned = item.not_asigned
    this.product_selected = item;
    this.modalRef = this.modalService.show(template);
  }
  changeAsignation(event): void {
    
    this.stockService.updateAsignation(this.product_selected).subscribe(
      result => {
        this.toastr.clear();
        this.toastr.success("Asignación cambiada con éxito","Bien")
        // window.location.reload();
        this.modalRef.hide();
        this.retrieveData("1");
        
      },
      error=> {
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
     )
  }

  retrieveData(page:string): any {
    
  	this.stockService.listChannels(page).subscribe(
  		result=> {
        // this.current_page = parseInt(page);
        // this.count = result.count;
        this.data = result.results;
        // if (page === '1' && this.total_pages.length == 0){
        //     this.total_pages = Array.from(Array(Math.ceil(this.count / 10) + 1), (_,x) => x);
        //     this.total_pages.splice(0, 1)
        //     if (this.total_pages.length > 1){
        //       this.pagination = true;
        //     }
        //   }
  			return result;
  		},
  		error=> {
  			this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
  		}
  	)
    }

    reset(event) {
      this.product_selected.mercadolibre_asigned = 0;
      this.product_selected.ripley_asigned = 0;
      this.product_selected.paris_asigned = 0;
      this.product_selected.linio_asigned = 0;
      this.product_selected.groupon_asigned = 0;
      this.product_selected.falabella_asigned = 0;
    }
    distributionEqual(event){

      // let asignation = this.product_selected.stock + this.product_selected.mercadolibre_asigned + this.product_selected.ripley_asigned + this.product_selected.paris_asigned + this.product_selected.linio_asigned + this.product_selected.groupon_asigned + this.product_selected.falabella_asigned
      // asignation = Number(asignation / 7);
      // this.product_selected.mercadolibre_asigned = asignation;
      // this.product_selected.mercadolibre_asigned = asignation;
      // this.product_selected.mercadolibre_asigned = asignation;
      // this.product_selected.mercadolibre_asigned = asignation;
      // this.product_selected.mercadolibre_asigned = asignation;
      // this.product_selected.mercadolibre_asigned = asignation;
      // this.product_selected.mercadolibre_asigned = asignation;

    }
  }




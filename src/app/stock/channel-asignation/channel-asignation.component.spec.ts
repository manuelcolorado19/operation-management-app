import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { ChannelAsignationComponent } from './channel-asignation.component';

describe('ChannelAsignationComponent', () => {
  let component: ChannelAsignationComponent;
  let fixture: ComponentFixture<ChannelAsignationComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ ChannelAsignationComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(ChannelAsignationComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

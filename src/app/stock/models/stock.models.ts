export class ListProduct {
  sku?: number;
  code_seven_digits?:number;
  description?: string;
  sale_quantity?: number;
  merma_quantity?: number;
  initial_stock?: number;
  left?: number;
  weeks_sales?: number;
  available_stock_weeks?: number;
  empty_stock_date?: Date = new Date();
}

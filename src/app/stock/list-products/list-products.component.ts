import { Component, OnInit, ElementRef, ViewChild, TemplateRef } from '@angular/core';
import { StockServiceService } from '../services/stock-service.service';
import { ToastrService } from 'ngx-toastr';
import { Router, ActivatedRoute } from '@angular/router';
import { ListProduct } from '../models/stock.models';
import { Subject } from 'rxjs/Subject';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';

import { Page } from '../../sales/models/sales.models';

@Component({
  selector: 'app-list-products',
  templateUrl: './list-products.component.html',
  styleUrls: ['./list-products.component.scss']
})
export class ListProductsComponent implements OnInit {
  data:any;
  // total_pages: any = [];
  // current_page:number;
  // pagination:boolean = true;
  last_inventory:string;
  quantity:number;
  params:any = {};
  product_selected:any;
  modalRef: BsModalRef;

  @ViewChild('myTable') table: any;

  searchTerm$ = new Subject<string>();
  resolve_data:any;
  count:number;
  no_click: boolean = true;

expanded: any = {};
  rows = []
  grandIndex:number;
  page = new Page();
  editing_fields:string = "";
  expandedId=[];


  constructor(private stockService: StockServiceService, private toastr:ToastrService, private route: ActivatedRoute, private elRef: ElementRef, private modalService: BsModalService, ) { 
    // this.data = this.route.snapshot.data.list_data.results;
    // console.log(this.data);
    // this.rows;
    this.params.page = 1;
    this.params.search = "";
    // console.log(this.params)
    this.resolve_data = this.route.snapshot.data.list_data;
  	this.retrieveData();
    this.stockService.search(this.searchTerm$)
      .subscribe(result => {
        // this.params.search = 
        this.rows = result['results'];
        this.page.pageNumber = 0;
        this.last_inventory = result['last_inventory']

        this.page.totalElements = result['count'];
        for (let i of this.rows){
          let initial_stock = 0;
          let initial_warehouse_stock = 0;
          let purchases_warehouse5 = 0;
          let sale_quantity = 0;
          let sale_pending = 0;
          let available_total = 0;

          for (let j of i.product_code){
            initial_stock += j.initial_stock;
            initial_warehouse_stock += j.initial_warehouse_stock;
            purchases_warehouse5 += j.purchases_warehouse5;
            sale_quantity += j.sale_quantity;
            sale_pending += j.sale_pending;
            available_total += (j.initial_stock + j.initial_warehouse_stock + j.purchases_warehouse5 - j.sale_quantity - j.sale_pending);
            
          }
          i.initial_stock = initial_stock;
          i.initial_warehouse_stock = initial_warehouse_stock;
          i.purchase_quantity = purchases_warehouse5;
          i.sale_quantity = sale_quantity;
          i.sale_pending = sale_pending;
          i.available_total = available_total;
        }
        this.rows = [...this.rows];
        return result;
      });

  }
  openModal(template: TemplateRef<any>, row:any) {
    this.product_selected = row;
    this.modalRef = this.modalService.show(template);
  }

  retrieveData(): any {
    // console.log(this.params)
  	this.stockService.listProducts(this.params).subscribe(
  		result=> {
        this.count = result.count;
        this.last_inventory = result['last_inventory']
        // this.data = result.results;
        this.rows = result.results;
        for (let i of this.rows){
          let initial_stock = 0;
          let initial_warehouse_stock = 0;
          let purchases_warehouse5 = 0;
          let sale_quantity = 0;
          let sale_pending = 0;
          let available_total = 0;

          for (let j of i.product_code){
            initial_stock += j.initial_stock;
            initial_warehouse_stock += j.initial_warehouse_stock;
            purchases_warehouse5 += j.purchases_warehouse5;
            sale_quantity += j.sale_quantity;
            sale_pending += j.sale_pending;
            available_total += (j.initial_stock + j.initial_warehouse_stock + j.purchases_warehouse5 - j.sale_quantity - j.sale_pending);
            
          }
          i.initial_stock = initial_stock;
          i.initial_warehouse_stock = initial_warehouse_stock;
          i.purchase_quantity = purchases_warehouse5;
          i.sale_quantity = sale_quantity;
          i.sale_pending = sale_pending;
          i.available_total = available_total;
        }
        this.rows = [...this.rows];
        // console.log(this.rows);
        this.page.totalElements = result.count;
  			return result;
  		},
  		error=> {
  			this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
  		}
  	)
    // }
  }

 toggleExpandRow(row, rowGrandIndex) {
    console.log(this.table);
    // console.log(this.expanded);
    this.table.rowDetail.toggleExpandRow(row);
    // const index=this.expandedId.indexOf(row.id);
    // if(index > -1)
    //   this.expandedId.splice(index,1);
    // else{
    //   this.expandedId.push(row.id);
    // }
  }



setPage(pageInfo){
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    console.log(this.params)
    this.retrieveData();
  }

  ngOnInit() {
  }

  exportStock() {
    this.stockService.exportExcel();
  }
  resetStock(){
    this.stockService.resetStockProduct(this.product_selected, this.quantity).subscribe(
      result=> {
        console.log(result);
        this.modalRef.hide();
        this.retrieveData();
        return result;
      },
      error=> {
        this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
      }
    )
  }

}

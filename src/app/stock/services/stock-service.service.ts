import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
import { url } from '../../../global';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { Observable } from 'rxjs/Observable';
import "rxjs/add/operator/debounceTime";
import "rxjs/add/operator/distinctUntilChanged";
import 'rxjs/add/operator/switchMap';
@Injectable({
  providedIn: 'root'
})
export class StockServiceService {
  public headers: HttpHeaders = new HttpHeaders();
  token: string;

  constructor(public http: HttpClient) { 
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
  	}
  }

  listProducts(params:any): any {
    // if (date !== null){

    // let format_date = date.getFullYear() + "-" + (date.getMonth() + 1) +"-" + date.getDate();
    return this.http.get(url+'product_sale_stock/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }

  listChannels(page:string): any {
    // if (date !== null){

    // let format_date = date.getFullYear() + "-" + (date.getMonth() + 1) +"-" + date.getDate();
    return this.http.get(url+'product_sale_channels/', {params:{page:page},headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }

  updateAsignation(data:any): any {
    console.log(data);
    return this.http.put(url+'product_sale_channels/' + data.id + "/", data, {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             }),

        )
  }

  search(terms: Observable<string>) {
    return terms.debounceTime(650)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntries(term));
  }

  searchEntries(term) {
    return this.http.get(url + 'product_sale_stock/',{params:{search:term},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
  exportExcel() {
    return this.http.get(url+'product_sale_stock_file/', {headers: this.headers, responseType: 'arraybuffer'})
    .subscribe(response => this.downLoadFile(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet"))
  }

  downLoadFile(data: any, type: string):any{
        
        const blob = new Blob([data], { type: type});
        const fileName: string = "Inventario Asiamerica.xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }
    
    getInventory(params:any){
      return this.http.get(url+'inventory/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
    }
    loadInventory(data:any): any {
    console.log(data);
    return this.http.post(url+'inventory/', data, {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             }),

        )
  }

  exportExcelInventory(inventory_id, name) {
    return this.http.get(url+'export/inventory/xlsx', {headers: this.headers, responseType: 'arraybuffer', params:{inventory:inventory_id}})
    .subscribe(response => this.downLoadFileInventory(response, "application/vnd.openxmlformats-officedocument.spreadsheetml.sheet", name))
  }

  downLoadFileInventory(data: any, type: string, name:string):any{
        
        const blob = new Blob([data], { type: type});
        const fileName: string = name + ".xlsx";
        const objectUrl: string = URL.createObjectURL(blob);
        const a: HTMLAnchorElement = document.createElement('a') as HTMLAnchorElement;
        a.href = objectUrl;
        a.download = fileName;
        document.body.appendChild(a);
        a.click();        

        document.body.removeChild(a);
        URL.revokeObjectURL(objectUrl);
    }

    resetStockProduct(row:any, quantity:number){
      return this.http.put(url+'product_sale_stock/'+row.id + "/", quantity, {headers: this.headers})
          .pipe(
            map((response: any) => {
              console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
    }
    warehouseStockProduct(params){
      return this.http.get(url+'stock-distribution/', {params:params, headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
    }
   newRetirement(data:any): any {
    // console.log(data);
    return this.http.post(url+'retirement/', data, {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             }),

        )
  }
  loadInitialstockWarehouse(data){
    return this.http.post(url+'load-warehouse/', data, {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             }),
        )
  }
  updateRetirement(data:any): any {
    return this.http.put(url+'retirement/' + data.id + "/", data, {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             }),

        )
  }

  searchRetirement(terms: Observable<string>) {
    return terms.debounceTime(700)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntriesRetirement(term));
  }

  searchEntriesRetirement(term) {
    return this.http.get(url + 'retirement/',{params:{search:term},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
  approvedRetirement(data){
    return this.http.put(url+'retirement/' + data.id + "/", data, {params:{status:data.status_retirement},headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             }),

        )
  }
  
  warehouseStockProductsearch(terms: Observable<string>) {
    return terms.debounceTime(700)
      .distinctUntilChanged()
      .switchMap(term => this.searchEntriesWarehouseStock(term));
  }
  searchEntriesWarehouseStock(term) {
    return this.http.get(url + 'stock-distribution/',{params:{search:term},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
}

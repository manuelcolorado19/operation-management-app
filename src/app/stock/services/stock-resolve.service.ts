import { Injectable } from '@angular/core';
import { Resolve, ActivatedRouteSnapshot, RouterStateSnapshot } from '@angular/router';
import { Http, HttpModule  } from '@angular/http';
import { Observable } from 'rxjs/Observable';
import { HttpHeaders, HttpClientModule } from '@angular/common/http';
import { StockServiceService } from './stock-service.service'
import { ActivatedRoute, Router  } from '@angular/router';

@Injectable()
export class StockResolveService implements Resolve<any>{

  constructor(private hnService: StockServiceService,  private router: Router,) {
  	// let url_sale = this.router.url.substr(this.router.url.lastIndexOf('/') + 1);
  	let url_sale = this.router.url;
  }

  resolve(route: ActivatedRouteSnapshot) {
    return this.hnService.listProducts('1');
  }
}


export class Product {
  id?: number;
  description?:string;
  sku?: string;
  quantity?: number;
  accesories?: string;
  
}

export class Client {
  name?: string;
  email?:string;
  phone?: string;
}
export class RegisterReturn {
  admission_date?:any = new Date();
  courier_selected?: string = 'courier';
  sale_id?: any;
  order_channel?: string;
  status_box?: string = 'intacta';
  comment?: string;
  client_request?: string = 'ninguno';
  client_comment?: string;
  products?: Product[];
  courier_id?: string = "1";
  status_package?: string = 'intacto';
  status_product?: string = 'intacto';
  bill_receive?: string = 'yes';
  missing?: string = 'no';
  client?: Client = null;
}

export class technical_report {
  technical_report?: string = '';
  return_sale?: string;
  technical_solution?: string = '';
  resolution?: string = null;
  client_contact?: string = null;
  retired?: string = null;
  // status_product_change?: string = ''
}

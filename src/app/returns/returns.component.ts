import { Component } from '@angular/core';

@Component({
  selector: 'rm-returns',
  //styleUrls: ['./auth.component.css'],
  template: `
  	<app-sidebar></app-sidebar>
    <router-outlet></router-outlet>
  `,
})
export class ReturnsComponent {
}

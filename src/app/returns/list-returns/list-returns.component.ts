import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/Subject';
import { ReturnsServiceService } from '../services/returns-service.service';
import { Page } from '../../sales/models/sales.models';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { technical_report } from '../models/returns.models';
@Component({
  selector: 'app-list-returns',
  templateUrl: './list-returns.component.html',
  styleUrls: ['./list-returns.component.scss']
})
export class ListReturnsComponent implements OnInit {
	rows = [];
  page = new Page();
  modalRef: BsModalRef;
  register:technical_report = new technical_report();
  return_selected: any;
  data:any = [];
  params: any = {};
	searchTerm$ = new Subject<string>();
  constructor(private returnService:ReturnsServiceService, private toastr:ToastrService, private modalService: BsModalService,) { 
    this.params.page = 1;
    this.page.pageNumber = 0;
    this.page.size = 10;
  	this.listData();

    this.returnService.search(this.searchTerm$, this.params)
      .subscribe(result => {
        this.page.pageNumber = 0
        this.params.page = 1
        this.page.totalElements = result['count'];

        this.data = result['results'];
        this.rows = this.data;
        return result;
      });
  }
  setPage(pageInfo){
    window.scroll(0,0);
    this.page.pageNumber = pageInfo.offset;
    this.params.page = pageInfo.offset + 1;
    // console.log(this.params)
    this.listData();

  }

  ngOnInit() {
  }

  listData(){
    this.returnService.listReturns(this.params).subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.page.totalElements = result.count;
          this.rows = result.results;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  openModal(template: TemplateRef<any>, row:any) {
    this.return_selected = row;
    this.modalRef = this.modalService.show(template);
  }
  openModal2(template: TemplateRef<any>) {
    this.modalRef.hide()
    this.register = new technical_report();
    this.register.return_sale = this.return_selected.id;
    this.modalRef = this.modalService.show(template);
  }
  generateTechnicalReport(){
     this.returnService.createTechnicalReport(this.register)
        .subscribe(
          result => {
              this.toastr.success("Informe Generado de forma exitosamente", "Bien");
              this.register = new technical_report();

              // this.products = [];
              // this.register.products = [];
              // this.sales = [];
              // this.sale_selected = null;

              const downloadLink = document.createElement("a");
              const fileName = "Informe técnico " +result['id']+ ".pdf";
              const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];

                let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                    'resizable,screenX=10,screenY=10,width=1050,height=1050';

                let htmlPop = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + escape(result['pdf_file'])
                                 + '"></embed>'; 

                let printWindow = window.open ("", "PDF", winparams);
                printWindow.document.write(htmlPop);
                this.modalRef.hide()
                this.listData();
  },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
      }

  reintegrateProduct(product){
    console.log(product);
    this.returnService.reintegrate(product, this.return_selected.id)
        .subscribe(
          result => {
              this.toastr.success("Producto reintegrado de forma exitosa", "Bien");
              this.register = new technical_report();
                this.modalRef.hide()
                this.listData();
  },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });
  }
}

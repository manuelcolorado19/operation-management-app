import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { ReturnsRoutingModule } from './returns-routing.module';
import { ReturnsComponent } from './returns.component';
import { LayoutModule } from '../@layout/layout.module';
import { ClaimsComponent } from './claims/claims.component';
import {
  MatButtonModule,
  MatFormFieldModule,
  MatInputModule,
  MatRippleModule,
  MatCardModule,
  MatAutocompleteModule
} from '@angular/material';
import { NgxDatatableModule } from '@swimlane/ngx-datatable';
import { ModalModule } from 'ngx-bootstrap/modal';
import { FormsModule } from '@angular/forms';
import { ListReturnsComponent } from './list-returns/list-returns.component';
import { RegisterReturnComponent } from './register-return/register-return.component';
import { BsDatepickerModule } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { esLocale } from 'ngx-bootstrap/locale';
import { NgxUploaderModule } from 'ngx-uploader';

defineLocale('es', esLocale);
@NgModule({
  imports: [
    CommonModule,
    ReturnsRoutingModule,
    LayoutModule,
    MatButtonModule,
    NgxUploaderModule,
    MatFormFieldModule,
    MatInputModule,
    MatRippleModule,
    MatCardModule,
    MatAutocompleteModule,
    BsDatepickerModule.forRoot(),
    ModalModule.forRoot(),
    NgxDatatableModule,
    FormsModule,
  ],
  declarations: [ReturnsComponent, ClaimsComponent, ListReturnsComponent, RegisterReturnComponent]
})
export class ReturnsModule { }

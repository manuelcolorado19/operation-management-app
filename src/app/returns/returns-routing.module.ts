import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { RouterModule, Routes } from '@angular/router';
import { ReturnsComponent } from './returns.component'; 
import { ClaimsComponent } from './claims/claims.component'; 
import { ListReturnsComponent } from './list-returns/list-returns.component'; 
import { RegisterReturnComponent } from './register-return/register-return.component'; 

const appRoutes : Routes = [{
  path: '',
  component: ReturnsComponent,
  children: [
    {
      path: 'claims',
      canActivate: [],
      component: ClaimsComponent,
      data: { title: 'Asiamerica | Reclamos' }
    },
    {
      path: 'list-returns',
      canActivate: [],
      component: ListReturnsComponent,
      data: { title: 'Asiamerica | Devoluciones' }
    },
    {
      path: 'register-return',
      canActivate: [],
      component: RegisterReturnComponent,
      data: { title: 'Asiamerica | Registro Devolución' }
    },
    { path: '', redirectTo: 'main', pathMatch: 'full' }
  ]
}];
@NgModule({
  imports: [
    CommonModule,
    RouterModule.forChild(appRoutes)
  ],
  exports:[RouterModule],
  declarations: [],
  providers: []
})
export class ReturnsRoutingModule { }

import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { RegisterReturnComponent } from './register-return.component';

describe('RegisterReturnComponent', () => {
  let component: RegisterReturnComponent;
  let fixture: ComponentFixture<RegisterReturnComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ RegisterReturnComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(RegisterReturnComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});

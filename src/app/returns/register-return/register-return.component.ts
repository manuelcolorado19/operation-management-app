import { BsLocaleService } from 'ngx-bootstrap/datepicker';
import {defineLocale} from 'ngx-bootstrap/chronos';
import { enGbLocale } from 'ngx-bootstrap/locale';
import { RegisterReturn, Product, Client } from '../models/returns.models';
import { ReturnsServiceService } from '../services/returns-service.service';
import { ToastrService } from 'ngx-toastr';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
import { Subject } from 'rxjs/Subject';
import { SalesServiceService } from '../../sales/services/sales-service.service';
import { Component, OnInit, EventEmitter, ViewChild,  ElementRef, TemplateRef} from '@angular/core';
import { UploadOutput, UploadInput, UploadFile, humanizeBytes, UploaderOptions, UploadStatus } from 'ngx-uploader';

@Component({
  selector: 'app-register-return',
  templateUrl: './register-return.component.html',
  styleUrls: ['./register-return.component.scss']
})
export class RegisterReturnComponent implements OnInit {
	products:any = [];
	rows = [];
  searchTerm$ = new Subject<string>();
  sales = [];
  rows_products = [];
  sale_selected: any = null;
  modalRef: BsModalRef;
  modalRef2: BsModalRef;
  rows2 = [];
	couriers = [];
	editing = {};
  selected_product:any;
  register: RegisterReturn = new RegisterReturn();
  formData: FormData;
  files: File[];
  @ViewChild('file_input') 
  file_input: ElementRef;
  uploadInput: EventEmitter<UploadInput>;
  humanizeBytes: Function;
  dragOver: boolean;
  options: UploaderOptions;

  constructor(private localeService: BsLocaleService,private returnService:ReturnsServiceService,private toastr:ToastrService,private modalService: BsModalService,private saleService: SalesServiceService,) { 
  	this.localeService.use('es');
    this.options = { concurrency: 1, maxUploads: 100,};
    this.files = [];
    this.uploadInput = new EventEmitter<UploadInput>();
    this.humanizeBytes = humanizeBytes;
  	this.listCourier();
    let data = [];
    this.saleService.searchProduct(this.searchTerm$)
      .subscribe(result => {
        console.log(result);
        if (result['term'] == ''){
          data = [];
        }else{

        data = result['results'];
        }
        this.rows_products = data;
        return result;
      });
  }

  ngOnInit() {
  }
  onUploadOutput(output: UploadOutput): void {
    if (output.type === 'addedToQueue' && typeof output.file !== 'undefined') {
      const file: File = output.file.nativeFile;
      
      this.files.push(output.file.nativeFile);
   
    }
  }

  handleFileInput(newfiles: FileList): void {
  
    // Future suported types: .png, .jpg
    for (let i = 0; i < this.files.length; i++) {
      const file: File = this.files[i];
      // make some checks for files
      this.files.push(file);
    }
  }
  removeAllFiles(): void {
    this.files = [];
    this.file_input.nativeElement.value = "";

  }




  listCourier(){
    this.returnService.listCourier().subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.couriers = result.results;
          // console.log(this.couriers);
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }
  addProducts(){
    if(this.register.order_channel === '' || this.register.order_channel === null || this.register.order_channel === undefined){
      this.toastr.error("Debe indicar un número de ordén para buscar los productos", "Error");
    }else{

  	this.returnService.bringProducts(this.register.order_channel).subscribe(
        result => {
          // console.log(result.results.length)
          if (result.results.length > 1){
            // console.log("Mas de uno");
            alert("Se encontraron " +result.results.length +" coincidencias para el dato ingresado, por favor seleccione la indicada.");
            this.sales = result.results;
          }else{
            if (result.results.length == 0){
              this.toastr.warning("No se encontraron productos para esta venta, agregarlos de forma manual", "Disculpe");
            }else{
              let data = result.results[0].sale_of_product;
              let push_data = [];
              this.sale_selected = result.results[0];
              // this.products = result.results[0].sale_of_product;
              for (let i = 0; i <data.length ; i++) {
                if(data[i].sku != 1){
                  push_data.push({
                    id:data[i].id_sale,
                    description:data[i].description,
                    sku:data[i].sku,
                    code_seven_digits:data[i].code_seven_digits,
                    color:data[i].color,
                    quantity:data[i].quantity,
                    id_sale:data[i].id_sale,
                    accesories:'',
                  });
                }
              }
              this.products = push_data;
              // console.log(this.products);
              this.register.products = this.products;
              this.register.sale_id = result.results[0].id
            }
          }
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
    }
  }
  searchProduct(template: TemplateRef<any>){
    this.rows_products = []
    this.modalRef = this.modalService.show(template);
  }
  selectUnitProduct(template: TemplateRef<any>, row:any) {
    // console.log(row);
    this.selected_product = row;
    this.rows2 = row.product_code;
    this.modalRef2 = this.modalService.show(template, { class: 'w-25 nested-modal' });
    // this.register.sale_id = "999999X";
    // this.products.push({
    //   id:row.id_sale,
    //   description:row.description,
    //   sku:row.sku,
    //   quantity:1,
    //   accesories:'',
    // });
    // this.products = [...this.products];
    // this.register.products = this.products;
  }
  selectVariation(row){
    this.register.sale_id = "999999X";


    this.products.push({
      id:this.selected_product.id_sale,
      description:this.selected_product.description,
      sku:this.selected_product.sku,
      code_seven_digits:row.code_seven_digits,
      color:row.color__description,
      quantity:1,
      accesories:'',
    });
    this.products = [...this.products];
    this.register.products = this.products;
    this.modalRef2.hide()
    this.modalRef.hide();


  }
  selectProduct(sale:any){
    let data = sale.sale_of_product;
    let push_data = [];
    // this.products = result.results[0].sale_of_product;
    for (let i = 0; i <data.length ; i++) {
      if(data[i].sku != 1){
        push_data.push({
          id:data[i].id_sale,
          description:data[i].description,
          sku:data[i].sku,
          code_seven_digits:data[i].code_seven_digits,
          color:data[i].color,
          quantity:data[i].quantity,
          accesories:'',
          id_sale:data[i].id_sale,
        });
      }
    }
    this.products = push_data;
    this.register.products = this.products;
    this.sale_selected = sale;
    this.register.sale_id = sale.id

  }
  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.products[rowIndex][cell] = event.target.value;
    this.products = [...this.products];
  }
  removeProduct(index){
  	 if (confirm('¿Desea eliminar este producto?')){
      this.products.splice(index, 1);
      this.products = [...this.products];
    }
  }
  filterChannelType($event){
    if(this.register.courier_selected === 'presential'){
      this.register.client = new Client();
      this.register.courier_id = "99999";
    }else{
      this.register.client = null;
      this.register.courier_id = "1";

    }
  }
  registerReturn(){
    if(this.register.products === undefined){
      this.toastr.error("Debe agregar al menos un producto en la devolución", "Error");
    }else{
        this.register.admission_date = this.formatDate(this.register.admission_date);
        if(this.sale_selected !== null){
          this.register.order_channel = this.sale_selected.order_channel;
        }else{
          this.register.order_channel = "99999";
        }

        this.returnService.createReturn(this.register, this.files)
        .subscribe(
          result => {
              this.toastr.success("Devolución guardada exitosamente", "Bien");
              this.register = new RegisterReturn();
              this.products = [];
              this.register.products = [];
              this.sales = [];
              this.sale_selected = null;
              this.files = [];
              this.file_input.nativeElement.value = "";

              const downloadLink = document.createElement("a");
              const fileName = "Informe de recepción " +result['id']+ ".pdf";
              const linkSource = 'data:application/pdf;base64,' + result['pdf_file'];

                let winparams = 'dependent=yes,locationbar=no,scrollbars=yes,menubar=yes,'+
                    'resizable,screenX=10,screenY=10,width=1050,height=1050';

                let htmlPop = '<embed width=100% height=100%'
                                 + ' type="application/pdf"'
                                 + ' src="data:application/pdf;base64,'
                                 + escape(result['pdf_file'])
                                 + '"></embed>'; 

                let printWindow = window.open ("", "PDF", winparams);
                printWindow.document.write(htmlPop);
                downloadLink.href = linkSource;
                downloadLink.download = fileName;
                downloadLink.click();
          },
          error =>{
            this.toastr.clear();
            for (let each in error.error){
              this.toastr.error(error.error[each][0], "Error")
            }
          });

    console.log(this.register);
    }
  }
  formatDate(date) {
    let d = new Date(date),
        month = '' + (d.getMonth() + 1),
        day = '' + d.getDate(),
        year = d.getFullYear();

    if (month.length < 2) month = '0' + month;
    if (day.length < 2) day = '0' + day;

    return [year, month, day].join('-');
  }

}

import { Injectable } from '@angular/core';
import { HttpHeaders } from '@angular/common/http';
import { Http, ResponseContentType } from '@angular/http';
import { url } from '../../../global';
import { Observable } from 'rxjs/Observable';
import { map, catchError, retryWhen, flatMap } from 'rxjs/operators';
import { HttpClient, HttpErrorResponse, HttpRequest } from '@angular/common/http';
@Injectable({
  providedIn: 'root'
})
export class ReturnsServiceService {
	public headers: HttpHeaders = new HttpHeaders();
  token: string;

  constructor(public http: HttpClient,) {
  	if (JSON.parse(localStorage.getItem('currentUser'))){
      this.token = JSON.parse(localStorage.getItem('currentUser')).token;
      this.headers = this.headers.append('Authorization', 'Bearer '+ this.token);
  	}
   }

   listClaims(): any {
    
    return this.http.get(url+'claims/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }

  updateClaim(row:any){
  	 return this.http.put(url+'claims/'+row.id + "/", row, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return true;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )

  }
  listReturns(params:any): any {
    return this.http.get(url+'returns/', {params:params,headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  listCourier(): any {
    return this.http.get(url+'courier/', {headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  bringProducts(order_channel:string): any {
    return this.http.get(url+'sale/', {params:{search:order_channel, return:"yes"},headers:this.headers})
           .pipe(
             map((response: any) => {
               return response
             })

        )
  }
  createReturn(register:any, files:any): any{
    console.log(register);
    let formData = new FormData();
    Object.keys(register).forEach(key => {
      if (key == 'products' || key == 'client'){
        formData.append(key, JSON.stringify(register[key]));
      }else{

      formData.append(key, register[key])
      }
    });
    for (let i = 0; i < files.length; i++) {
      const file: File = files[i];
      formData.append('images', file, file.name);
      
    }
    return this.http.post(url+'returns/',formData, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  newClaim(data:any){
     return this.http.post(url+'claims/',data, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  createTechnicalReport(register:any): any{
    return this.http.post(url+'technical-report/',register, {headers: this.headers})
          .pipe(
            map((response: any) => {
              // console.log(response);
                return response;
            }),
            // catchError((error: HttpErrorResponse | any) => {
            //   console.log("nopo");
            // })
          )
  }
  reintegrate(product, id_return){
    return this.http.put(url+'returns/' + id_return + "/",product, {headers: this.headers})
              .pipe(
                map((response: any) => {
                  // console.log(response);
                    return response;
                }),
                // catchError((error: HttpErrorResponse | any) => {
                //   console.log("nopo");
                // })
              )
  }
  search(terms: Observable<string>, params: any){

    return terms.debounceTime(650)
      .distinctUntilChanged()
      .switchMap((term, params) => this.searchReturns(term, params));

  }

  searchReturns(term, params) {
    // console.log(params)
    return this.http.get(url + 'returns/',{params:{search:term},headers:this.headers})
    .pipe(
        map((res) => {
          // console.log(res);
          return res;
        })
      )
  }
}

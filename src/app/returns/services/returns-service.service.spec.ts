import { TestBed } from '@angular/core/testing';

import { ReturnsServiceService } from './returns-service.service';

describe('ReturnsServiceService', () => {
  beforeEach(() => TestBed.configureTestingModule({}));

  it('should be created', () => {
    const service: ReturnsServiceService = TestBed.get(ReturnsServiceService);
    expect(service).toBeTruthy();
  });
});

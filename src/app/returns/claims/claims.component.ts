import { Component, OnInit, TemplateRef } from '@angular/core';
import { ToastrService } from 'ngx-toastr';
import { Subject } from 'rxjs/Subject';
import { ReturnsServiceService } from '../services/returns-service.service';
import { BsModalService } from 'ngx-bootstrap/modal';
import { BsModalRef } from 'ngx-bootstrap/modal/bs-modal-ref.service';
@Component({
  selector: 'app-claims',
  templateUrl: './claims.component.html',
  styleUrls: ['./claims.component.scss']
})
export class ClaimsComponent implements OnInit {
	rows = [];
  claim_selected:any ={};
  claim_new:any = {};
	editing = {};
  // send_message: string = '';
  modalRef: BsModalRef;
	searchTerm$ = new Subject<string>();
  constructor(private returnService:ReturnsServiceService, private toastr:ToastrService, private modalService: BsModalService) { 
  	this.listData();
  }

  ngOnInit() {
  }


listData(){
    this.returnService.listClaims().subscribe(
        result => {
          // this.page.totalElements = result.count;
          // this.data = result.results;
          this.rows = result;
          return result;
        },
          error =>{
            this.toastr.clear();
            this.toastr.error("Hubo un error al cargar los datos", "Error");
            
          })
  }

  updateValue(event, cell, rowIndex) {
    this.editing[rowIndex + '-' + cell] = false;
    this.rows[rowIndex][cell] = event.target.value;
    this.rows = [...this.rows];
  }

   saveData(row) {
    this.modalRef.hide();
    // console.log(this.claim_selected);
    this.returnService.updateClaim(row)
          .subscribe(
              result => {
                this.toastr.success("Reclamo actualizado exitosamente", "Bien");
                // this.listData();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
  }

  openModal(template: TemplateRef<any>, row:any) {
    this.claim_selected = row;
    this.modalRef = this.modalService.show(template);
  }
  createClaim(event, template2:TemplateRef<any>){
    this.modalRef = this.modalService.show(template2);
  }
  saveNewClaim(){
    this.modalRef.hide();
     this.returnService.newClaim(this.claim_new)
          .subscribe(
              result => {
                this.toastr.success("Reclamo creado exitosamente", "Bien");
                this.listData();
              },
              error => {
                this.toastr.clear();
                for (let each in error.error){
                  this.toastr.error(error.error[each][0], "Error")
                }
              }
          )
  }

}

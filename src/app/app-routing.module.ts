import { ExtraOptions, RouterModule, Routes } from '@angular/router';
import { NgModule } from '@angular/core';

const appRoutes: Routes = [
  { path: 'auth', loadChildren: './auth/auth.module#AuthModule' },
  { path: 'sale', loadChildren: './sales/sales.module#SalesModule' },
  { path: 'stock', loadChildren: './stock/stock.module#StockModule' },
  { path: 'returns', loadChildren: './returns/returns.module#ReturnsModule' },
  { path: 'purchases', loadChildren: './purchases/purchases.module#PurchasesModule' },
  { path: 'sku', loadChildren: './sku/sku.module#SkuModule' },
  { path: 'prices', loadChildren: './prices/prices.module#PricesModule' },
  { path: 'configuration', loadChildren: './configuration/configuration.module#ConfigurationModule' },
  { path: 'mercadolibre', loadChildren: './mercadolibre/mercadolibre.module#MercadolibreModule' },
  { path: 'payments', loadChildren: './payments/payments.module#PaymentsModule' },
	{ path: '**', redirectTo: 'auth/login' }
]

const config: ExtraOptions = {
  useHash: true,
}

@NgModule({
  imports: [
      RouterModule.forRoot(appRoutes,
        // { enableTracing: true }
      )
  ],
  exports: [RouterModule],
  providers: []
})

export class AppRoutingModule { }
